const mysql = require('mysql2');
const setting = require('./settings');
exports.executeSql = (sql, options, callback) => {
    console.log(sql)
    console.log(options)
    const connMysql = mysql.createConnection(setting.sqlConfig)
    connMysql.execute(sql, options, function (err, results, fields) {
        if (err) {
            callback(results, err)
        }else if(callback !== undefined){
            callback(results)
        }
    })
    connMysql.end()
};
