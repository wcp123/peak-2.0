module.exports = (type, x)=>{
    var ApprovalTransBroker = require('./ApprovalTransmittal')
    var ApprovalTransDiscounter = require('./ApprovalTransDiscounter')
    var Approval = require('./Approval')
    var ScoreCard = require('./ScoreCard')
    var DisbursmentDiscounter = require('./DisbursmentDiscounter')
    var DisbursmentTitled = require('./DisburementTitled')
    var DocRequestForm = require('./DocRequestForm')
    var FundingCoverSheet = require('./FundingCoverSheet')
    var BoardingSheet = require('./BoardingSheet')
    var DisbursmentBroker = require('./DisbursmentBroker')
    var NewScoreCard = require('./ScoreCardNew')
    var PhoneAudit = require('./PhoneAudit')
    if(type==='Approval Transmittal Broker'){
        return ApprovalTransBroker(x)
    }
    else if(type==='Approval'){
        return Approval(x)
    }
    else if(type==='Score Card'){
        return NewScoreCard(x)
    }
    else if(type==='Doc Request Form'){
        return DocRequestForm(x)
    }
    else if(type==='Approval Transmittal Discounter'){
        return ApprovalTransDiscounter(x)
    }
    else if(type==='Funding Cover Sheet'){
        return FundingCoverSheet(x)
    }
    else if(type==='Boarding Sheet'){
        return BoardingSheet(x)
    }
    else if(type==='Disbersment'){
        if(x.source['Company Name']==='DFNY'){
            return DisbursmentTitled(x)
        }
        else if(x.source['Type']==1){
            return DisbursmentBroker(x)
        }
        else{
            return DisbursmentDiscounter(x)
        }
    }
    else if(type==='Phone Audit'){
        return PhoneAudit(x)
    }
}