module.exports = (x) =>{
    var formatCurrency = require('format-currency');
    let hanldeBanking = function(){
        var i = 0
        var html = '';
        x.bankInfo.map(statement=>{
            i+=1;
            html += `<tr>
                <td>
                    Bank ${i} Deposit
                </td>
                <td>
                    <input class='sm' value="${statement['Deposites']}" />
                </td>
                <td>
                    Bank ${i} Withdrawls
                </td>
                <td>
                    <input class="sm" value="${statement['Withdraws']}" />
                </td>
                <td>
                    Bank ${i} Ending Balance
                </td>
                <td>
                    <input class="sm" value="${statement['Balance']}" />
                </td>
            </tr>`
        })
        return html
    }
    let handlePG = function(){
        var i = 0;
        var html = '';
        x.pgInfo.map(pg =>{
            i+=1;
            html += `<tr>
                <td>
                    Guarantor ${i}
                </td>
                <td>
                    <input class="sm" value="${pg['FICO']}"/>
                </td>
                <td>
                    Ownership ${i}
                </td>
                <td>
                    <input class="sm" value="${pg['Ownership']}" />
                </td>
            </tr>`
        })
        return html
    }
    let getDebtRatio = function(){
        return 1.4
    }
    var handlePercent=function(percent){
        var y = parseInt(percent)
        return y/100
    }
    var getTIB = function(){
        let tib = x.companyInfo['Date Established']
        let diff = ((Date.now()) - Date.parse(tib))/1000
        let years = 0
        let days =0
        if (diff >= (365.25 * 86400)){
            years = Math.floor(diff / (362.25*86400))
            diff -= years * 365.25 * 86400
        }
        if(diff >= (86400 * 30.42)){
            days = Math.floor(diff / (86400 * 30.42))
        }
        years += days/(86400 * 30.42)
        years = years.toFixed(2)
        return years
    }
    var getAverageBank = function(){
        let i = 0
        let average = 0
        x.bankInfo.map( statement=>{
            i+=1
            let balance = Number(statement['Balance'].replace(/[^0-9.-]+/g,""));
            average += balance
        })
        return (average/i)
    }
    var getAverageBankFormat = function(){
        let i = 0
        let average = 0
        x.bankInfo.map( statement=>{
            i+=1
            let balance = Number(statement['Balance'].replace(/[^0-9.-]+/g,""));
            average += balance
        })
        let opts = { format: '%s%v', symbol: '$' }
        return formatCurrency((average/i), opts)
    }
    console.log('Score Card')
    return(`<!DOCTYPE html>
    <html>
        <head>
                <link ref='stylesheet' type='text/css' href='SC.css' />
        </head>
        <body>
            <div>
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                            </td>
                        </td>
                        <tr class="Header">
                            
                            <th>
                                <h3><strong>Credit Scoring Report</strong></h3>
                            </th>
                        </tr>
                        <tr>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            App ID
                                            <input class='sm' value="${x.appId}" style="align-self: left" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Company Name
                                            <input value="${x.companyInfo['Company Name']}"  style="align-self: left"/>
                                        </td>
                                    </tr>
                                    <tr>    
                                        <td>
                                            Collateral Description
                                            <input class='lg' value="${x.application['Collateral Description']}"  style="align-self: left; width: 100%"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            Equipment Cost
                                                        </td>
                                                        <td>
                                                            <input value="${x.application['Equipment Cost']}" class='sm' />
                                                        </td>
                                                        <td>
                                                            Term
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Terms']}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Loan Aount
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Approved Amount']}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Lowest FICO
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Lowest FICO']}" />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Lowest FICO']>670?'Accept':x['Lowest FICO']>650?'Caution':'Reject'}" style="${x['Lowest FICO']>670?'background: green; color:white;':x['Lowest FICO']>650?'background: yellow':'background: red'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Paynet
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Paynet']}" />
                                                        </td>
                                                        <td>
                                                            <input  class="sm"  value="${x['Paynet']>670?'Accept':x['Paynet']>650?'Caution':'Reject'}" style="${x['Paynet']>670?'background: green; color:white;':x['Paynet']>650?'background: yellow':'background: red'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Comp Credit
                                                        </td>
                                                        <td>
                                                            <input class="sm"  value="${x['Comp Credit']}"  />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Comp Credit']>49?'Accept':'Reject'}" style="${x['Comp Credit']>49?'background: green; color:white;':'background: red'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Average Bank Statements
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${getAverageBankFormat()}"  />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${getAverageBank()>2500?'Accept':'Reject'}" style="${getAverageBank()>2500?'background: green; color:white;':'background: red'}"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Time in Business
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${getTIB()}">
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${getTIB()>=2?'Accept':'Reject'}" style="${getTIB()>=2?'background: green; color:white;':'background: red'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            OLV
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['OLV']}" />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${handlePercent(x['OLV'])>.09?'Accept':'Reject'}" style="${handlePercent(x['OLV'])>.09?'background: green; color:white;':'background: red'}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Payment Coverage
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${getDebtRatio()}" />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${getDebtRatio()>1.24?'Accept': 'Reject'}" style="${getDebtRatio()>1.24?'background: green; color:white;':'background: red'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Age of Equipment
                                                        </td>
                                                        <td>   
                                                            <input class="sm" value="${x['Age of Equipment']}" />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${8>x['Age of Equipment']?'Accept': 'Reject'}" style="${8>x['Age of Equipment']?'background: green; color:white;': 'background: red'}" >
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Additional Collateral Cash
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Additional Collateral Cash']?x['Additional Collateral Cash']:''}"/>
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Additional Collateral']?0>x['Additional Collateral Cash']?'Accept': 'Reject':'N/A'}" style="${x['Additional Collateral Cash']?0>x['Additional Collateral Cash']?'background: green; color:white;': 'background: red':'background: black; color: white'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            OFAC
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${!x['OFAC']?'No Records': 'Records Found'}" />
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${!x['OFAC']?'Accept': 'Reject'}" style="${!x['OFAC']?'background: green; color:white;': 'background: red'}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    <td>
                                                        SBSS
                                                    </td>
                                                    <td>
                                                        <input class="sm" value="${x['SBSS']?x['SBSS']:''}" />
                                                    </td>
                                                    <td>
                                                        <input class="sm" value="${x['SBSS']?(x['SBSS']>210?'Accept':x['SBSS']>185?'Caution':'Reject'):'NA'}" style="${x['SBSS']?x['SBSS']>210?'background: green; color:white;':x['SBSS']>185?'background: yellow':'background: red':'background: black'}" />
                                                    </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td>
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            Loan Grade
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Loan Grade']?x['Loan Grade']:''}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Total Weight
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Total Weight']?x['Total Weight']: ''}" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Calculated Yield
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Calculated Yield']?x['Calculated Yield']:''}">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Discretionary Yield
                                                        </td>
                                                        <td>
                                                            <input class="sm" value="${x['Discrectionary']?x['Discrectionary']:''}" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Credit Comments
                                        </td>
                                        <td>
                                            <textarea name="Credit comment" style="align-self: left" id="Ccomment" cols="40" rows="10">${x['Credit Comment']['Comment']}</textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <table class='Banking'>
                                            <tbody>
                                                ${hanldeBanking()}
                                            </tbody>
                                        </table>
                                    </tr>
                                    <tr>
                                        <table>
                                            <tbody>
                                                ${handlePG()}
                                            </tbody>
                                        </table>
                                    </tr>
                                </tbody>
                            </table>
                        </tr>
                    </tbody>
                </table>
            </div>
        </body>
    </html>`)

}