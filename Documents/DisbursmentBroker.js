module.exports = (x) =>{
    function handleDate(date){
        return String(date).split('T')[0]
    }
    function vendor(x){
        var i = 0
        var string = ''
        x.vendors.map(vendor=>{
            i+=1
            string+= `<tr>
            <td>
    
                <table>
                    <tbody>
                        <tr>
                            <th>
                                Vendor ${i} Payee
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" value="${vendor['Company Name']?vendor['Company Name']:''}" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" value="${vendor['Address']?vendor['Address']:''}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" value="${vendor['City']?vendor['City']:''}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" value="${vendor['State']?vendor['State']:''}"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <th>
                                Wire or ACH
                            </th>
                            <td>
                                <select value="${vendor.Billing['Wire or ACH']?vendor.Billing['Wire or ACH']:'Wire'}" >
                                    <option value="${0}">Wire</option>
                                    <option value="${1}">ACH</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Account Number
                            </th>
                            <td>   
                                <input type="text" value="${vendor.Billing['Account Number']?vendor.Billing['Account Number']:''}" />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Routing Number
                            </th>
                            <td>
                                <input type="text" value="${vendor.Billing['Routing Number']?vendor.Billing['Routing Number']:''}"/>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Bank Name
                            </th>
                            <td>
                                <input type="text" value="${vendor.Billing['Bank Name']?vendor.Billing['Bank Name']:''}" />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Reference
                            </th>
                            <td>
                                <input type="text" value="${vendor['Dis Add']['Vendor Reference']?vendor['Dis Add']['Vendor Reference']:''}"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <table>
                    <tbody>
                    <tr>
                    <th>
                        Funding Amount
                    <th>
                </tr>
                <tr>
                    <td>
                        <input type="test" value="${vendor['Dis Add']['Total']?vendor['Dis Add']['Total']:''}"/>
                    </td>
                </tr>
                ${vendor['Prefunding']?vendor['Prefunding'].map(pre=>{
                    return `
                        <tr>
                            <td>
                                <input type="text" value="${pre['Amount']?pre['Amount']:'$0.00'}"/>
                            </td>
                        </tr>
                    `
                }):''}
                        
            </tbody>
        </table>
    </tb>
    <td>
        <table>
            <tbody>
                <tr>
                    <th>
                        Release Date
                    </th>
                </tr>
                <tr>
                    <td>
                        <input type="date" value="${vendor['Dis Add']['Release Date']?handleDate(vendor['Dis Add']['Release Date']):''}" />
                    </td>
                </tr>
                ${vendor['Prefunding']?vendor['Prefunding'].map(pre=>{
                    return `
                        <tr>
                            <td>
                                <input type="date" value="${pre['Release Date']?handleDate(pre['Release Date']):''}" />
                            </td>
                        </tr>
                    `
                })
                :''}
                    </tbody>
                </table>
            </td>
        </tr>`
        })
        return string
        
    }
    return `<!DOCTYPE html>
    <html>
    <head>
        <title>${x.companyInfo['Compnay Name']}.pdf</title>
        <link ref='stylesheet' type='text/css' href='test.css' />
    </head>
    <body>
        <div class='invoice-box'>
        <h1 class='doc-title' >
            Disbursment



        <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px" style="padding: 10px 10px 5px 400px">
        </h1>


                <br/>    

        <b> Contract Number</b>


        <input type="text" value="${x['Contract Number']}" />


        <b>Company Name</b>


        <table>
            <tbody>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>
                                            Source Commission
                                        </th>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" value="${x.source['Company Name']}"/>
                                        </td>
                                    </tr>
                                    <tr>
    
                                        <td>
                                            <input type="text" value="${x.source['Contact']['Address']?x.source['Contact']['Address']:''}"/>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" value="${x.source['Contact']['City']?x.source['Contact']['City']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" value="${x.source['Contact']['State']?x.source['Contact']['State']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="text" value="${x.source['Contact']['Zip']?x.source['Contact']['Zip']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Wire or ACH
                                        </th>
                                        <td>
                                            <select value="${x.source['Billing']['ACH or Wire']?x.source['Billing']['ACH or Wire']:'Wire'}">
                                                <option value="${0}">Wire</option>
                                                <option value="${1}">ACH</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Account Number
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Billing']['Account Number']?x.source['Billing']['Account Number']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Routing Number
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Billing']['Routing Number']?x.source['Billing']['Routing Number']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Bank Name
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Billing']['Bank Name']?x.source['Billing']['Bank Name']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Comm Due
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Broker Commission']?x.source['Broker Commission']:''}" />
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <th>
                                            Release Date
                                        </th>
                                        <td>
                                            <input type="date" value="${x.source['Release Date']?handleDate(x.source['Release Date']):''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Reference
                                        </th>
                                        <td>
                                            <select>
                                                <option>${x.companyInfo['Company Name']?x.companyInfo['Company Name']:''}</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>
                                            Broker Commission
                                        </th>
                                        <td>
                                            <input value="${x['Broker Commission']?x['Broker Commission']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Doc Fee Add
                                        </th>
                                        <td>
                                            <input value="${x['Doc Fee Add']?x['Doc Fee Add']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Doc Fee Deducted
                                        </th>
                                        <td>
                                            <input value="${x['Doc Fee Deducted']?x['Doc Fee Deducted']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Commission Held
                                        </th>
                                        <td>
                                            <input value="${x['Commission Held']?x['Commission Held']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
    
                                        <th>
                                            Release Date
                                        </th>
                                        <td>
                                            <input value="${x['Release Date']?x['Release Date']:''}" />
                                        </td>
    
                                    </tr>
                                    <tr>
                                        <th>
                                            Shortfall
                                        </th>
                                        <td>
                                            <input type="text" value="${x['Shortfall']?x['Shortfall']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Shortfall Comment
                                        </th>
                                        <td>
                                            <textarea name="" id="" cols="30" rows="10">${x['Shortfall Comment']?x['Shortfall Comment']:''}</textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    
                        
                        ${vendor(x)}
                </tbody>
            </table>
            <table style="border: 3px solid grey;">
                            <tbody>
                                
                                <tr>
                                    <th>
                                        Loan Amount
                                    </th>
                                    <th>
                                        Loan Amt W/O Doc fee
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" value="${x['Total Invoiced Amount']?x['Total Invoiced Amount']:'$0.00'}" />
                                    </td>
                                    <td>
                                        <input type="text" value="${x['Loan Amount']?x['Loan Amount']:'$0.00'}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Payee total
                                    </th>
                                    <th>
                                        Doc Fee Financed Amount
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" value="${x['Vendor Total']?x['Vendor Total']:'$0.00'}" />
                                    </td>
                                    <td>
                                        <input type="text" value="${x['Doc Fee Financed']?x['Doc Fee Financed']:'$0.00'}" />
                                    </td>
                                </tr>
                            </tbody>
    
                        </table>
                      
                   
                
        </body>
    </html>
    
    <!-- 
        
     -->`
}