module.exports= (x) =>{
    var handleEquipmentDescription = function(){

    }
    var handleLast4AccountNumber = function(){
        var accountNumber = (x.ACH['Account Number']+'')
        return accountNumber.substr(accountNumber.length - 4)
    }
    var handleStartDate = function(){
        var date = new Date(x['First Payment'])
        return date.getDate()
    }
    var handleStartMonth = function(){
        var date = new Date(x['First Payment'])
        return date.getMonth()
    }
    var handleVendors = function(){
        x.vendor.map(vendor=>{
             return `
                <br/>
                Vendor: <span style="text-decoration: underline;">${vendor['Company Name']}</span>  Amount: <span style="text-decoration: underline;">${vendor['Total']}</span>
            `
        })
    }
    
    return `<!DOCTYPE html>
    <head>
        <title>${x.companyInfo['Compnay Name']}.pdf</title>
        <link ref='stylesheet' type='text/css' href='test.css' />
    </head>
    <body>
        <div class='invoice-box'>
            <table cellspacing='0' cellpadding='0'  >
                <tbody >
                    <tr class='top'>
                        <td colspan style="text-decoration: underline;"='2'>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class='title' >
                                        <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h1 class='doc-title'>
                                                Phone Audit
                                            </h1>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <th >
                                            Company Name:
                                        </th>
                                        <td style="border-bottom: 2px solid black;">
                                            ${x.companyInfo['Company Name']}
                                        </td>
                                        <th >
                                            Contract Number
                                        </th>
                                        <td style="border-bottom: 2px solid black;">
                                            ${x['Contract Number']}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Phone Number
                                        </th>
                                        <td style="border-bottom: 2px solid black;">
                                            ${x.companyInfo['Phone Number']}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>
                                            Hello
                                        </th>
                                        <td style="border-bottom: 2px solid black;">
                                            ${x['Signer Name']}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>
                                            This is
                                        </th>
                                        <th style="border-bottom: 2px solid black;">
                                            ${x['Doc Person']}
                                        </th>
                                        <th>
                                            from Dedicated Funding. May I take a minute to review your
                                        </th>
                                    
                                        <th>
                                            loan docs with you on 
                                        </th>
                                        <th style="border-bottom: 2px solid black;">
                                            ${handleEquipmentDescription()}
                                        </th>
                                        <th>
                                            (equipment):
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                Amount of Loan <span style="text-decoration: underline;">${x['Total Financed Amount']}</span style="text-decoration: underline;"> term of Loan <span style="text-decoration: underline;">${x['Term']}</span style="text-decoration: underline;"> Payment <span style="text-decoration: underline;">${x['Payment Amount']}</span style="text-decoration: underline;">
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                Your payment will be <span style="text-decoration: underline;">${handleStartDate()}</span> of each month starting in <span style="text-decoration: underline;">${handleStartMonth()}</span> and that will be ACH out of the account on file which we have as follows:
                                <br>
                                Bank: <span style="text-decoration: underline;">${x.ACH['Bank Name']}</span>
                                <br>
                                Routing Number: <span style="text-decoration: underline;">${x.ACH['Routing Number']}</span>
                                <br>
                                Last 4 of Account Number: <span>${handleLast4AccountNumber()}</span>
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                Looks like we need to the following vendors:
                                    ${handleVendors()}
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                Do you have the equipment? No: <input type="checkbox" /> Yes: <input type="checkbox" />
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                Are you ready to fund the vendor(s) and start the loan? No: <input type="checkbox" /> Yes: <input type="checkbox" />
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                Do you have any questions? No: <input type="checkbox" /> Yes: <input type="checkbox" />
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                What is the email address you want to use for invoices and billing correspondence? 
                                <br>
                                <br>
                                <hr>
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <li>
                                If there are any issues with the payment process, what is the name and phone number of the person to be contacted?
                                <br>
                                <br>
                                <hr>
                            </li>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                        <th>
                                            Again, thank you and have a nice day!!
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>
                                            Internal Use Only:
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            Comments: ${x.comment['Comment']}
                                        </td>
                                    <tr>
                                        <td>
                                            <br>
                                            <hr>
                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br>
                                            <hr>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br>
                                            <hr>
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Completed By:
                        </td>
                        <td>
                            <hr/>
                        </td>
                        <td>
                            Date:
                        </td>
                        <td>
                            <hr/>
                        </td>
                </tbody>
            </table>
        </body>
    </html>`
}