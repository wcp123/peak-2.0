module.exports = (x) =>{
    var handleVendors = function(){
        var i = 0
        var html = `
            <tr style="align: center">
                <th>
                    Vendors:
                    <br/>
                    <i>
                        Any down payment, trade ins, or prefunding terms need to be listed on the invoice(s)
                    </i>
                </th>
            </tr>
            `
        if(x.vendorList.length>=1){
            x.vendorList.map(vendor =>{        
                html+=`
                    <tr style="align: center">
                        <td>
                            <input value="${vendor['Company Name']}" style="width: 100px"/>
                            Total Invoice Amount: <input value="$" />
                            ____Paid
                            ____ Not Paid
                        </td>
                    </tr>`
                i+=1
            })
        }
        if(i===0){
            return `<tr><td>No Vendors Given</td></tr>`
        }
        return html
    }
    var handleFedID = function(){
        var ssnSplice = function(raw){
            return raw.slice(0, 2) + '-' + raw.slice(3, 4) + '-' + raw.slice(5,8)
        }
        var einSplice = function(raw){
            return raw.slice(0, 2) + '-' + raw.slice(2,8)
        }
        var raw = x.companyInfo['Federal ID']?x.companyInfo['Federal ID'].replace('-',''):''
        if(x.companyInfo['Is SSN']===1){
            return ssnSplice(raw)
        }
        else{
            return einSplice(raw)
        }
    }


    return `<!DOCTYPE html>
    <html>
        <head>
            <link ref='stylesheet' type='text/css' href='DRF.css' />
        </head>
        <body>
            <table>
                <tbody>
                    <tr>
                        <th>
                            <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                        </th>
                    </tr>
                    <tr>
                       <th>
                           Funding Cover Sheet
                       </th>
                    </tr>
                    <tr>
                        <td>
                            Discounter: 
                            <input style="width: 300px" value="${x.source['Company Name']}"/>
                        </td>
                        <td>
                            Contact: <input style="width: 300px" value="${x.sourceContact['Contact Name']?x.sourceContact['Contact Name']:''}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone: <input style="width: 300px" value="${x.sourceContact['Phone']?x.sourceContact['Phone']:''}"/>
                        </td>
                        <td>
                            Email: <input style="width: 300px" value="${x.sourceContact['Email']?x.sourceContact['Email']:''}"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Lessee/Debtor:
                            <input style="width: 300px" value="${x.companyInfo['Company Name']}" />
                        </td>
                        <td>
                            DBA:
                            <input style="width: 250px" value="${x.companyInfo['DBA Name']==='null'?'':x.companyInfo['DBA Name']==='undefined'?'':x.companyInfo['DBA Name']}">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="100%">
                            Physical Address:
                            <input type="text" style="width: 100%" value="${x.companyInfo['Address'] + ' ' + x.companyInfo['City'] + ', ' + x.companyInfo['State'] + ' ' + x.companyInfo['Zip']}"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="100%">
                            Billing Address:
                            <input type="text" style="width: 100%" value="${x.companyInfo['Billing Address']?x.companyInfo['Billing Address']:''}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i>(Required if different from Physical Address)</i>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="100%">
                            Equipment Locaiton:
                            
                            <hr style="width: 100%" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                            Fed ID<i>(Required)</i>
                            <input type="text" class="sm" value="${handleFedID()}"  />
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Verbal Verification:
                        </th>
                    </tr>
                    <tr>
                        <td>
                            Contact:_________________________________
                        
                            <i>Written authorization is required if other than signer</i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phone # _________________________________
                        </td>
                    </tr>
                    <tr>
                        <td>
                            E-mail address to send courtesy invoicing stmts:___________________________________________
                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ______ Equipment delivered, ready for verbal and inspection
                        </td>
                    </tr>
                    <tr>
                        <td>
                            _______ Equipment not delivered
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ________ Prefunding required (1/2 commission will be held until inspection completed)
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Sales Tax Treatement
                        </th>
                    </tr>
                    <tr>
                        <td>
                            ________ Up front
                        
                            ________On Stream
                        
                            ________Rate Used
                        
                            ________Exempt 
                        
                        (will need exemption form)
                        </td>
                    </tr>
                    ${handleVendors()}
                    <tr>
                        <td>
                            <i>Any down payment, trade ins, or prefunding terms need to be listed on the invoice(s)</i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Additional vendors (if not approved by credit please request approval prior to submitting for funding)
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr>
                        <td>
                            <hr style="width: 100%">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Special Instructions
                        </th>
                    </tr>
                    <tr><td></td></tr>
                    <tr>
                        <td>
                            <hr style="width: 100%">
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr>
                        <td>
                            
                            <hr style="width: 100%">
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr>
                        <td>
                            
                            <hr style="width: 100%">
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
    </html>`
}