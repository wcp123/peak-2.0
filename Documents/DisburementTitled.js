module.exports = (x) =>{

    function vendorTotalCalc(vendor){
        if(vendor['Prefunding']){
            var total = 0
            vendor['Prefunding'].map(pre=>{
                total += Number(String(pre['Amount']).replace(/[0-9.-]/g,''))
            })
            return total
        }
        else{
            return vendor['Dis Add']['Total']?vendor['Dis Add']['Total']:'$0.00'
        }
    }
    function handleDate(date){
        return String(date).split('T')[0]
    }
    function vendor(x){
        var i = 0
        var string = ''
        x.vendors.map(vendor=>{
            i+=1
            string+= `
                <hr/>

                <table class="Vendor" name="Vendor ${i}">
                    <tbody>

                    <tr>
                        <!--- <th/> is too much indentation.  ---->
                        <td>
                            <table>
                            <tbody>
                                <tr>
                                <td/>

                                    <th>
                                        Vendor ${i} Payee
                                    </th>
                                </tr>
                                <tr>
                                    <th></th>

                                    <td>
                                        <input type="text" value="${vendor['Company Name']}" />
                                    </td>
                                </tr>
                                <tr>
                                <th></th>

                                    <td>
                                        <input type="text" value="${vendor['Address']}"/>
                                    </td>
                                </tr>
                                <tr>
                                <th></th>

                                    <td>
                                        <input type="text" value="${vendor['City']}"/>
                                    </td>
                                </tr>
                                <tr>
                                <th></th>

                                    <td>
                                        <input type="text" value="${vendor['State']}"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    <td/>
                </tr>
               
                    <td>
                    <table>
                        <tbody>
                            <tr>
                                <th>
                                    Wire or ACH
                                </th>
                                <td>
                                    <select value="${vendor.Billing['Wire or ACH']?vendor.Billing['Wire or ACH']:0}" >
                                        <option value="${0}">Wire</option>
                                        <option value="${1}">ACH</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Account Number
                                </th>
                                <td>   
                                    <input type="text" value="${vendor.Billing['Account Number']?vendor.Billing['Account Number']:''}" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Routing Number
                                </th>
                                <td>
                                    <input type="text" value="${vendor.Billing['Routing Number']?vendor.Billing['Routing Number']:''}"/>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Bank Name
                                </th>
                                <td>
                                    <input type="text" value="${vendor.Billing['Bank Name']?vendor.Billing['Bank Name']:''}" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Account Holder's Name
                                </th>
                                <td>
                                    <input type="text" value="${vendor.Billing['Account Holders Name']?vendor.Billing['Account Holders Name']:''}" />
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Reference
                                </th>
                                <td>
                                    <input type="text" value="${vendor['Dis Add']['Vendor Reference']?vendor['Dis Add']['Vendor Reference']:''}"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <th>
                                    Funding Amount
                                <th>
                            </tr>
                            <tr>
                                <td>
                                    <input type="test" value="${vendor['Dis Add']['Total']?vendor['Dis Add']['Total']:''}"/>
                                </td>
                            </tr>
                            ${vendor['Prefunding']?vendor['Prefunding'].map(pre=>{
                                return `
                                    <tr>
                                        <td>
                                            <input type="text" value="${pre['Amount']?pre['Amount']:'$0.00'}"/>
                                        </td>
                                    </tr>
                                `
                            }):''}
                                    
                        </tbody>
                    </table>
                </td>
                <td>
                    <table>
                        <tbody>
                            <tr>
                                <th>
                                    Release Date
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" value="${vendor['Dis Add']['Release Date']?handleDate(vendor['Dis Add']['Release Date']):''}" />
                                </td>
                            </tr>
                            ${vendor['Prefunding']?vendor['Prefunding'].map(pre=>{
                                return `
                                    <tr>
                                        <td>
                                            <input type="text" value="${pre['Release Date']?handleDate(pre['Release Date']):''}" />
                                        </td>
                                    </tr>
                                `
                            })
                            :''}
                        
                        </tbody>
                    </table>
                    </td>
                </tbody>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <th>
                                Vendor Doc Fee
                            </th>
                            <td>
                                <input value="${vendor['Dis Add']['Doc Fee']?vendor['Dis Add']['Doc Fee']:'$0.00'}" />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Vendor Comm
                            </th>
                            <td>
                                <input value="${vendor['Dis Add']['Vendor Commission']?vendor['Dis Add']['Vendor Commission']:''}" />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Total Due
                            </th>
                            <td>
                                <input value="${vendorTotalCalc(vendor)}" />
                            </td>
                        </tr>
                    </tbody>
                </table>
           `
        })
        return string
        
    }
    return `<!DOCTYPE html>
    <html>
    <head>
        <title>${x.companyInfo['Compnay Name']}.pdf</title>
        <link ref='stylesheet' type='text/css' href='test.css' />
    </head>
    <body>
        <div class='invoice-box' style="padding: 10px">
            
                       
            
            <h1 class='doc-title' >
                Disbursment
        
    
        
            <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px" style="padding: 10px 10px 5px 400px">
            </h1>
        
        
                    <br/>    
        
            <b> Contract Number</b>
        
        
            <input type="text" value="${x['Contract Number']}" />
        
        
            <b>Company Name</b>
        
        
            <input type="text" value="${x.companyInfo['Company Name']}"/>
                        
                        
            <table cellspacing='0' cellpadding='0'  >
                <tbody >
                   
                    <tr>
                        <td>
                            <table>
                                <tbody>
                                    <tr>
                                    <th/>

                                        <th>
                                            Source Commission
                                        </th>
                                        
                                    </tr>
                                    <tr>
                                        <th/>
                                        <td>
                                            <input type="text" value="${x.source['Company Name']}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th/>
                                        <td>
                                            <input type="text" value="${x.source['Contact']['Address']?x.source['Contact']['Address']:''}"/>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <th/>

                                        <td>
                                            <input type="text" value="${x.source['Contact']['City']?x.source['Contact']['City']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                         <th/>

                                        <td>
                                            <input type="text" value="${x.source['Contact']['State']?x.source['Contact']['State']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th/>

                                        <td>
                                            <input type="text" value="${x.source['Contact']['Zip']?x.source['Contact']['Zip']:''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Wire or ACH
                                        </th>
                                        <td>
                                            <select value="${x.source['Billing']['ACH or Wire']}">
                                                <option value="${0}">Wire</option>
                                                <option value="${1}">ACH</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Account Number
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Billing']['Account Number']?x.source['Billing']['Account Number']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Routing Number
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Billing']['Routing Number']?x.source['Billing']['Routing Number']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Bank Name
                                        </th>
                                        <td>
                                            <input type="text" value="${x.source['Billing']['Bank Name']?x.source['Billing']['Bank Name']:''}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Reps Commission
                                        </th>
                                        <td>
                                            <input type="text" value="${x['Broker Commission']?x['Broker Commission']:''}" />
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <th>
                                            Release Date
                                        </th>
                                        <td>
                                            <input type="text" value="${x['disbursements']['Release Date']?handleDate(x['disbursements']['Release Date']):''}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Doc Fee Add
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Doc Fee Add']?x['disbursements']['Doc Fee Add']:'$0.00'}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Doc Fee Deduct
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Doc Fee Deduct']?x['disbursements']['Doc Fee Deduct']:'$0.00'}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Comm Due
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Commission Due']?x['disbursements']['Commission Due']:'$0.00'}"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tbody>
                                    
                                    <tr>
                                        <th>
                                            Doc Fee
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Doc Fee']?x['disbursements']['Doc Fee']:'$0.00'}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Dedicated Fee
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Dedicated Fee']?x['disbursements']['Dedicated Fee']:'$0.00'}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Docusign Fee
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Docusign Fee']?x['disbursements']['Docusign Fee']:'$0.00'}" />
                                        </td>
                                    </tr>
                                    <tr>
    
                                        <th>
                                            Wire Fee
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Wire Fee']?x['disbursements']['Wire Fee']:'$0.00'}" />
                                        </td>
    
                                    </tr>
                                    <tr>
                                        <th>
                                            Title Fee
                                        </th>
                                        <td>
                                            <input type="text" value="${x['disbursements']['Title Fee']?x['disbursements']['Title Fee']:'$0.00'}"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            GPS Fee
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['GPS Fee']?x['disbursements']['GPS Fee']:'$0.00'}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Inspection Fee
                                        </th>
                                        <td>
                                            <input value="${x['disbursements']['Inspection Fee']?x['disbursements']['Inspection Fee']:'$0.00'}" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                       
                   
                </tbody>
            </table>
            ${vendor(x)}
                                
            <br/>
            <b>Verification: Loan amount without doc fee should  equal Payee total</b>
            <br/>
            <table style="border: 3px solid grey;">
                            <tbody>
                                
                                <tr>
                                    <th>
                                        Loan Amount
                                    </th>
                                    <th>
                                        Loan Amt W/O Doc fee
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" value="${x['Total Invoiced Amount']?x['Total Invoiced Amount']:''}" />
                                    </td>
                                    <td>
                                        <input type="text" value="${x['Loan Amount']?x['Loan Amount']:''}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Payee total
                                    </th>
                                    <th>
                                        Doc Fee Financed Amount
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" value="${x['Vendor Total']?x['Vendor Total']:''}" />
                                    </td>
                                    <td>
                                        <input type="text" value="${x['Doc Fee Financed']?x['Doc Fee Financed']:''}" />
                                    </td>
                                </tr>
                            </tbody>
    
                        </table>
        </body>
    </html>
    
    <!-- 
        
     -->`
}