module.exports = (x) =>{
    function handleDate(date){
        return String(date).split('T')[0]
    }
    var handleTaxes = function(){
        if(x['Exempt']==1){
            return `
            <th>
                Tax Exempt Certificate
            </th>
            <td>
                <input type="text" value="${x['Exempt Certificate']?x['Exempt Certificate']:''}"
            </td>
            `
        }
        else if(x['On Stream']!==0||x['On Stream']!==null){
            return `
            <th>
                On Stream
            </th>
            <td>
                <input type="checkbox" checked="true" />
            </td>
            <th>
                Sales Tax Rate
            </th>
            <td>
                <input type="text" value="${x['Tax']?x['Tax']:x['Sales Tax Rate']?x['Sales Tax Rate']:'0.00%'}"/>
            </td>
            `
        }
        return `
            <th>
                On Stream 
            </th>
            <td>
                <input type="checkbox" checked="false" />
            </td>
            <th>
                Sales Tax Amount
            </th>
            <td>
                <input type="text" value="${x['Tax']?x['Tax']:x['Sales Tax Amount']?x['Sales Tax Amount']:'$0.00'}"/>
            </td>
        `
    }




    return(`
    <!DOCTYPE html>
<html>
    <head>
        <title>${x.companyInfo['Company Name']}.pdf</title>
        <link ref='stylesheet' type='text/css' href='test.css' />
    </head>
    <body>
        <div class='invoice-box'>
            <table cellspacing='0' cellpadding='0'  >
                <tbody >
                    <tr class='top'>
                        <td colspan='2'>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class='title' >
                                        <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h1 class='doc-title'>
                                                Boarding Sheet
                                            </h1>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class='companyInfo'>
                                <tbody>
                                    <tr>
                                        <th>
                                            Contract Number
                                        </th>
                                    
                                        <td>
                                            <input type="text" value="${x['Contract Number']}"/>
                                        </td>
                                    
                                        <th>
                                            Company Name
                                        </th>
                                        <td>
                                            <input type="text" value="${x.companyInfo['Company Name']}"/>
                                        </td>
                                    
                                    ${x.companyInfo['DBA']!==undefined?`<th>DBA</th><td><input type="text" value="${x.companyInfo['DBA']}"/></td>`:''}
                                    
                                   
                                    <th>
                                        NAICS Code
                                    </th>
                                    <td>
                                        <input type="text" value="${x.companyInfo['NAICS']?x.companyInfo['NAICS']:''}"/>
                                    </td>
                                    </tr>
                                        <th>
                                            Email Address
                                        </th>
                                        <td>
                                            <input type="text" value="${x['Accounting Email']?x['Accounting Email']:''}"/>
                                        </td>
                                   
                                       <th>
                                           Contact Name
                                       </th>
                                       <td>
                                        <input type="text" value="${x['Contact Name']?x['Contact Name']:''}"/>

                                           
                                       </td>
                                       <th>
                                           Phone Number
                                       </th>
                                       <td>
                                            <input type="text" value=" ${x.companyInfo['Phone Number']?x.companyInfo['Phone Number']:''}"/>
                                       </td>
                                       <th>
                                           Federal ID
                                       </th>
                                       <td>
                                            <input type="text" value="${x.companyInfo['Federal ID']?x.companyInfo['Federal ID']:''}"/>
                                       </td>
                                   </tr>
                                   <tr>
                                       <th>
                                           Address
                                       </th>
                                       <td>
                                            <input type="text" value="${x['Billing Address']?x['Billing Address']:x.companyInfo['Address']}"/>
                                       </td>
                                       <th>
                                           City
                                       </th>
                                       <td>
                                            <input type="text" value="${x['Billing City']?x['Billing City']:x.companyInfo['City']}"/>
                                       </td>
                                       <th>
                                           State
                                       </th>
                                       <td>
                                           <input type="text" value="${x['Billing State']?x['Billing State']:x.companyInfo['State']}"/>
                                       </td>
                                       <th>
                                           Zip
                                       </th>
                                       <td>
                                            <input type="text" value="${x['Billing Zip']?x['Billing Zip']:x.companyInfo['Zip']}"/>
                                       </td>
                                   </tr>
                                </tbody>
                            </table>
                            <hr/>
                        </td>
                    </tr>
                    <tr>

                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Account Holder's Name
                                    </th>
                                    <td>
                                        <input type="text" value="${x.companyInfo['Billing']['Account Holders Name']?x.companyInfo['Billing']['Account Holders Name']:''}"/>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        ACH Routing Number
                                    </th>
                                    <td>
                                        <input type="text" value="${x.companyInfo['Billing']['Routing Number']?x.companyInfo['Billing']['Routing Number']:''}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        ACH Account Number
                                    </th>
                                    <td>
                                        <input type="text" value="${x.companyInfo['Billing']['Account Number']?x.companyInfo['Billing']['Account Number']:''}">
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        ACH Bank Name
                                    </th>
                                    <td>
                                        <input type="text" value="${x.companyInfo['Billing']['Bank Name']?x.companyInfo['Billing']['Bank Name']:''}"/>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <hr/>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        First Payment Date
                                    </th>
                                    <td>
                                        <input type="date" value="${x.boardingsheet['First Payment Date']?handleDate(x.boardingsheet['First Payment Date']):''}"/>
                                    </td>
                                    <th>
                                        Accrual Date
                                    </th>
                                    <td>
                                        <input type="date" value="${x.boardingsheet['Loan Start Date']?handleDate(x.boardingsheet['Loan Start Date']):''}" />
                                    </td>
                                    <th>
                                        Basic Rental Payment
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Payment Amount']?x['Payment Amount']:''}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Advanced
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Advanced']?x['Advanced']:''}" />
                                    </td>
                                    <th>
                                        Remaining Term
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Remaining Payments']?x['Remaining Payments']>=0?x['Remaining Payments']:'':''}"/>
                                    </td>
                                    <th>
                                        Inital Term
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Term']?x['Term']:''}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Payment Frequency
                                    </th>
                                    <td>
                                        <input type="text" value="${x.boardingsheet['Payment Frequency']?x.boardingsheet['Payment Frequency']:''}"/>
                                    </td>
                                    <th>
                                        Contract Type
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Contract Type']?x['Contract Type']:'A-Promissory Note'}"/>
                                    </td>
                                
                                    <th>
                                        Billing Group
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Billing Group']?x['Billing Group']:'A-1-7'}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Equipment Description
                                    </th>
                                    <td>
                                        <textarea cols="30" rows="3"  value="${x.boardingsheet['Equipment Description']?x.boardingsheet['Equipment Description']:''}"/>
                                    </td>
                                    <th>
                                        Asset Code
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Asset Code']?x['Asset Code']:''}"/>
                                    </td>
                                    <th>
                                        Upfront
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Upfront']?x['Upfront']:'None'}"/>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                        <hr/>
                        <table>
                            <tbody>
                                <tr>
                                    ${handleTaxes()}
                                </tr>
                            </tbody>
                        </table>
                        <hr/>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Source Name
                                    </th>
                                    <td>
                                        <input type="text" value="${x.source['Company Name']}"/>
                                    </td>
                                    <th>
                                        Type
                                    </th>
                                    <td>
                                        <input type="text" value="${x.source['Source Class']===1?'Broker':x.source['Source Class']===2?'Discounter':x.source['Source Class']===3?'Direct':'Bank'}">
                                    </td>
                                    
                                </tr>
                            </tbody>
                            </table>
                            <hr/>
                            <table>
                                <tbody>    
                                <tr>
                                    
                                    <th>
                                        Insurance Information
                                        
                                    </th>
                                </tr>
                                <tr>

                                    <th>
                                        Effect Date
                                    </th>
                                    <td>
                                        <input type="date" value="${x.boardingsheet['Effect Date']?handleDate(x.boardingsheet['Effect Date']):''}"/>
                                    </td>
                                    <th>
                                        Expire Date
                                    </th>
                                    <td>
                                        <input type="date" value="${x.boardingsheet['Expire Date']?handleDate(x.boardingsheet['Expire Date']):''}"/>
                                    </td>
                                    <th>
                                        Loan Amount
                                    </th>
                                    <td>
                                        <input type="text" value="${x['Loan Amount']?x['Loan Amount']:''}"/>
                                    </td>

                                </tr>
                                <tr>
                                    <th>
                                        TBAS Rate
                                    </th>
                                    <td>
                                        <input type="text" value="${x.boardingsheet['TBAS Rate']?x.boardingsheet['TBAS Rate']:'0.00%'}"/>
                                    </td>
                                    <th>
                                        Dedicated Rate
                                    </th>
                                    <td>
                                        <input type="text" value="${x.credit['Discrectionary']?x.credit['Discrectionary']:x.credit['Calculated Yeild']?x.credit['Calculated Yeild']:'0.00%'}"/>
                                    </td>
                                    <th>
                                        Customer Rate
                                    </th>
                                    <td>
                                        <input type="text" value="${x.boardingsheet['Customer Rate']?x.boardingsheet['Customer Rate']:'0.00%'}"/>
                                    </td>
                                </tr>

                                <tr>
                                    <th>
                                        Status
                                    </th>
                                    <td>
                                        <input type="text" value="${x.status['Status']?x.status['Status']:''}" />
                                    </td>
                                    <th>
                                        Status Date
                                    </th>
                                    <td>
                                        <input type="date" value="${x.status['Status Date']?handleDate(x.status['Status Date']):''}"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
`)
}