module.exports = (x) =>{

    var handleDocFee = function(){
        //need to allow for additional fees to be incorporated
        var formatCurrency = require('format-currency')
        // return formatCurrency(Number(x['Doc Fee'].replace(/[^0-9.-]+/g,"")))
        let docFee = Number(x['Doc Fee'].replace(/[^0-9.-]+/g,""));
        let inspectionFee = Number(x['Inspection Fee'].replace(/[^0-9.-]+/g,""));
        let opts = { format: '%s%v', symbol: '$' }
        return formatCurrency(docFee+inspectionFee, opts)
    }
    var handleVendors = function(){
        var html = ''
        if(x.vendorList.length>=1){
            x.vendorList.map(vendor =>{
                html+=`
                    <tr>
                        <td>
                            <input style="width: 400px" value="${vendor['Company Name']}"/>
                        </td>
                        <td>
                            <input sytle="width: 50px" value="${vendor['Approved']?'Yes':'No'}"/>
                        </td>
                        <td>
                            <input sytle="width: 50px" value="${vendor['Approved for Prefunding']?'Yes':'No'}"/>
                        </td>
                        <td>
                            <input style="width: 50px" value="${vendor['Prefunding Terms']==='undefined'?'':vendor['Prefunding Terms']==='null'?'':vendor['Prefunding Terms']}"/>
                        </td>
                    </tr>
                `
            })
        }
        else{
            html+=`
                <tr>
                    <td>
                        No Vendors Listed
                    </td>
                </tr>
            `
        }
        return html
    }
    var handlePG = function(){
        var i = 0
        var j = 0
        var html =
        `<tr>
            <th>
                Guarantors:
            </th>
        </tr>
        <tr>
        `
        
        
            x.pgInfo.map(pg=>{
                if(i===0 || i%2===0){
                    html+=`
                        <tr>
                            <td>
                                <input value="${pg['First Name'] + ' ' +pg['Last Name']}"/>
                            
                        `
                    i+=1
                }
                else{
                    html+=`
                            
                                <input value="${pg['First Name'] + ' ' +pg['Last Name']}"/>
                            </td>
                        </tr>`
                    i+=1
                }
            })
            if(i%2===0){
                html+='</td></tr>'
                return html
            }
            
        
        
            x.cgInfo.map(cg=>{
                if(j===0 || j%2===0){
                html+=`
                <tr>
                    <td>
                        <input value="${cg['Company Name']}"/>
                    
                `
                j+=1
                }
                else{
                    html+=`
                       
                            value="${cg['Company Name']}"/>
                        </td>
                    </tr>`
                    j+=1
                }
            })
            if(j%2===0){
                html+='</td></tr>'
                return html
            }
            
        
        
        if(i===0 || j==0){
            return `<tr><td>No Guarantors</td></tr>`
        }
        // else if(i%2===0 || j%2===0){
        //     html+='</tr>'
        //     return html
        // }
        return html
    }
    var handleFeetoSource = function(){
        var formatCurrency = require('format-currency')
        let amount = Number(x['Approved Amount'].replace(/[^0-9.-]+/g,""));
        if(amount<75000){
            return "Up to 10 points"
        }
        else if(amount<150000){
            return "Up to 8 points"
        }
        else{
            return "Up to 6 points"
        }
    } 
    return `<!DOCTYPE html>
    <html>
        <head>
            <title>${x.companyInfo['Compnay Name']+' Approval Transmittal'}.pdf</title>
            <link ref='stylesheet' type='text/css' href='ApprovalTransmittal.css' />
        </head>
        <body>
            <table>
                <tbody>
                    <tr style="background-color: lightgrey">
                        <td>
                            <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                        </td>
                        <td sytle="fornt-size: 50px">
                            Approval Transmittal
                        </td>
                    </tr>
                    <tr>
                        <td>
                            TO:
                            ${x.source['Company Name']}
                            <hr width="500px"/>
                        </td>
                        <td>
                            Application Number:
                            ${x.application['ID']}
                            <hr width="100px"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            E-mail
                            <hr width="500px" />
                        </td>
                        <td>
                            Approval Date:
                            ${x['Status Date']}
                            <hr width="100px" />
                        </td>
                    </tr>
                    <tr style="align-items: right">
                        <td></td>
                        <td>
                            Expiration Date
                            ${x['Credit Expire']}
                            <hr width="100px" />
                        </td>
    
                    </tr>
                    <tr>
                        <td class="Disclaimer" style="border: 10px solid gray" colspan="3">
                            This memo serves as a confirmation to you that below mentioned applicant has been approved for credit under
                            <br/>
                            the terms listed below. Any changes to these terms must be approved by credit prior to funding and subject to receipt
                            <br/>
                            and acceptance of all documentation.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Company:</strong>
                            ${x.companyInfo['Company Name']}
                            <hr width="350px" />
                        </td>
                        <td>
                            <strong>DBA</strong>
                            ${x.companyInfo['DBA Name']==='undefined'?'':x.companyInfo['DBA Name']==='null'?'':x.companyInfo['DBA Name']}
                            <hr width="350px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Collateral Description:</strong>
                            ${x.application['Collateral Description']}
                            <hr style="margin-left: 120px" width="350px" />
                        </td>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        Approved Amount
                                    </td>
                                    <td>
                                        <input value="${x['Approved Amount']}" />
                                    </td>
                                    <td>
                                        Frequency
                                    </td>
                                    <td>
                                        <input value="${x['Frequency']}" />
                                    </td>
                                    <td>
                                        Dedicated Rate
                                    </td>
                                    <td>
                                        <input value="${x['Discrectionary']?x['Discrectionary']:x['Calculated Yield']}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Terms
                                    </td>
                                    <td>
                                        <input value="${x['Terms']}" />
                                    </td>
                                    <td>
                                        Down Payment
                                    </td>
                                    <td>
                                        <input value="${x['Down Payment']?x['Down Payment']:'$0.00'}" />
                                    </td>
                                    <td>
                                        Fee to Source
                                    </td>
                                    <td>
                                        <!-------This needs more info------->
                                        <input value="${handleFeetoSource()}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Documentaiton Fee
                                    </td>
                                    <td>
                                        <input value="${handleDocFee()}" />
                                    </td>
                                    <td>
                                        Advance Payment
                                    </td>
                                    <td>
                                        <input value="${x['Payment']}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inspection
                                    </td>
                                    <td>
                                        <input value="${x['Inspection']?'Required':'Not Required'}" />
                                    </td>
                                    <td>
                                        ACH
                                    </td>
                                    <td>
                                        <input value="${x['ACH Waived']?'Not Required':'Required'}" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                
                                ${handlePG()}
                            </tbody>
                        </table>
                    </tr>                
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Vendors:
                                    </th>
                                    <th>
                                        Approved
                                    </th>
                                    <th>
                                        Approved for Prefunding
                                    </th>
                                    <th>
                                        Terms
                                    </th>
                                </tr>
                                ${handleVendors()}
                            </tbody>
                        </table>
                       
                    </tr>
                    <tr>
                        <td>
                            Credit Comment
                        </td>
                        <td>
                            <textarea cols="50" rows="3" style="text-align: left" >
                                ${x.additional['Credit Requirements']?x.additional['Credit Requirements']:''}
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Addiotal Credit Requirements
                                    </th>
                                </tr>

                                <tr>
                                    <td>
                                        UCC Search
                                        <input type="text" class="sm" style="width: 15px" value="${x.additional['UCC Search']?'X':''}"/>
                                    </td>
                                    <td>
                                        Legal Land Description
                                        <input type="" class="sm" style="width: 15px" value="${x.additional['Legal Land Description']?'X':''}"/>
                                    </td>
                                    <td>
                                        Equipment Approval
                                        <input type="" class="sm" style="width: 15px" value="${x.additional['Equipment Approval']?'X':''}"/>
                                    </td>
                                    <td>
                                        Vendor Approval
                                        <input type="" class="sm" style="width: 15px" value="${x.additional['Vendor Approval']?'X':''}"/>
                                    </td>
                                    <td>
                                        Title/MSO
                                        <input type="" class="sm" style="width: 15px" value="${x.additional['Title/MSO']?"X":''}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th>
                                                    Items needed for Funding:
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" class="sm" style="width: 15px" value="${x.additional['Lease or Loan Agreement']?'X':''}"/>
                                                    Lease/Loan Agreement
                                                </td>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Vendor Invoice']?'X':''}"/>
                                                    Vendor Invoice
                                                </td>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Assignment']?'X':''}"/>
                                                    Assignment
                                                </td>  
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Exhibit A']?'X':''}"/>
                                                    Exhibit A
                                                </td>
                                                <td>
                                                    <i>Proof of Tax Exemption if applicable</i>
                                                </td>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Notice of Assignment']?'X':''}"/>
                                                    Notice of Assignment
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Delivery and Acceptance']?'X':''}"/>
                                                    Delivery and Acceptance
                                                </td>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['UCC Fixture Filing']?'X':''}"/>
                                                    UCC Fixture Filing
                                                </td>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Copy checks to vendor']?'X':''}"/>
                                                    Copy(s) checks to vendor
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Corporate Resolution']?'X':''}" />
                                                    Corporate Resolution/Incumbency
                                                </td>
                                                <td>
                                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Legal land Description']?'X':''}" />
                                                    Legal Land Description
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </tr>
                                <tr>
                                    <td>
                                    <input type="" class="sm" style="width: 15px" value="${x.additional['Title/MSO']?'X':''}"/>

                                    </td>
                                    <td>
                                    Copies of front and back of MSO/Title showing Dedicated Funding as lien holder

                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </tr>
                    <tr>
    
                    </tr>
                    <tr>
                        <td>
                            Additional items need to be submitted with funding package:
                        </td>
                        <td>
                            <ul>
                                <li>
                                    Funding cover sheet
                                </li>
                                <li>
                                   Copies of Driver's license on all signers
                                </li>
                                <li>
                                    Guarantor(s) home address, phone number and e-mail address
                                </li>
                                <li>
                                    Time In business validation (if SOS was not provided with credit submission)
                                </li>
                                <li>
                                    Articles of Incorporation (Bylaws maybe requested)
                                </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
    </html>`
}