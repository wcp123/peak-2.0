module.exports = (x) =>{
    // ${x.companyInfo['Federal ID']?x.companyInfo['Federal ID']==='undefined'?'':x.companyInfo==='null'?'':handleFedID():''}
    var handleGuarantors = function(){
        var i = 0
        var j = 0
        var html =
        `<tr>
            <th>
                Guarantors:
            </th>
        </tr>
        <tr>
        `
        
        
            x.pgInfo.map(pg=>{
                html+=`
                    <tr>
                        <td>
                            <input name='PG ${i}' value="${pg['First Name'] + ' ' +pg['Last Name']}"/>
                            
                            Email Address: <input name="PG ${i} Email" />
                        </td>
                    </tr>`  
                    i+=0
            })            
            x.cgInfo.map(cg=>{
                if(j===0 || j%2===0){
                html+=`
                <tr>
                    <td>
                        <input value="${cg['Company Name']}"/>
                    
                `
               
                }
                else{
                    html+=`
                       
                            <input value="${cg['Company Name']}"/>
                        </td>
                    </tr>`
                    
                }
                j+=1
            })
            if(j%2===0){
                html+='</td></tr>'
                return html
            }
            
         
        
        if(i===0&&j==0){
            return `<tr><td>No Guarantors</td></tr>`
        }
        // else if(i%2===0 || j%2===0){
        //     html+='</tr>'
        //     return html
        // }
        return html
    }
    var handleVendors = function(){
        var i = 0
        var html = `
            <tr style="align: center">
                <th>
                    Vendors:
                    <br/>
                    <i>
                        Any down payment, trade ins, or prefunding terms need to be listed on the invoice(s)
                    </i>
                </th>
            </tr>
            `
        
            x.vendorList.map(vendor =>{        
                html+=`
                    <tr style="align: center">
                        <td>
                            <input value="${vendor['Company Name']}" style="width: 300px"/>
                            Total Invoice Amount:_______________
                        </td>
                    </tr>`
                i+=1
            })
        
        if(i===0){
            return `<tr><td>No Vendors Listed</td></tr>`
        }
        return html
    }
    var handleDocFee = function(){
        //need to allow for additional fees to be incorporated
        var formatCurrency = require('format-currency')
        // return formatCurrency(Number(x['Doc Fee'].replace(/[^0-9.-]+/g,"")))
        let docFee = Number(x['Doc Fee'].replace(/[^0-9.-]+/g,""));
        let inspectionFee = Number(x['Inspection Fee'].replace(/[^0-9.-]+/g,""));
        let opts = { format: '%s%v', symbol: '$' }
        return formatCurrency(docFee+inspectionFee, opts)
    }
    var handleFedID = function(){
        if(x.companyInfo['Federal ID']){
            var ssnSplice = function(raw){
                raw = String(raw).replace('-','')

                return raw.slice(0, 2) + '-' + raw.slice(2, 4) + '-' + raw.slice(4,9)
            }
            var einSplice = function(raw){
                raw = String(raw).replace('-','')

                return raw.slice(0, 2) + '-' + raw.slice(2,9)
            }
            var raw = x.companyInfo['Federal ID'].replace(/a-z -/gi,'')
            if(x.companyInfo['Is SSN']===1){
                return ssnSplice(raw)
            }
            else{
                return einSplice(raw)
            }
        }
        else{
            return ''
        }
        
    }
    console.log('Doc Request Form')
    return `
    <!DOCTYPE html>
<html>
    <head>
        <link ref='stylesheet' type='text/css' href='DRF.css' />
    </head>
    <body>
        <table>
            <tbody>
                <tr>
                    <td>
                        <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                    </td>
                </tr>
                <tr>
                    <th>
                        Doc Request Form: 
                    </th>
                </tr>
                <tr>
                    <table>
                        <tbody> 
                            <tr>
                                <td>
                                    Company:
                                    <input style="width: 300px" value="${x.companyInfo['Company Name']}" />
                                </td>
                                <td>
                                    DBA:
                                    <input style="width: 250px" value="${x.companyInfo?x.companyInfo['DBA Name']==='undefined'?'':x.companyInfo['DBA Name']==='null'?'':x.companyInfo['DBA Name']:''}">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    Physical Address:
                                    <input type="text" style="width: 100%" value="${x.companyInfo['Address'] + ' ' + x.companyInfo['City'] + ', ' + x.companyInfo['State'] + ' ' + x.companyInfo['Zip']}"  />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    Billing Address:
                                    <input type="text" style="width: 100%" value="" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i>(Required if different from Physical Address)</i>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    Equipment Location:
                                    
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    
                                </td>
                                <td>
                                    Fed ID<i>(Required)</i>
                                    <input type="text" class="sm" value="${handleFedID(x.companyInfo['Federal ID'])}" />
                                    <!---<input type="text" class="sm" value=""  />--->
                                </td>
                                <td>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    Signer Name:
                                    <hr style='width: 70%; margin-left: 90px'>
                                </td>
                                <td colspan="2">
                                    Title:
                                    <hr style='width: 100%'>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td colspan="1">
                                    Phone Number:
                                    <hr style="width: 80%"/>
                                </td>
                                <td colspan="3">
                                    E-Mail Address:
                                    <hr style="width: 100%" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    E-mail address to send Courtesy invoincing statements: 
                                    <hr style="width: 40%; margin-left: 350px;"/>
                                </td>
                            </tr>
                            <tr>
                                <table>
                                    <tbody>
                                        ${handleGuarantors()}
                                    </tbody>
                                </table>
                            </tr>
                            <tr>
                                <table>
                                    <tbody> 
                                        ${handleVendors()}
                                    </tbody>
                                </table>
                            </tr>
                            <tr>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Additional Vendors <i>(if not approved by credit please request approval prior to requesting documentation)</i>
                                
                                    <hr  style="width: 100%; margin-top: 30px"/>
                                </td>
                            </tr>
                            <tr>
                                
                                <td>
                                    Documentation Fee 
                                    <input sytle="width: 50px" value="${handleDocFee()}"  />
                                </td>
                                <td>
                                    Financed:
                                
                                    Yes
                                    <input style="width: 20px" />
                                
                                    No
                                    <input style="width: 20px" />
                                </td>
                            
                            </tr>
                            <tr>
                                <table>
                                    <tbody>

                                        <tr>
                                            <td>
                                                Comments on Doc Fee: 
                                                <hr style="width: 100%; margin-left: 100px" />
                                            </td>
                                        </tr>
                                    </tbody>
                                <table>
                            </tr>
                            <tr>
                                <td>
                                    Total Financed Amount <i>(Please include T-value if you have one)</i>
                                    <hr style="width: 100%; margin-top: 50px"  />
                                </td>
                            </tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr>
                                <td>
                                    Terms
                                    <input value="" style="width: 40px"/>
                                
                                    Dedicated Rate
                                    <input value="${x['Discrectionary']?x['Discrectionary']:x['Calculated Yield']}" style="width: 55px"/>
                                </td>
                                <td>
                                    Payment Amount
                                    <hr style="width: 100%; margin-left: 120px">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Advance Payment(s)
                                    <hr style="width: 80%; margin-left: 130px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Commission <i>(please indicate any deductions such as doc fee)</i>
                                    <hr style="width: 70%; margin-left: 375px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Documents e-mailed to <input style="width: 300px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Special Instructions
                                </td>
                            </tr>
                            <tr><td><hr style="width: 100%"></td></tr>
                            <tr><td><hr></td></tr>
                            <tr><td><hr></td></tr>
                        </tbody>
                    </table>
                </tr>
               
            </tbody>
        </table>
    </body>
</html>
    `



}