module.exports = (x)=>{
    let handleBanks = ()=>{
        var bankCount = 0
        var html = ''
        html += `<table>
            <tbody>`
        x.bankInfo.map(bank=>{
            bankCount+=1
            html +=  `<tr>
                    <td>
                        Bank ${bankCount} Deposits
                    </td>
                    <td>
                        <input type="text" value="${bank['Deposites']?bank['Deposites']:''}" />
                    </td>
                    <td>
                        Bank ${bankCount} Withdrawls
                    </td>
                    <td>
                        <input type="text" value="${bank['Withdrawals']?bank['Withdrawals']:''}" />
                    </td>
                    <td>
                        Bank ${bankCount} Balance
                    </td>
                    <td>
                        <input type="text" value="${bank['Balance']?bank['Balance']:''}" />
                    </td>
                </tr>`
        })
        html += `</tbody>
        </table>`
        return html
    }
    let handlePG = ()=>{
        var pgCount = 0
        var html = ''
        html+= `<table>
            <tbody>`
        x.pgInfo.map(pg=>{
            pgCount += 1
            html+=`<tr>
                    <td>
                        Guar${pgCount} FICO
                    </td>
                    <td>
                        <input type="text" value="${pg['FICO']?pg['FICO']:''}" />
                    </td>
                    <td>
                        Ownership ${pgCount}
                    </td>
                    <td>
                        <input type="text" value="${pg['Ownership']?pg['Ownership']:''}" />
                    </td>
                </tr>`

                })
        html+=  `</tbody>
        </table>`

        return html
    }
    let formatCurrency = require('format-currency')
    let opts = { format: '%s%v', symbol: '$' }

    return `
    <!DOCTYPE html>
<html>
    <head>
        <link ref='stylesheet' type='text/css' href='SC.css' />
    </head>
    <body>
        <div>
            
            <h3><strong>Credit Scoring Report</strong></h3>
            <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px" style="padding: 1px 1px 10px 500px" >

            <table>
                <tbody>
                    <tr>
                        <td>
                            <tr>
                                <td>
                                    App ID
                                </td>
                                <td>
                                    <input type="text" name='appID' value="${x['appID']}" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Company Name
                                </td>
                                <td>
                                    <input type="text" name='Company Name' size="30" value="${x.companyInfo['Company Name']}"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Collateral Description
                                </td>
                                <td>
                                    <textarea name='Collateral Description' cols="30" rows="3" >${x.application['Collateral Description']}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Submitted Amount
                                </td>
                                <td>
                                    <input name='Submitted Amount' value="${x.application['Submitted Amount']}"  />
                                </td>
                                <td>
                                    Term
                                </td>
                                <td>
                                    <input type="text" value="${x['Terms']}"/>
                                </td>
                            </tr>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table style="align:right">
                <tbody>
                    <tr>
                        <td>
                        <table>
                        <tbody>
                            <tr>
                                <td>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Loan Grade
                                                </td>
                                                <td>
                                                    <input type="text" name='Loan Grade' value="${x['Loan Grade']?x['Loan Grade']:''}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Prob. Deliquency
                                                </td>
                                                <td>
                                                    <input name='Deliquency' value="${x['Deliquency'].toFixed(2)}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Points
                                                </td>
                                                <td>
                                                    <input type="text" name='Points' value="${x['Points']}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Calculated Yield
                                                </td>
                                                <td>
                                                    <input type="text" name='Calculated Yield' value="${x['Calculated Yield']}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Discretionary Yield
                                                </td>
                                                <td>
                                                    <input name='Discrectionary' value="${x['Discrectionary']?x['Discrectionary']:''}" />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                        </td>
                    </tr>
                </tbody>
            </table>

            <table>
                <tbody>
                    <tr>
                        <td>
                            Lowest FICO
                        </td>
                        <td>
                            <input type="text" name='Lowest FICO' value="${x['Lowest FICO']}" />
                        </td>
                        <td>
                            <input type="text" style="${x['Lowest FICO']>=685?"background:green":x['Lowest FICO']>=670?"background:grey":"background:yellow"}" value="${x['Lowest FICO']>=685?'Resale':x['Lowest FICO']>=670?'Either':'See Average'}"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Average FICO
                        </td>
                        <td>
                            <input type="text" name='Average FICO' value="${x['Average FICO']}" />
                        </td>
                        <td>
                            <input type="text" style="${x['Average FICO']>=670?"background:grey":x['Average FICO']>=650?"background:lightblue":"background: red"}" value="${x['Average FICO']>=670?'Either':x['Average FICO']>=650?'Retention':'Decline'}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            SBSS Score
                        </td>
                        <td>
                            <input type="text" name='SBSS' value="${x['SBSS']}" />
                        </td>
                        <td>
                            <input type="text" style="${x['SBSS']>=190?"background:green":x['SBSS']>=175?"background:grey":x['SBSS']>=165?"background:lightblue":"background:red"}" value="${x['SBSS']>=190?'Resale':x['SBSS']>=175?'Either':x['SBSS']>=165?'Retention':'Decline'}"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Paynet
                        </td>
                        <td>
                            <input type="text" name='Paynet' value="${x['Paynet']}" />
                        </td>
                        <td>
                            <input type="text" style="${x['Paynet']>=680?"background:green":x['Paynet']>=670?"background:grey":x['Paynet']>=635?"background:lightblue":"background:red"}" value="${x['Paynet']>=680?'Resale':x['Paynet']>=670?'Either':x['Paynet']>=635?'Retention':'Decline'}"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comp Credit
                        </td>
                        <td>
                            <input name='Comp Credit' value="${x['Comp Credit']}" />
                        </td>
                        <td>
                            <input type="text" style="${Number(String(x['Comp Credit']).split('%')[0])>=75?"background:green":Number(String(x['Comp Credit']).split('%')[0])>=50?"background:grey":Number(String(x['Comp Credit']).split('%')[0])>=35?"background:lightblue":"background:red"}" value="${Number(String(x['Comp Credit']).split('%')[0])>=75?'Resale':Number(String(x['Comp Credit']).split('%')[0])>=50?'Either':Number(String(x['Comp Credit']).split('%')[0])>=35?'Retention':'Decline'}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Average Bank Statement
                        </td>
                        <td>
                            <input name='Average Banks' value="${formatCurrency(x['Average Banks'], opts)}"  />
                        </td>
                        <td>
                            <input type="text" style="${x['Average Banks']>=1500?"background:green":x['Average Banks']>=1000?"background:grey":"background:red"}" value="${x['Average Banks']>=1500?'Resale':x['Average Banks']>=1000?'Either':'Decline'}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Payment Coverage
                        </td>
                        <td>
                            <input type="text" value="${x['Payment Coverage']?x['Payment Coverage'].toFixed(2):0}" />
                        </td>
                        <td>
                            <input type="text" style="${x['Payment Coverage']>=1.75?"background:green":x['Payment Coverage']>=1.50?"background:grey":x['Payment Coverage']>=1.35?"background:lightblue":"background:red"}" value="${x['Payment Coverage']>=1.75?'Resale':x['Payment Coverage']>=1.50?'Either':x['Payment Coverage']>=1.35?'Retention':'Decline'}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Time In Business
                        </td>
                        <td>
                            <input type="text" name='Time in Business' value="${x['Time in Business']}"  />
                        </td>
                        <td>
                            <input type="text" name='Time in Business' style="${x['Time in Business']>=4?"background:green":x['Time in Business']>=3?"background:grey":x['Time in Business']>=2?"background:lightblue":"background:red"}" value="${x['Time in Business']>=4?'Resale':x['Time in Business']>=3?'Either':x['Time in Business']>=2?'Retention':'Decline'}"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OLV
                        </td>
                        <td>
                            <input type="text" name='OLV' value="${x['OLV']}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Age of Equipment
                        </td>
                        <td>
                            <input type="text" name='Age of Equipment' value="${x['Age of Equipment']?x['Age of Equipment']:0}"  />
                        </td>
                        <td>
                            <input type="text" style="${x['Age of Equipment']<=5?"background:green":x['Age of Equipment']>10?"background:lightblue":"background:grey"}" value="${x['Age of Equipment']<=5?'Resale':x['Age of Equipment']>10?'Retention':'Either'}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Additional Collateral Cash
                        </td>
                        <td>
                            <input value="${x['Additional Collateral Cash']?x['Additional Collateral Cash']:0+'%'}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            OFAC
                        </td>
                        <td>
                            <input type="text" name='OFAC' value="${x['OFAC']===0?'No Records':'Records Found'}" />
                        </td>
                        <td>
                            <input type="text" value="${x['OFAC']==0?'Accept':'Decline'}" style="${x['OFAC']===0?"background:green":"background:red"}" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Judgements
                        </td>
                        <td>
                            <input type="text" value="${x['Judgements']?x['Judgements']:"None"}" />
                        </td>
                        <td>
                            Tax Liens
                        </td>

                        <td>
                            <input type="text" value="${x['Tax Leans']?x['Tax Leans']:"None"}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PG Lawsuits
                        </td>
                        <td>
                            <input type="text" value="${x['Lawsuits']?x['Lawsuits']:"None"}" />
                        </td>
                        <td>
                            Slow Pay on Paynet
                        </td>
                        <td>
                            <input type="text" value="${x['Slow Pay']?x['Slow Pay']===1?x['Slow Pay']:"None":"None"}" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Losses on Paynet
                        </td>
                        <td>
                            <input type="text" value="${x['Loses on Paynet']?x['Loses on Paynet']==1?x['Loses on Paynet']:"None":"None"}" />
                        </td>
                    </tr>

                </tbody>
            </table>
            <table>
                <tbody>
                    <tr>
                        <td>
                            Credit Comments
                        </td>
                        <td>
                            <textarea name="Credit Comment" cols="50" rows="7">${x['Credit Comment']['Comment']}</textarea>
                        </td>
                    </tr>
                </tbody>
            </table>
            ${handlePG()}
            <br/>
            ${handleBanks()}
        </div>
    </body>
</html>`
}
