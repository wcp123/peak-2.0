module.exports = (x) =>{
    var handleDocFee = function(){
        var formatCurrency = require('format-currency')
        let docFee = Number(x['Doc Fee'].replace(/[^0-9.-]+/g,""));
        let inspectionFee = Number(x['Inspection Fee'].replace(/[^0-9.-]+/g,""));
        let opts = { format: '%s%v', symbol: '$' }

        return formatCurrency(docFee+inspectionFee, opts)


    }
    var handleFeetoSource = function(){
        var formatCurrency = require('format-currency')
        let amount = Number(x['Approved Amount'].replace(/[^0-9.-]+/g,""));
        if(amount<75000){
            return "Up to 10 points"
        }
        else if(amount<150000){
            return "Up to 8 points"
        }
        else{
            return "Up to 6 points"
        }
    } 
    var handlePG = function(){
        var html = ''
        var i = 0
        //need to render all parts of vendor for case of no Vendors
        x.pgInfo.map(pg =>{
            i+=1
            if(i!==0 && i%2===0){
                var name=pg['First Name'] + ' ' + (pg['Middle Name']?pg['Middle Name']:'') + ' ' + pg['Last Name']
                html+=`
                    <td>
                        <input value="${name}"  />
                    </td>
                </tr>
                `
            }else{
                var name=pg['First Name'] + ' ' + (pg['Middle Name']?pg['Middle Name']:'') + ' ' + pg['Last Name']
                html+=`
                <tr>
                    <td>
                    <input value="${name}" />
                    </td>
                
                `
            }
            
           
        })
        x.cgInfo.map(cg =>{
            i+=1
            if(i!==0 && i%2===0){
                var name=cg['DBA Name']?cg['DBA Name']:cg['Company Name']
                html+=`
                    <td>
                        <input value="${name}"  />
                    </td>
                </tr>
                `
            }else{
                var name=cg['DBA Name']?cg['DBA Name']:cg['Company Name']
                html+=`
                <tr>
                    <td>
                    <input value="${name}" />
                    </td>
                
                `
            }
            
           
        })
        if(i%2===1){
            html+="</tr>"
        }
        return html
    }
    
    var handleVendors = function(){
        var html = ''
        if(x.vendorList.length>=1){
            x.vendorList.map(vendor =>{
                html+=`
                    <tr>
                        <td>
                            ${vendor['Company Name']}
                        </td>
                        <td>
                            <input value="${vendor['Approved']?'Yes':'No'}" size="5"/>
                        </td>
                        <td>
                            <input value="${vendor['Approved for Prefunding']==1?'Yes':'No'}" size="5"/>
                        </td>
                        ${vendor['Approved for Prefunding']==1?`
                        <td>
                            <input value="${vendor['Approved for Prefunding']==1?vendor['Prefunding Terms']?vendor['Prefunding Terms']:'':''}" size="5"/>
                        </td>
                        `:''}
                        
                    </tr>
                `
            })
        }
        else{
            html+=`
                <tr>
                    <td>
                        No Vendor Listed
                    </td>
                </tr>
            `
        }
        return html
    }
    console.log('Approval Transmittal Broker')
    return(`<!DOCTYPE html>
    <html>
        <head>
            <title>${x.companyInfo['Compnay Name']+' Approval Transmittal'}.pdf</title>
            <link ref='stylesheet' type='text/css' href='ApprovalTransmittal.css' />
        </head>
        <body>
            <table>
                <tbody>
                    <tr style="background-color: lightgrey">
                        <td>
                            <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px">
                        </td>
                        <td sytle="fornt-size: 50px">
                            Approval Transmittal
                        </td>
                    </tr>
                    <tr>
                        <td>
                            TO: 
                            ${x.source['Company Name']}
                            <hr width="500px"/>
                        </td>
                        <td>
                            Application Number:
                            ${x.application['ID']}
                            <hr width="100px"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            E-mail
                            <hr width="500px" />
                        </td>
                        <td>
                            Approval Date:
                            ${x['Status Date']}
                            <hr width="100px" />
                        </td>
                    </tr>
                    <tr style="align-items: right">
                        <td></td>
                        <td>
                            Expiration Date
                            ${x['Credit Expire']}
                            <hr width="100px" />
                        </td>
    
                    </tr>
                    <tr>
                        <td class="Disclaimer" style="border: 10px solid gray" colspan="3">
                            This memo serves as a confirmation to you that below mentioned applicant has been approved for credit under
                            <br/>
                            the terms listed below. Any changes to these terms must be approved by credit prior to funding and subject to receipt
                            <br/>
                            and acceptance of all documentation.
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Company:</strong>
                            ${x.companyInfo['Company Name']}
                            <hr width="350px" />
                        </td>
                        <td>
                            <strong>DBA</strong>
                            ${x.companyInfo['DBA Name']?x.companyInfo['DBA Name']==='undefined'?'':x.companyInfo['DBA Name']==='null'?'':x.companyInfo['DBA Name']:''}
                            <hr width="350px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Collateral Description:</strong>
                            ${x.application['Collateral Description']}
                            <hr style="margin-left: 120px" width="350px" />
                        </td>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <td>
                                        Approved Amount
                                    </td>
                                    <td>
                                        <input value="${x['Approved Amount']}" />
                                    </td>
                                    <td>
                                        Frequency
                                    </td>
                                    <td>
                                        <input value="${x['Frequency']}" />
                                    </td>
                                    <td>
                                        Dedicated Rate
                                    </td>
                                    <td>
                                        <input value="${x['Discrectionary']?x['Discrectionary']:x['Calculated Yield']}">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Terms
                                    </td>
                                    <td>
                                        <input value="${x['Terms']}" />
                                    </td>
                                    <td>
                                        Down Payment
                                    </td>
                                    <td>
                                        <input value="${x['Down Payment']?x['Down Payment']:'$0.00'}" />
                                    </td>
                                    <td>
                                        Fee to Source
                                    </td>
                                    <td>
                                        <!-------This needs more info------->
                                        <input value="${handleFeetoSource()}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Documentation Fee
                                    </td>
                                    <td>
                                        <input value="${handleDocFee()}" />
                                    </td>
                                    <td>
                                        Advance Payment
                                    </td>
                                    <td>
                                        <input value="${x['Payment']?x['Payment']:''}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Inspection
                                    </td>
                                    <td>
                                        <input value="${x['Inspection']?'Required':'Not Required'}" />
                                    </td>
                                    <td>
                                        ACH
                                    </td>
                                    <td>
                                        <input value="${x['ACH Waived']?'Not Required':'Required'}" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Guarantors:
                                    </th>
                                </tr>
                                ${handlePG()}
                            </tbody>
                        </table>
                    </tr>                
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Vendors:
                                    </th>
                                    <th>
                                        Approved
                                    </th>
                                    <th>
                                        Approved for Prefunding
                                    </th>
                                    <th>
                                        Terms
                                    </th>
                                </tr>
                                ${handleVendors()}
                            </tbody>
                        </table>
                       
                    </tr>
                    <tr>
                        <td>
                            Credit Condition
                        </td>
                        <td>
                            <textarea cols="70" rows="7" style="text-align:left" >
                                ${x.additional['Credit Requirements']?x.additional['Credit Requirements']:''}
                            </textarea>
                        </td>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Items needed prior to Documentation request:
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        UCC Search
                                        <input type="text" class="sm" value="${x.additional['UCC Search']?'X':''}"/>
                                    </td>
                                    <td>
                                        Legal Land Description
                                        <input type="" class="sm" value="${x.additional['Legal Land Description']?'X':''}"/>
                                    </td>
                                    <td>
                                        Equipment Approval
                                        <input type="" class="sm" value="${x.additional['Equipment Approval']?'X':''}"/>
                                    </td>
                                    <td>
                                        Vendor Approval
                                        <input type="" class="sm" value="${x.additional['Vendor Approval']?'X':''}"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr>
    
                    </tr>
                    <tr>
                        <td>
                            Dedicated Funding will generate the documentation for this transaction. Please complete attached documentation
                            <br/>
                            request form and submit along with:
                        </td>
                        <td>
                            <ul>
                                <li>
                                    Time in business validation (if SOS was not provided with credit submission)
                                </li>
                                <li>
                                    Articles of Incorporation (Bylaws maybe requested) Maybe submitted at closing
                                </li>
                                <li>
                                    Vendor invoice or Quote (must include prefunding terms if applicable)
                                </li>
                                <li>
                                    Pricing run (T-value) if available
                                </li>
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
    </html>`)
}