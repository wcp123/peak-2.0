const { format } = require('mysql2/promise')
const { application } = require('express')
const creditAdjustment = 1.3
module.exports = (x) =>{
    var formatCurrency = require('format-currency')
    var adjustedAmount = (amount) =>{
        var number = Number(String(amount).replace(/[^0-9-.]+/g, ''))
        return String('$'+formatCurrency((number*creditAdjustment)))
    }
    var totalFacility =(approvedAmount, downPayment)=>{
        var amount = Number(String(approvedAmount).replace(/[^0-9-.]+/g, ''))
        var down = Number(String(downPayment).replace(/[^0-9-.]+/g, ''))
        return String('$'+formatCurrency((amount-down)))
    }
    var handleCgs = ()=>{
        var html =''
        if(x['cgInfo'].length>=1){
            html+=`
            <table>
                <tbody>
                    <tr>
                        <th style="background-color: #ffda5e; border: black">
                            Corporate Gaurantors
                        </th>
                        <th>
                            DBA Name
                        </th>
                    </tr>
            `
            x['cgInfo'].map(cg=>{
                html+=`
                <tr>
                    <td>

                        <input value="${cg['Company Name']}"/>
                    </td>
                    <td>
                        <input value="${cg['DBA Name']?cg['DBA Name']:''}/>
                    </td>
                </tr>
                `
            })


            html+=`
            </tbody>
            </table>`
        }
        return html
    }
    var pg = function(){
        var html = `<tr  style="background-color: #ffda5e; border: black">
                        <th >
                            Guarantors:
                        </th>
                        <th>
                            Ownership %
                        </th>
                        <th style="border-left: black">
                            Experian CRA Score
                        </th>
                        <th style="border-left: black">
                            Trade Lines
                        </th>
                        <th style="border-left: black">
                            Years in Bureau
                        </th>
                    </tr>`
            x.pgInfo.map((pg)=>{
            html += (`<tr>
                <td style="text-align:left">
                    ${pg['First Name']} ${pg['Last Name']}
                </td>
                <td style="text-align:right">
                   ${ pg['Ownership']}
                </td>
                <td style="text-align:right">
                    ${pg['FICO']}
                </td>
                <td style="text-align:right">
                    ${pg['Trade Line']?pg['Trade Line']:''}
                </td>
                <td style="text-align:right">
                    ${pg['Years in Bureau']}
                </td>
            </tr>`)  
        })
        return html
    }
    var Tib = function(){
        let tib = x.companyInfo['Date Established']
        let diff = ((Date.now()) - Date.parse(tib))/1000
        let years = 0
        let days =0
        if (diff >= (365.25 * 86400)){
            years = Math.floor(diff / (362.25*86400))
            diff -= years * 365.25 * 86400
        }
        if(diff >= (86400 * 30.42)){
            days = Math.floor(diff / (86400 * 30.42))
        }
        years += days/(86400 * 30.42)
        years = years.toFixed(2)
        return years
    }
    var averageBank = function(){
        let i = 0
        let average = 0
        x.bankInfo.map( statement=>{
            i+=1
            let balance = Number(statement['Balance'].replace(/[^0-9.-]+/g,""));
            average += balance
        })
        let opts = { format: '%s%v', symbol: '$' }
        return formatCurrency((average/i), opts)
    }
    // var handleTotalFacilaties = function(){
    //     let loan = Number(String(x['Approved Amount']).replace(/[^0-9.-]+/g,""));
    //     let down = Number((x['Down Payment']+'').replace(/[^0-9.-]+/g,""));
    //     return formatCurrency(loan-down)
    // }
    console.log('Approval')
    return(
`<!DOCTYPE html>
<html>
    <head>
        <title>${x.companyInfo['Compnay Name']}.pdf</title>
        <link ref='stylesheet' type='text/css' href='test.css' />
    </head>
    <body>
        <div class='invoice-box'>
            <h1> Approval Memo </h1>
            <img src="file:///D:/Repo/API/peak-2.0/Documents/High-res-JPG.JPG" height="100px" width="400px" style=" 1px 1px 10px 400px " >

            <table cellspacing='0' cellpadding='0'  >
                <tbody >
                    
                    <tr class='Approval-Sign'>
                        <table>
                                <tbody>
                                    <tr>
                                        <td >
                                            ${x.staffName['Credit Officer']}
                                            <hr width='500px' />
                                        </td>
                                        <td>
                                            Date
                                            <hr width='200px' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            ${x.staffName['Second Approval']}
                                            <hr width='500px' />
                                        </td>
                                        <td>
                                            Date
                                            <hr width='200px' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td >
                                            TBAS
                                            <hr width='500px' />
                                        </td>
                                        <td>
                                            Date
                                            <hr width='200px' />
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <table>
                            <tbody>
                                <tr class="justify-center"> 
                                    <td>
                                        ${adjustedAmount(x['Approved Amount'])}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        ${x.companyInfo['Company Name']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <textarea sytle="align: center" cols="30" rows="3" >${x.application['Collateral Description']?x.application['Collateral Description']: 'Description Not Given'}</textarea>
                                    </td>    
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr>
                        <table class='Specifics-Table'>
                            <tbody>
                                <tr class='Specifics' length='100%' style='background-color: #ffda5e; border: black'>
                                        <th style="max-width: 100%">
                                            Specifics: 
                                        </th>
                                </tr>
                                <tr>
                                    <td>
                                        Approved Amt
                                    </td>
                                    <td>
                                        <input value="${adjustedAmount(x['Approved Amount'])}" style="max-width: 100px" />
                                    </td>
                                    <td>
                                        Terms
                                    </td>
                                    <td>
                                        <input value="${x['Terms']}" style="max-width: 60px; align-content: center" />
                                    </td>
                                    <td>
                                        Loan Grade
                                    </td>
                                    <td>
                                        <input value="${x['Loan Grade']?x['Loan Grade']:"N/A"}" style="max-width: 60px; align-content: center"" />
                                    </td>
                                    <td>
                                        Dedicated Interest Rate
                                    </td>
                                    <td>
                                        <input value="${x['Discrectionary']?x['Discrectionary']:x['Calculated Yield']}" style="max-width: 60px; align-content: center""/>   
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Down Payment
                                    </td>
                                    <td>
                                        <input value="${x['Down Payment']?x['Down Payment']:"$0.00"}" style="max-width: 100px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total Facility
                                    </td>
                                    <td>
                                        <input value="${totalFacility(adjustedAmount(x['Approved Amount']), x['Down Payment'])}" />
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr class='Guarantors-Keys'>
                        <table>
                            <tbody>
                                ${pg()}
                            </tbody>
                        </table>
                    </tr>
                    <tr>
                        ${handleCgs()}
                    </tr>

                    <tr class='Paynet'>
                        <table>
                            <tbody>
                                <tr style="background: #ffda5e; border: black; width: 100%">
                                    <th>
                                        Paynet: 
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Master Score
                                    </td>
                                    <td>
                                        <input value="${x['Paynet']}" style="max-width: 60px; align-content: center""/>
                                    </td>
                                    <td>
                                        Percentile
                                    </td>
                                    <td>
                                        <input value="${x['Percentile']?x['Percentile']:''}" style="max-width: 60px; align-content: center""/>
                                    </td>
                                    <td>
                                        Comparable Term Credit as a % of Request
                                    </td>
                                    <td>
                                        <input value="${x['Comp Credit']}" style="max-width: 60px; align-content: center" />
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr class='Trans-Guidelines'>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Transactions Guidelines and Requirements
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Time in Business
                                    </td>
                                    <td>
                                        ${Tib()}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Transaction Term Months
                                    </td>
                                    <td>
                                        ${x['Terms']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Average Bank Balance
                                    </td>
                                    <td>
                                        <!---This needs reviewed--->
                                        ${averageBank()}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Lowest FICO
                                    </td>
                                    <td>
                                        ${x['Lowest FICO']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Paynet
                                    </td>
                                    <td>
                                        ${x['Paynet']}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        SBSS
                                    </td>
                                    <td>
                                        ${x['SBSS']?x['SBSS']:''}
                                    </td>    
                                </tr>
                                <tr>
                                    <td>
                                        OFAC
                                    </td>
                                    <td>
                                        ${x['OFAC']?x['OFAC']==1?"Record Found":"No Records":"No Records"}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                    <tr>
                        <td>
                            Credit Comment
                        </td>
                        <td>
                            <textarea name="Credit Comment" cols="50" rows="7">${x['Credit Comment']['Comment']}</textarea>
                        </td>
                    </tr>
                    <tr class='Personnel'>
                        <table>
                            <tbody>
                                <tr>
                                    <th>
                                        Personnel
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        Analyst
                                    </td>
                                    <td>
                                        <input value="${x.staffName['Credit Officer']}" />
                                    </td>
                                    <td>
                                        BDO
                                    </td>
                                    <td>
                                        <input value="${x.staffName['BDO']}" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>

</html>
    `)




}

