var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.commissions`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
        
    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.commissions WHERE \`Doc ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.commissions set \`lock\` = 1 WHERE ID =  ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }
        
        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.commissions set lock = 0 WHERE ID =  ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }
        
        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.commissions `;
            var options 
            if(data.staffID){
                sql += "(\`Doc ID\`,\`Staff ID\`,\`Type\`, \`Amount\`,\`Date\`) "+"VALUES ";
                sql+= "(?,?,?,?,?)"
                options = [data.docID, data.staffID, data.type, data.amount, data.date]
            }else if(data.vendorID){
                sql += "(\`Doc ID\`,\`Vendor ID\`,\`Type\`, \`Amount\`,\`Date\`) "+"VALUES ";
                sql+= "(?,?,?,?)"
                options = [data.docID, data.vendorID, data.type, data.amount, data.date]
            }else if(data.sourceID){
                sql += "(\`Doc ID\`,\`Source ID\`, \`Type\`, \`Amount\`,\`Date\`) "+"VALUES ";
                sql+= "(?,?,?,?,?)"
                options = [data.docID, data.sourceID, data.type, data.amount, data.date]
            }
            
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.commissions SET `;
            var options =[]
            //repeat the following line for each column in the table;
            
            if (data.docID){
                options.push(data.docID)
                sql+= (" `Doc ID` = ?, "); //only strings need the single quote
            
            }
            if (data.staffID){
                options.push(data.staffID)
                sql+= (" `Staff ID` = ?, "); //only strings need the single quote
            
            }
            if (data.vendorID){
                options.push(data.vendorID)
                sql+= (" `Vendor ID` = ?, "); //only strings need the single quote
            
            }if (data.sourceID){
                options.push(data.sourceID)
                sql+= (" `Source ID` = ?, "); //only strings need the single quote
            
            }if (data.amount){
                options.push(data.amount)
                sql+= (" `Amount` = ?, "); //only strings need the single quote
            
            }
            if (data.date){
                options.push(data.date)
                sql+= (" `Date` = ?, "); //only strings need the single quote
            
            }
            
            
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            options.push(data.id)
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.commissions`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};