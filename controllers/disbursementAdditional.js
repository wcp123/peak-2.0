var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`disbursement additional\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`disbursement additional\` WHERE \`Doc ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getID = function (req,resp, name){
    db.executeSql(`SELECT ID FROM ${settings.sqlConfig.database}.\`disbursement additional\` WHERE \`Doc ID\` = '?'`,[name], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req, resp, data);
        }
    })
}
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`disbursement additional\` set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`disbursement additional\` set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`disbursement additional\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.docID){
                sql+="`Doc ID`, "
                options.push(data.docID)
                sqlEnd+=`?, `
            }
            if(data.vendorID){
                sql+="`Vendor ID`, "
                options.push(data.vendorID)
                sqlEnd+=`?, `
            }
            if(data.docFee){
                sql+="`Doc Fee`, "
                options.push(data.docFee)
                sqlEnd+=`?, `
            }
            if(data.vendorCommission){
                sql+="`Vendor Commission`, "
                options.push(data.vendorCommission)
                sqlEnd+=`?, `
            }
            if(data.total){
                sql+="`Total`, "
                options.push(data.total)
                sqlEnd+=`?, `
            }
            if(data.releaseDate){
                sql+="`Release Date`, "
                options.push(data.releaseDate)
                sqlEnd+=`?, `
            }
            if(data.vendorReference){
                sql+="`Vendor Reference`, "
                options.push(data.vendorReference)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });

        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`disbursement additional\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.docID){
                options.push(data.docID )
                sql+= (" `Doc ID` = ?,"); //only strings need the single quote

            }if (data.vendorID){
                options.push(data.vendorID)
                sql+= (" `Vendor ID` = ?,"); //only strings need the single quote

            }if (data.docFee){
                options.push(data.docFee)
                sql+= (" `Doc Fee` = ?,"); //only strings need the single quote

            }
            if(data.dob){
            //     i    f(data.dob!=undefined&&data.dob!=null){
                var date = String(data.dob).indexOf('T')===-1?data.dbo:String(data.dob).split('T')[0]
                options.push(date)//         
                sql+=(" `Date of Birth` = ?,")
            //     }

            }
            if (data.vendorCommission){
                options.push(data.vendorCommission)
                sql+= (" `Vendor Commission` = ?,"); //only strings need the single quote

            }if (data.total){
                options.push(data.total)
                sql+= (" Total = ?,"); //only strings need the single quote

            }if (data.releaseDate){
                options.push(data.releaseDate)
                sql+= (" `Release Date` = ?,"); //only strings need the single quote

            }
            if(data.vendorReference){
                options.push(data.vendorReference)
                sql+=(" `Vendor Reference` = ?,")
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            options.push(data.id)
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`disbursement additional\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id], function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
