var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql("SELECT * FROM ${settings.sqlConfig.database}.'broker commisions'", [],function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.'broker commisions' WHERE ID = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        // if (data){
        //     var sql = "INSERT INTO ${settings.sqlConfig.database}.'broker commisions' ";
        //     sql += "('Doc ID', 'Amount', 'Broker ID') "+"VALUES ";
        //     sql+= util.format("(%d,%O,%d)",
        //                     data.docID, data.amount, data.brokerID);
        //     db.executeSql(sql,function (x, err){
        //         if(err){
        //             msg.show500(req, resp, err);
        //         }
        //         else{
        //             msg.sendJSON(req,resp, x.insertId)
        //         }
        //     });
        // }
        // else{
        //     throw new Error("Input not valid");
        // }
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.'broker commisions' (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.docID){
                sql+="`Doc ID`, "
                options.push(data.docID)
                sqlEnd+=`?, `
            }
            if(data.amount){
                sql+="`Amount`, "
                options.push(data.amount)
                sqlEnd+=`?, `
            }
            if(data.brokerID){
                sql+="`Broker ID`, "
                options.push(data.brokerID)
                sqlEnd+=`?, `
            }

            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var options =[]
            var sql = `UPDATE ${settings.sqlConfig.database}.'broker commisions' SET `;
            //repeat the following line for each column in the table;

            if (data.docID){
                options.push(data.docID)
                sql+= (" 'Doc ID' = ?,"); //only strings need the single quote

            }if (data.amount){
                options.push(data.amount)
                sql+= (" 'Amount' = ?,"); //only strings need the single quote

            }if (data.brokerID){
                options.push(data.brokerID)
                sql+= (" 'Broker ID' = ?,"); //only strings need the single quote

            }


            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ${data.id}`;
            //
            db.executeSql(sql,function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.'broker commisions'`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
