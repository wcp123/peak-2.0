var db = require("../db");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`credit additional\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
        
    });
};
//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`credit additional\` WHERE \`App ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`credit additional\` set 'lock' = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }
        
        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`credit additional\` set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }
        
        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`credit additional\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.appID){
                sql+="`App ID`, "
                options.push(data.appID)
                sqlEnd+=`?, `
            }
            if(data.legalLandDescription){
                sql+="`Legal Land Description`, "
                options.push(data.legalLandDescription)
                sqlEnd+=`?, `
            }
            if(data.titleMSO){
                sql+="`Title/MSO`, "
                options.push(data.titleMSO)
                sqlEnd+=`?, `
            }
            if(data.uccSearch){
                sql+="`UCC Search`, "
                options.push(data.uccSearch)
                sqlEnd+=`?, `
            }
            if(data.creditRequirements){
                sql+="`Credit Requirements`, "
                var creditRequirements = String(data.creditRequirements).replace(`"`,`'`)
                options.push(creditRequirements)
                sqlEnd+=`?, `
            }
            if(data.equipmentApproval){
                sql+="`Equipment Approval`, "
                options.push(data.equipmentApproval)
                sqlEnd+=`?, `
            }
            if(data.leaseLoanAgreement){
                sql+="`Lease or Loan Agreement`, "
                options.push(data.leaseLoanAgreement)
                sqlEnd+=`?, `
            }
            if(data.vendorInvoice){
                sql+="`Vendor Invoice`, "
                options.push(data.vendorInvoice)
                sqlEnd+=`?, `
            }
            if(data.assignment){
                sql+="`Assignment`, "
                options.push(data.assignment)
                sqlEnd+=`?, `
            }
            if(data.exhibitA){
                sql+="`Exhibit A`, "
                options.push(data.exhibitA)
                sqlEnd+=`?, `
            }
            if(data.noticeOfAssignment){
                sql+="`Notice of Assignment`, "
                options.push(data.noticeOfAssignment)
                sqlEnd+=`?, `
            }
            if(data.deliveryAndAcceptance){
                sql+="`Delivery and Acceptance`, "
                options.push(data.deliveryAndAcceptance)
                sqlEnd+=`?, `
            }
            if(data.uccFixtureFiling){
                sql+="`UCC Fixture Filing`, "
                options.push(data.uccFixtureFiling)
                sqlEnd+=`?, `
            }
            if(data.copyChecksToVendor){
                sql+="`Copy checks to vendor`, "
                options.push(data.copyChecksToVendor)
                sqlEnd+=`?, `
            }
            if(data.corporateResolution){
                sql+="`Corporate Resolution`, "
                options.push(data.corporateResolution)
                sqlEnd+=`?, `
            }
            if(data.copiesOfMSO){
                sql+="`Copies of MSO/Title`, "
                options.push(data.copiesOfMSO)
                sqlEnd+=`?, `
            }
            if(data.vendorApproval){
                sql+="`Vendor Approval`, "
                options.push(data.vendorApproval)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`credit additional\` SET `;
            var options = []
            //repeat the following line for each column in the table;
            if(data.appID){
                options.push(data.appID)
                sql+=` \`App ID\` = ?,`
            }
            if(data.legalLandDescription){
                options.push(data.legalLandDescription)
                sql+=` \`Legal Land Description\` =  ?,`
            }
            if(data.titleMSO){
                options.push(data.titleMSO)
                sql+=` \`Title/MSO\` = ?,`
            }
            if(data.uccSearch){
                options.push(data.uccSearch)
                sql+=` \`UCC Search\` = ?,`
            }
            if(data.equipmentApproval){
                options.push(data.equipmentApproval)
                sql+=` \`Equipment Approval\` = ?,`
            }
            if(data.creditRequirements){
                options.push(data.creditRequirements)
                sql+=` \`Credit Requirements\` = ?,`
            }
            if(data.leaseLoanAgreement){
                options.push(data.leaseLoanAgreement)
                sql+=` \`Lease or Loan Agreement\` = ?,`
            }
            if(data.vendorInvoice){
                options.push(data.vendorInvoice)
                sql+=` \`Vendor Invoice\` = ?,`
            }
            if(data.assignment){
                options.push(data.assignment)
                sql+=` \`Assignment\` = ?,`
            }
            if(data.exhibitA){
                options.push(data.exhibitA)
                sql+=` \`Exhibit A\` = ?,`
            }
            if(data.noticeOfAssignment){
                options.push(data.noticeOfAssignment)
                sql+=` \`Notice of Assignment\` = ?,`
            }
            if(data.deliveryAndAcceptance){
                options.push(data.deliveryAndAcceptance)
                sql+=` \`Delivery and Acceptance\` = ?,`
            }
            if(data.uccFixtureFiling){
                options.push(data.uccFixtureFiling)
                sql+=` \`UCC Fixture Filing\` = ?,`
            }
            if(data.copyChecksToVendor){
                options.push(data.copyChecksToVendor)
                sql+=` \`Copy checks to vendor\` = ?,`
            }
            if(data.corporateResolution){
                options.push(data.corporateResolution)
                sql+=` \`Corporate Resolution\` = ?,`
            }
            if(data.copiesOfMSO){
                options.push(data.copiesOfMSO)
                sql+=` \`Copies of MSO/Title\` = ?,`
            }
            if(data.vendorApproval){
                options.push(data.vendorApproval)
                sql+=`\`Vendor Approval\` = ?,`
            }
            
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ${data.id}`;
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`credit additional\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id], function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};