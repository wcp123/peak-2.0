var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`corporate gaurantor\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`Select * from ${settings.sqlConfig.database}.\`corporate gaurantor\` Where \`ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        // if (data){
        //     var date = String(data.dateEstablished).indexOf('T')===-1?data.dateEstablished:String(data.dateEstablished).split('T')[0]
        //     var sql = "INSERT INTO ${settings.sqlConfig.database}.`corporate gaurantor` ";
        //     sql += "(`Company Name`, `Signors Name`, `Signors Title`, Address, `SOS Type`, `Date Established`) "+"VALUES ";
        //     sql+= util.format("(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")",
        //             data.companyName, data.signorsName, data.signorsTitle, data.address, data.sosType,date)
        //     db.executeSql(sql,function (body, err){
        //         if(err){
        //             msg.show500(req, resp, err);
        //         }
        //         else{
        //             msg.sendJSON(req,resp, body.insertId)
        //         }
        //     });
        // }
        // else{
        //     throw new Error("Input not valid");
        // }
        console.log(data)
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`corporate gaurantor\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.companyName){
                sql+="`Company Name`, "
                options.push(data.companyName)
                sqlEnd+=`?, `
            }
            if(data.signorsName){
                sql+="`Signors Name`, "
                options.push(data.signorsName)
                sqlEnd+=`?, `
            }
            if(data.address){
                sql+="`Address`, "
                options.push(data.address)
                sqlEnd+=`?, `
            }
            if(data.sosType){
                sql+="`SOS Type`, "
                options.push(data.sosType)
                sqlEnd+=`?, `
            }
            if(data.dateEstablished){
                sql+="`Date Established`, "
                options.push(data.dateEstablished)
                sqlEnd+=`?, `
            }
            if(data.city){
                sql+="`City`, "
                options.push(data.city)
                sqlEnd+=`?, `
            }
            if(data.state){
                sql+="`State`, "
                options.push(data.state)
                sqlEnd+=`?, `
            }
            if(data.zip){
                sql+="`Zip`, "
                options.push(data.zip)
                sqlEnd+=`?, `
            }
            if(data.fedID){
                sql+="`Federal ID`, "
                options.push(data.fedID)
                sqlEnd+=`?, `
            }
            if(data.jurisdiction){
                sql+="`Jurisdiction`, "
                options.push(data.jurisdiction)
                sqlEnd+=`?, `
            }
            if(data.phoneNumber){
                sql+="`Phone Number`, "
                options.push(data.phoneNumber)
                sqlEnd+=`?, `
            }


            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    throw new Error(err)
                }
                else{
                    try{
                        if (data){
                            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`application to cg\` `;
                            sql += "(`Application ID`, `CG ID`) "+"VALUES ";
                            sql+= "(?,?)" 
                            console.log(data)
                            var options = [data.appID, x.insertId]
                            db.executeSql(sql,options, function (body, err){
                                if(err){
                                   throw new Error(err)
                                }
                                else{
                                    msg.sendJSON(req,resp, x.insertId)
                                }
                            });          
                        }
                        else{
                            throw new Error("Input not valid");
                        }
                    }
                    catch (ex) {
                        throw new Error(ex)
                
                    }
                    // msg.sendJSON(req,resp, x.insertId)
                }
            });

        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        console.log(ex)
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`corporate gaurantor\` SET `;
            var options = []
            //repeat the following line for each column in the table;

            if (data.companyName){
                options.push(data.companyName)
                sql+= (" `Company Name` = ?,"); //only strings need the single quote

            }
            if (data.signorsName ){
                options.push(data.signorsName)
                sql+= (" `Signors Name` = ?,"); //only strings need the single quote

            }if (data.address){
                options.push(data.address)
                sql+=(" `Address` = ?,")
            }if (data.sosType){
                options.push(data.sosType)
                sql+=(" `SOS Type` = ?,")
            }if (data.dateEstablished){
                var date = String(data.dateEstablished).indexOf('T')===-1?data.dateEstablished:String(data.dateEstablished).split('T')[0]
                options.push(date)
                sql+=(" `Date Established` = ?,")
            }if(data.city){
                options.push(data.city)
                sql+=(" `City` = ?,")
            }
            if(data.state){
                options.push(data.state)
                sql+=(" `State` = ?,")
            }
            if(data.zip){
                options.push(data.zip)
                sql+=(" `Zip` = ?,")
            }
            if(data.fedID){
                options.push(data.fedID)
                sql+=(" `Federal ID` = ?,")
            }
            if(data.jurisdiction){
                options.push(data.jurisdiction)
                sql+=(" `Jurisdiction` = ?,")
            }
            if(data.phoneNumber){
                options.push(data.phoneNumber)
                sql+=(" `Phone Number` = ?,")
            }
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID =?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`corporate gaurantor\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id], function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
