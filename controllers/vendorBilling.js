var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`vendor billing\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the vendors.js file in the controllers module
exports.getBill = function (req, resp, vendorID){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`vendor billing\` WHERE \`ID\` = ?`,[vendorID], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.get = function (req, resp, vendorID){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`vendor billing\` WHERE \`Vendor ID\` = ?`,[vendorID], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`vendor billing\` set \`lock\` = 1 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`vendor billing\` set lock = 0 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`vendor billing\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.docID){
                sql+="`Doc ID`, "
                options.push(data.docID)
                sqlEnd+=`?, `
            }
            if(data.vendorID){
                sql+="`Vendor ID`, "
                options.push(data.vendorID)
                sqlEnd+=`?, `
            }
            if(data.wireOrAch){
                sql+="`Wire or ACH`, "
                options.push(data.wireOrAch)
                sqlEnd+=`?, `
            }
            if(data.accountNumber){
                sql+="`Account Number`, "
                options.push(data.accountNumber)
                sqlEnd+=`?, `
            }
            if(data.routingNumber){
                sql+="`Routing Number`, "
                options.push(data.routingNumber)
                sqlEnd+=`?, `
            }
            if(data.bankName){
                sql+="`Bank Name`, "
                options.push(data.bankName)
                sqlEnd+=`?, `
            }
            if(data.accountHoldersName){
                sql+="`Account Holders Name`, "
                options.push(data.accountHoldersName)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`vendor billing\` SET `;
            //repeat the following line for each column in the table;
            if(data.docID){
                sql+=(" `Doc ID` = ?,")
                options.push(data.docID)
            }
            if(data.vendorID){
                sql+=(" `Vendor ID` = "+data.vendorID+",")
                options.push(data.vendorID)
            }
            if (data.accountNumber){
                sql+= (" `Account Number` = ?,"); //only strings need the single quote
                options.push(data.accountNumber)

            }
            if (data.routingNumber){
                sql+= (" `Routing Number` = ?,"); //only strings need the single quote
                options.push(data.routingNumber)

            }if(data.bankName){
                sql+=(" `Bank Name` = ?,")
                options.push(data.bankName)
            }if(data.wireOrAch){
                sql+=(" `Wire or ACH` = ?,")
                options.push(data.wireOrAch)
            }
            if(data.accountHoldersName){
                sql+=(" `Account Holders Name` = ?,")
                options.push(data.accountHoldersName)
            }


            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`vendor billing\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
