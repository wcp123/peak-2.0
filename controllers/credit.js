var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require("../settings")
exports.getList = function(req, resp){
    db.executeSql("SELECT * FROM ${settings.sqlConfig.database}.credit",[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.credit WHERE \`App ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.credit set 'lock' = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.credit set lock = 0 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`credit\` (`
            var sqlEnd = ") VALUES (";
            var options =[]
            if(data.appID){
                sql+="`App ID`, "
                options.push(data.appID)
                sqlEnd+=`?, `
            }

            if(data.terms){
                sql+="`Terms`, "
                options.push(data.terms)
                sqlEnd+=`?, `
            }
            if(data.paynet){
                sql+="`Paynet`, "
                options.push(data.paynet)
                sqlEnd+=`?, `
            }
            if(data.additionalColateralCash){
                sql+="Additional Colateral Cash`, "
                options.push(data.additionalColateralCash)
                sqlEnd+=`?, `
            }
            if(data.estimatedPayment){
                sql+="`Estimated Payment`, "
                options.push(data.estimatedPayment)
                sqlEnd+=`?, `
            }
            if(data.creditExpires){
                sql+="`Credit Expire`, "
                options.push(data.creditExpires)
                sqlEnd+=`?, `
            }
            if(data.creditOfficer){
                sql+="`Credit Officer`, "
                options.push(data.creditOfficer)
                sqlEnd+=`?, `
            }
            if(data.secondApproval){
                sql+="`Second Approval`, "
                options.push(data.secondApproval)
                sqlEnd+=`?, `
            }
            if(data.descretionary){
                sql+="`Discrectionary`, "
                options.push(data.descretionary)
                sqlEnd+=`?, `
            }
            if(data.eotOptions){
                sql+="`EOT Options`, "
                options.push(data.eotOptions)
                sqlEnd+=`?, `
            }
            if(data.securityDeposite){
                sql+="`Security Deposite`, "
                options.push(data.securityDeposite)
                sqlEnd+=`?, `
            }
            if(data.payment){
                sql+="`Payment`, "
                options.push(data.payment)
                sqlEnd+=`?, `
            }
            if(data.frequency){
                sql+="`Frequency`, "
                options.push(data.frequency)
                sqlEnd+=`?, `
            }
            if(data.grid){
                sql+="`Grid`, "
                options.push(data.grid)
                sqlEnd+=`?, `
            }
            if(data.docFee){
                sql+="`Doc Fee`, "
                options.push(data.docFee)
                sqlEnd+=`?, `
            }
            if(data.downPayment){
                sql+="`Down Payment`, "
                options.push(data.downPayment)
                sqlEnd+=`?, `
            }
            if(data.compCredit){
                sql+="`Comp Credit`, "
                options.push(data.compCredit)
                sqlEnd+=`?, `
            }
            if(data.ofac){
                sql+="`OFAC`, "
                options.push(data.ofac)
                sqlEnd+=`?, `
            }if(data.policyException){
                sql+="`Policy Exception`, "
                options.push(data.policyException)
                sqlEnd+=`?, `
            }
            if(data.achWaived){
                sql+="`ACH Waived`, "
                options.push(data.achWaived)
                sqlEnd+=`?, `
            }
            if(data.olv){
                sql+="`OLV`, "
                options.push(data.olv)
                sqlEnd+=`?, `
            }if(data.feeToSource){
                sql+="`Fee to Source`, "
                options.push(data.feeToSource)
                sqlEnd+=`?, `
            }
            if(data.inspection){
                sql+="`Inspection`, "
                options.push(data.inspection)
                sqlEnd+=`?, `
            }
            if(data.docRequestID){
                sql+="`Doc Request ID`, "
                options.push(data.docRequestID)
                sqlEnd+=`?, `
            }if(data.status){
                sql+="`Status`, "
                options.push(data.status)
                sqlEnd+=`?, `
            }
            if(data.statusDate){
                sql+="`Status Date`, "
                options.push(data.statusDate)
                sqlEnd+=`?, `
            }
            if(data.totalWeight){
                sql+="`Total Weight`, "
                options.push(data.totalWeight)
                sqlEnd+=`?, `
            }if(data.calculatedYield){
                sql+="`Calculated Yield`, "
                options.push(data.calculatedYield)
                sqlEnd+=`?, `
            }
            if(data.sbss){
                sql+="`SBSS`, "
                options.push(data.sbss)
                sqlEnd+=`?, `
            }
            if(data.loanGrade){
                sql+="`Loan Grade`, "
                options.push(data.loanGrade)
                sqlEnd+=`?, `
            }if(data.lowestFICO){
                sql+="`Lowest FICO`, "
                options.push(data.lowestFICO)
                sqlEnd+=`?, `
            }
            
            if(data.trucks){
                sql+="`Truck`, "
                options.push(data.trucks)
                sqlEnd+=`?, `
            }if(data.interviewed){
                sql+="`Interviewed`, "
                options.push(data.interviewed)
                sqlEnd+=`?, `
            }
            if(data.additionalColateralValue){
                sql+="`Additional Collateral Value`, "
                options.push(data.additionalColateralValue)
                sqlEnd+=`?, `
            }
            if(data.approvedAmount){
                sql+="`Approved Amount`, "
                options.push(data.approvedAmount)
                sqlEnd+=`?, `
            }if(data.lawsuits){
                sql+="`Lawsuits`, "
                options.push(data.lawsuits)
                sqlEnd+=`?, `
            }
            if(data.taxLeans){
                sql+="`Tax Leans`, "
                options.push(data.taxLeans)
                sqlEnd+=`?, `
            }
            if(data.judgements){
                sql+="`Judgements`, "
                options.push(data.judgements)
                sqlEnd+=`?, `
            }if(data.lowestRevolving){
                sql+="`Lowest Revolving`, "
                options.push(data.lowestRevolving)
                sqlEnd+=`?, `
            }
            if(data.percentile){
                sql+="`Percentile`, "
                options.push(data.percentile)
                sqlEnd+=`?, `
            }
            if(data.purpose){
                sql+="`Purpose`, "
                options.push(data.purpose)
                sqlEnd+=`?, `
            }if(data.ageOfEquipment){
                sql+="`Age of Equipment`, "
                options.push(data.ageOfEquipment)
                sqlEnd+=`?, `
            }
            if(data.slowPay){
                sql+="`Slow Pay`, "
                options.push(data.slowPay)
                sqlEnd+=`?, `
            }
            if(data.losesOnPaynet){
                sql+="`Loses on Paynet`, "
                options.push(data.losesOnPaynet)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.credit SET `;
            var options = []
            //repeat the following line for each column in the table;

            if (data.appID){
                options.push(data.appID)
                sql+= (" `App ID` = ?,"); //only strings need the single quote

            }if (data.terms){
                options.push(data.terms)
                sql+= (" Terms = ?,"); //only strings need the single quote

            }if (data.paynet){
                options.push(data.payment)
                sql+= (" `Paynet` = ?,"); //only strings need the single quote

            }
            if (data.additionalColateralCash){
                options.push(data.additionalColateralCash)
                sql+= (" `Additional Colateral Cash` = ?,"); //only strings need the single quote

            }if (data.estimatedPayment){
                options.push(data.estimatedPayment)
                sql+= (" `Estimated Payment` = ?,"); //only strings need the single quote

            }if (data.creditOfficer){
                options.push(data.creditOfficer)
                sql+= (" `Credit Officer` = ?,"); //only strings need the single quote

            }if (data.secondApproval){
                options.push(data.secondApproval)
                sql+= (" `Second Approval` = ?,"); //only strings need the single quote

            }if (data.descretionary){
                options.push(data.descretionary)
                sql+= (" `Discrectionary` = ?,"); //only strings need the single quote

            }if (data.eotOptions){
                options.push(data.eotOptions)
                sql+= (" `EOT Options` = ?,"); //only strings need the single quote

            }if (data.securityDeposite){
                options.push(data.securityDeposite)
                sql+= (" `Security Deposite` = ?,"); //only strings need the single quote

            }if (data.payment){
                options.push(data.payment)
                sql+= (" Payment = ?,"); //only strings need the single quote

            }if (data.frequency){
                options.push(data.frequency)
                sql+= (" Frequency = ?,"); //only strings need the single quote

            }if (data.grid){
                options.push(data.grid)
                sql+= (" Grid = ?,"); //only strings need the single quote

            }if (data.docFee){
                options.push(data.docFee)
                sql+= (" `Doc Fee` = ?,"); //only strings need the single quote

            }if (data.downPayment){
                options.push(data.downPayment)
                sql+= (" `Down Payment` = ?,"); //only strings need the single quote

            }if (data.compCredit){
                options.push(data.compCredit)
                sql+= (" `Comp Credit` = ?,"); //only strings need the single quote

            }if (data.OFAC){
                options.push(data.OFAC)
                sql+= (" OFAC = ?,"); //only strings need the single quote

            }if (data.policyExecption){
                options.push(data.policyExecption)
                sql+= (" `Policy Exception` = ?,"); //only strings need the single quote

            }if (data.achWaived){
                options.push(data.achWaived)
                sql+= (" `ACH Waived` = ?,"); //only strings need the single quote

            }if (data.olv){
                options.push(data.olv)
                sql+= (" OLV = ?,"); //only strings need the single quote

            }if (data.feeToSource){
                options.push(data.feeToSource)
                sql+= (" `Fee to Source` = ?,"); //only strings need the single quote

            }if (data.inspection){
                options.push(data.inspection)
                sql+= (" Inspection = ?,"); //only strings need the single quote

            }if (data.docRequestID){
                options.push(data.docRequestID)
                sql+= (" `Doc Request ID` = ?,"); //only strings need the single quote

            }if (data.decisionDate){
                options.push(data.decisionDate)
                sql+= (" `Decision Date` = ?,"); //only strings need the single quote

            }if (data.status){
                options.push(data.status)
                sql+= (" `Status` = ?,"); //only strings need the single quote

            }if (data.statusDate){
                var date = data.statusDate.indexOf('T')==-1?data.statusDate:data.statusDate.split("T")[0]
                options.push(date)
                sql+= (" `Status Date` = ?,"); //only strings need the single quote

            }if (data.totalWeight){
                options.push(data.totalWeight)
                sql+= (" `Total Weight` = ?,"); //only strings need the single quote

            }if (data.calculatedYield){
                options.push(data.calculatedYield)
                sql+= (" `Calculated Yield` = ?,"); //only strings need the single quote

            }if (data.sbss){
                options.push(data.sbss)
                sql+= (" SBSS = ?,"); //only strings need the single quote

            }if (data.loanGrade){
                options.push(data.loanGrade)
                sql+= (" `Loan Grade` = ?,"); //only strings need the single quote

            }if (data.lowestFICO){
                options.push(data.lowestFICO)
                sql+= (" `Lowest FICO` = ?,"); //only strings need the single quote

            }if (data.creditExpires){
                var date = data.creditExpires.indexOf('T')==-1?data.creditExpires:data.creditExpires.split("T")[0]
                options.push(date)
                sql+= (" `Credit Expire` = ?,"); //only strings need the single quote

            }if (data.trucks){
                options.push(data.trucks)
                sql+= (" Trucks = ?,"); //only strings need the single quote

            }if (data.interviewed){
                options.push(data.interviewed)
                sql+= (" Interviewed = ?,"); //only strings need the single quote

            }if (data.additionalColateralValue){
                options.push(data.additionalColateralValue)
                sql+= (" `Additional Collateral Value` = ?,"); //only strings need the single quote

            }if (data.approvedAmount){
                options.push(data.approvedAmount)
                sql += (" `Approved Amount` = ?," );
            }if (data.percentile){
                options.push(data.percentile)
                sql += (" `Percentile` = ?,")
            }if (data.lowestRevolving){
                options.push(data.lowestRevolving)
                sql += (" `Lowest Revolving` = ?,")
            }if (data.lawsuits){
                options.push(data.lawsuits)
                sql += (" `Lawsuits` = ?,")
            }if (data.judgements){
                options.push(data.judgements)
                sql += (" `Judgements` = ?,")
            }if (data.taxLeans){
                options.push(data.taxLeans)
                sql += (" `Tax Leans` = ?,")
            }if (data.lock){
                options.push(data.lock)
                sql += (" `lock` = ?,")
            }if(data.ageOfEquipment){
                options.push(data.ageOfEquipment)
                sql += (" `Age of Equipment` = ?,")
            }if(data.purpose){
                options.push(data.purpose)
                sql+=(" `Purpose` = ?,")
            }if(data.losesOnPaynet){
                options.push(data.losesOnPaynet)
                sql+=(" `Loses on Paynet` = ?,")
            }
            if(data.slowPay){
                options.push(data.slowPay)
                sql+=(" `Slow Pay` = ?,")
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ${data.id}`;
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.credit`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id], function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
