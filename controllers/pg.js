var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require("../settings")
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`personal gaurantors\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`personal gaurantors\` WHERE \`ID\` = ?`, [id],function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getID = function (req,resp, name){
    db.executeSql(`SELECT ID FROM ${settings.sqlConfig.database}.\`personal gaurantors\` WHERE \`First Name\` = ?`,[name], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req, resp, data);
        }
    })
}
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`personal gaurantors\` set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`personal gaurantors\` set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`personal gaurantors\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.firstName){
                sql+="`First Name`, "
                sqlEnd+=`?, `
                options.push(data.firstName)
            }
            if(data.middleName){
                sql+="`Middle Name`, "
                sqlEnd+=`?, `
                options.push(data.middleName)
            }
            if(data.lastName){
                sql+="`Last Name`, "
                sqlEnd+=`?, `
                options.push(data.lastName)
            }
            if(data.phoneNumber){
                sql+="`Phone Number`, "
                sqlEnd+=`?, `
                options.push(data.phoneNumber)
            }
            if(data.address){
                sql+="`Address`, "
                sqlEnd+=`?, `
                options.push(data.address)
            }
            if(data.city){
                sql+="`City`, "
                sqlEnd+=`?, `
                options.push(data.city)
            }if(data.county){
                sql+="`County`, "
                sqlEnd+=`?, `
                options.push(data.county)
            }
            if(data.state){
                sql+="`State`, "
                sqlEnd+=`?, `
                options.push(data.state)
            }
            if(data.zip){
                sql+="`Zip`, "
                sqlEnd+=`?, `
                options.push(data.zip)
            }
            if(data.ssn){
                sql+="`SSN`, "
                sqlEnd+=`?, `
                options.push(data.ssn)
            }
            if(data.ownership){
                sql+="`Ownership`, "
                sqlEnd+=`?, `
                options.push(data.ownership)
            }
            if(data.title){
                sql+="`Title`, "
                sqlEnd+=`?, `
                options.push(data.title)
            }if(data.fico){
                sql+="`FICO`, "
                sqlEnd+=`"${data.fico}", `
                options.push(data.fico)
            }
            if(data.tradeLine){
                sql+="`Trade Line`, "
                sqlEnd+=`?, `
                options.push(data.tradeLine)
            }
            if(data.yearsInBureau){
                sql+="`Years in Bureau`, "
                sqlEnd+=`?, `
                options.push(data.yearsInBureau)
            }if(data.revolvingAvailableCredit){
                sql+="`Revolving Available Credit`, "
                sqlEnd+=`?, `
                options.push(data.revolvingAvailableCredit)
            }
            if(data.dob){
                sql+="`Date of Birth`, "
                sqlEnd+=`?, `
                options.push(data.dob)
            }
            if(data.email){
                sql+="`Email`, "
                sqlEnd+=`?, `
                options.push(data.email)
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    throw new Error(err)
                }
                else{
                    try{
                        if (data){
                            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`application to pg\` `;
                            sql += "(`App ID`, `PG ID`) "+"VALUES ";
                            sql+= "(?,?)" 
                            console.log(data)
                            var options = [data.appID, x.insertId]
                            db.executeSql(sql,options, function (body, err){
                                if(err){
                                   throw new Error(err)
                                }
                                else{
                                    msg.sendJSON(req,resp, x.insertId)
                                }
                            });          
                        }
                        else{
                            throw new Error("Input not valid");
                        }
                    }
                    catch (ex) {
                        throw new Error(ex)
                
                    }
                    // msg.sendJSON(req,resp, x.insertId)
                }
            });

        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`personal gaurantors\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.firstName){
                sql+= (" `First Name` = ?,"); //only strings need the single quote
                options.push(data.firstName)
            }if (data.middleName){
                sql+= (" `Middle Name` = ?,"); //only strings need the single quote
                options.push(data.middleName)
            }if (data.lastName){
                sql+= (" `Last Name` = ?,"); //only strings need the single quote
                options.push(data.lastName)
            }
            // if(data.dob){
            //     if(data.dob!=undefined&&data.dob!=null){
            //         var date = String(data.dob).indexOf('T')===-1?data.dbo:String(data.dob).split('T')[0]
            //         sql+=(" `Date of Birth` = \""+ date+"\",")
            //     }

            // }
            if (data.phoneNumber){
                sql+= (" `Phone Number` = ?,"); //only strings need the single quote
                options.push(data.phoneNumber)

            }if (data.address){
                sql+= (" Address = ?,"); //only strings need the single quote
                options.push(data.address)

            }if (data.city){
                sql+= (" City = ?,"); //only strings need the single quote
                options.push(data.city)

            }if (data.county){
                sql+= (" County = ?,"); //only strings need the single quote
                options.push(data.county)

            }if (data.state){
                sql+= (" State = ?,"); //only strings need the single quote
                options.push(data.state)

            }if (data.zip){
                sql+= (" Zip = ?,"); //only strings need the single quote
                options.push(data.zip)

            }if (data.ssn){
                sql+= (" `SSN` = ?,"); //only strings need the single quote
                options.push(data.ssn)

            }if (data.ownership){
                sql+= (" `Ownership` = ?,"); //only strings need the single quote
                options.push(data.ownership)

            }if (data.title){
                sql+= (" `Title` = ?,"); //only strings need the single quote
                options.push(data.title)

            }if (data.fico){
                sql+= (" `FICO` = ?,"); //only strings need the single quote
                options.push(data.fico)

            }if (data.tradeLine){
                sql+= (" `Trade Line` = ?,"); //only strings need the single quote
                options.push(data.tradeLine)

            }if (data.yearsInBureau){
                sql+= (" `Years in Bureau` = ?,"); //only strings need the single quote
                options.push(data.yearsInBureau)

            }
            if (data.revolvingAvailableCredit){
                sql+= (" `Revolving Available Credit` = ?,"); //only strings need the single quote
                options.push(data.revolvingAvailableCredit)

            }if (data.email){
                sql+= (" `Email` = ?,"); //only strings need the single quote
                options.push(data.email)

            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID =?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        console.log(ex)
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`personal gaurantors\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
