var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
const setttings = require('../settings')

exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.applicants`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.applicants WHERE ID = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getID = function (req,resp, reqBody){
    var body = JSON.parse(reqBody);
    var name = body['Comapny Name'];
    db.executeSql(`SELECT ID FROM ${setttings.sqlConfig.database}.applicants WHERE 'Company Name' = '?'`, [name],function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req, resp, data);
        }
    })
}
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${setttings.sqlConfig.database}.applicants set 'lock' = 1 WHERE ID = ? `, [id],(data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${setttings.sqlConfig.database}.applicants set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, reqBody){
    try{
        if (!reqBody) throw new Error("Input not valid");
        // var data = JSON.parse(reqBody);
        // if (reqBody){

        //     var sql = "INSERT INTO ${setttings.sqlConfig.database}.applicants (`Company Name`, `DBA Name`, `Contact Name`, `Phone Number`, `Email Address`, `Address`, `City`, `County`, `State`, `Zip`, `Is SSN`, "+
        //     "`Federal ID`, `SOS Type`, `Description of Business`, `Date Established`, NAICS, Website, `Incorporated State`) "+"VALUES";
        //     sql+= util.format("(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s, %s, %s,%s, %s)",
        //                         reqBody.companyName, reqBody.dbaName, reqBody.contactName, reqBody.phoneNumber, reqBody.emailAddress, reqBody.address, reqBody.city, reqBody.county, reqBody.state, reqBody.zip, reqBody.isSSN, reqBody.fedID, reqBody.sosType, reqBody.description, reqBody.establishedDate, reqBody.naics, reqBody.website, reqBody.incorporatedState);
        //     // db.executeSql('')
        //     db.executeSql(sql,function (x, err){
        //         if(err){
        //             msg.show500(req, resp, err);
        //         }
        //         else{
        //             msg.sendJSON(req,resp, x.insertId)
        //         }
        //     });

        // }
        // else{
        //     throw new Error("Input not valid");
        // }
        if (reqBody){
            console.log(reqBody)
            var options = []
            var sql = `INSERT INTO ${setttings.sqlConfig.database}.applicants (`
            var sqlEnd = ") VALUES (";
            if(reqBody.companyName){
                sql+="`Company Name`, "
                sqlEnd+=`?, `
                options.push(reqBody.companyName)
            }
            if(reqBody.dbaName){
                sql+="`DBA Name`, "
                sqlEnd+=`?, `
                options.push(reqBody.dbaName)
            }
            if(reqBody.contactName){
                sql+="`Contact Name`, "
                sqlEnd+=`?, `
                options.push(reqBody.contactName)
            }
            if(reqBody.phoneNumber){
                sql+="`Phone Number`, "
                sqlEnd+=`?, `
                options.push(reqBody.phoneNumber)
            }
            if(reqBody.emailAddress){
                sql+="`Email Address`, "
                sqlEnd+=`?, `
                options.push(reqBody.emailAddress)
            }
            if(reqBody.address){
                sql+="`Address`, "
                sqlEnd+=`?, `
                options.push(reqBody.address)
            }
            if(reqBody.city){
                sql+="`City`, "
                sqlEnd+=`?, `
                options.push(reqBody.city)
            }
            if(reqBody.county){
                sql+="`County`, "
                sqlEnd+=`?, `
                options.push(reqBody.county)
            }
            if(reqBody.state){
                sql+="`State`, "
                sqlEnd+=`?, `
                options.push(reqBody.state)
            }
            if(reqBody.zip){
                sql+="`Zip`, "
                sqlEnd+=`?, `
                options.push(reqBody.zip)
            }
            if(reqBody.isSSN){
                sql+="`Is SSN`, "
                sqlEnd+=`?, `
                options.push(reqBody.isSSN)
            }
            if(reqBody.fedID){
                sql+="`Federal ID`, "
                sqlEnd+=`?, `
                options.push(reqBody.fedID)
            }
            if(reqBody.sosType){
                sql+="`SOS Type`, "
                sqlEnd+=`?, `
                options.push(reqBody.sosType)
            }
            if(reqBody.description){
                sql+="`Description of Business`, "
                sqlEnd+=`?, `
                options.push(reqBody.description)
            }
            if(reqBody.naics){
                sql+="`NAICS`, "
                sqlEnd+=`?, `
                options.push(reqBody.naics)
            }
            if(reqBody.website){
                sql+="`Website`, "
                sqlEnd+=`?, `
                options.push(reqBody.website)
            }
            if(reqBody.incorporatedState){
                sql+="`Incorporated State`, "
                sqlEnd+=`?, `
                options.push(reqBody.incorporatedState)
            }
            if(reqBody.establishedDate){
                var date = String(reqBody.establishedDate).indexOf('T')==-1?reqBody.establishedDate:String(reqBody.establishedDate).split('T')[0]
                sql+="`Date Established`, "
                sqlEnd+=`?, `
                options.push(date)
            }if(reqBody.comment){
                sql+="Comment, "
                sqlEnd+=`?, `
                options.push(reqBody.comment)
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }

    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var options = []
            var sql = `UPDATE ${setttings.sqlConfig.database}.applicants SET `;
            //repeat the following line for each column in the table;

            if (data.companyName){
                sql+= (" `Company Name` = ?,"); //only strings need the single quote
                options.push(data.companyName)

            }if (data.dbaName){
                sql+= (" `DBA Name` = ?,"); //only strings need the single quote
                options.push(data.dbaName)

            }if (data.contactName){
                sql+= (" `Contact Name` = ?,"); //only strings need the single quote
                options.push(data.contactName)

            }
            if (data.phoneNumber){
                sql+= (" `Phone Number` = ?,"); //only strings need the single quote
                options.push(data.phoneNumber)

            }if (data.emailAddress){
                sql+= (" `Email Address` = ?,"); //only strings need the single quote
                options.push(data.emailAddress)

            }if (data.address){
                sql+= (" Address = ?,"); //only strings need the single quote
                options.push(data.address)

            }if (data.city){
                sql+= (" City = ?,"); //only strings need the single quote
                options.push(data.city)

            }if (data.county){
                sql+= (" County = ?,"); //only strings need the single quote
                options.push(data.county)

            }if (data.state){
                sql+= (" State = ?,"); //only strings need the single quote
                options.push(data.state)

            }if (data.zip){
                sql+= (" Zip = ?,"); //only strings need the single quote
                options.push(data.zip)

            }if (data.isSSN){
                sql+= (" `Is SSN` = ?,"); //only strings need the single quote
                options.push(data.isSSN)

            }if (data.fedID){
                sql+= (" `Federal ID` = ?,"); //only strings need the single quote
                options.push(data.fedID)

            }if (data.sosType){
                sql+= (" `SOS Type` = ?,"); //only strings need the single quote
                options.push(data.sosType)

            }if (data.description){
                sql+= (" `Description of Business` = ?,"); //only strings need the single quote
                options.push(data.description)

            }if (data.establishedDate){
                var date = String(data.establishedDate).indexOf("T")==-1?data.establishedDate:String(data.establishedDate).split("T")[0]
                sql+= (" `Date Established` = ?,"); //only strings need the single quote
                options.push(date)

            }if (data.naics){
                sql+= (" NAICS = ?,"); //only strings need the single quote
                options.push(data.naics)

            }if (data.website){
                sql+= (" Website = ?,"); //only strings need the single quote
                options.push(data.website)
            }if(data.incorporatedState){
                sql+=(" `Incorporated State` = ?,")
                options.push(data.incorporatedState)
            }
            if(data.comment){
                sql+=(" `Comment` = ?,")
                options.push(data.comment)
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            options.push(data.id)
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${setttings.sqlConfig.database}.applicants`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
