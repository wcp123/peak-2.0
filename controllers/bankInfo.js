var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
const setttings = require('../settings')

const cleanDate = (date)=>{
    return String(date).split('T')[0]
}
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\`bankinfo\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\`bankinfo\` WHERE \`App ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err)
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${setttings.sqlConfig.database}.\`bankinfo\` set 'lock' = 1 WHERE ID =? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${setttings.sqlConfig.database}.\`bankinfo\` set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        // if (data){
        //     var sql = "INSERT INTO ${setttings.sqlConfig.database}.`bankinfo` ( `App ID`, `Withdrawals`, `Deposites`, Balance, `Date`, `Bank Name`) "+"VALUES";
        //     sql+= util.format("( %d,'%s','%s','%s','%s','%s')",
        //                     data.appID, data.withdraw, data.deposite, data.balance, data.date, data.bankName);
        //     db.executeSql(sql,function (x, err){
        //         if(err){
        //             msg.show500(req, resp, err);
        //         }
        //         else{
        //             msg.sendJSON(req,resp, x.insertId)
        //         }
        //     });
        // }
        // else{
        //     throw new Error("Input not valid");
        // }
        if (data){
            var sql = `INSERT INTO ${setttings.sqlConfig.database}.\`bankinfo\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.appID){
                sql+="`App ID`, "
                sqlEnd+=`?, `
                options.push(data.appID)
            }
            if(data.withdraw){
                sql+="`Withdrawals`, "
                sqlEnd+=`?, `
                options.push(data.withdraw)
            }
            if(data.deposite){
                sql+="`Deposites`, "
                sqlEnd+=`?, `
                options.push(data.deposite)
            }
            if(data.balance){
                sql+="`Balance`, "
                sqlEnd+=`?, `
                options.push(data.balance)
            }
            if(data.date){
                sql+="`Date`, "
                sqlEnd+=`?, `
                options.push(cleanDate(data.date))
            }
            if(data.bankName){
                sql+="`Bank Name`, "
                sqlEnd+=`?, `
                options.push(data.bankName)
            }

            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        // if (!reqBody) throw new Error("Input not valid");
        // var data = JSON.parse(reqBody);
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${setttings.sqlConfig.database}.bankinfo SET `;
            var options = []
            //repeat the following line for each column in the table;
            var date = (data.date+'').indexOf('T')===-1?data.date:(data.date+'').split('T')[0]
            if (data.CreditID){
                sql+= (" `Credit ID` =?,"); //only strings need the single quote
                options.push(data.CreditID)

            }if (data.withdraw){
                sql+= (" `Withdrawals` = ?,"); //only strings need the single quote
                options.push(data.withdraw)

            }if (data.deposite){
                sql+= (" `Deposites` = ?,"); //only strings need the single quote
                options.push(data.deposite)

            }
            if (data.balance){
                sql+= (" Balance = ?,"); //only strings need the single quote
                options.push(data.balance)

            }if (data.date){
                sql+= (" Date = ?,"); //only strings need the single quote
                options.push(cleanDate(data.date))

            }if (data.bankName){
                sql+= (" `Bank Name` = ?,"); //only strings need the single quote
                options.push(data.bankName)

            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID =?`;
            options.push(data.id)
            //
            db.executeSql(sql,options, function (data, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, data)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, id){
    try{
        if (!id){
            throw new Error("Input not valid");
        }else{
            var sql = `DELETE FROM ${setttings.sqlConfig.database}.bankinfo WHERE \`ID\` = ?`;
            db.executeSql(sql,[id],function (data, err){
                if(err){
                    msg.show500(req, resp, err);
                }else{
                    msg.sendJSON(req,resp, {})
                }
            });
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
