var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
const { v4: uuidv4 } = require('uuid');
const settings = require('../settings')

exports.getList = function(req, resp){
    db.executeSql(`
            SELECT 
                   a.*
                   , app.\`Company Name\` as \`Company Name\`
                   , app.\`DBA Name\` as \`DBA Name\`
                   , staff.\`First Name\` as \`staff_first_name\`
                   , staff.\`Last Name\` as \`staff_last_name\`
            FROM 
                ${settings.sqlConfig.database}.applications a 
            JOIN 
                ${settings.sqlConfig.database}.applicants app 
                    ON a.\`Company ID\` = app.\`ID\`
            JOIN 
                ${settings.sqlConfig.database}.staff staff 
                    ON a.\`BDO\` = staff.\`ID\`
            ORDER BY a.\`ID\` DESC
        `,[], function (data, err) {

        if(err){
            msg.show500(req, resp, err);
        }
        else{
            var applicants = []
            if(data.length){
                data.forEach(function(row) {
                    var tempRow = {
                        ID: row['ID'],
                        'Company ID': row['Company ID'],
                        'Source ID': row['Source ID'],
                        'Submitted Date': row['Submitted Date'],
                        'Submitted Amount': row['Submitted Amount'],
                        'Equipment Cost': row['Equipment Cost'],
                        'Collateral Description': row['Collateral Description'],
                        Decision: row['Decision'],
                        'Purpose of Loan': row['Purpose of Loan'],
                        BDO: row['BDO'],
                        'Requested Frequency': row['Requested Frequency'],
                        'Requested Advanced': row['Requested Advanced'],
                        'Requested Terms': row['Requested Terms'],
                        lock: row['lock'],
                        'company': {
                            ID: row['Company ID'],
                            'Company Name': row['Company Name'],
                            'DBA Name': row['DBA Name']
                        },
                        staff: {
                            first_name: row['staff_first_name'],
                            last_name: row['staff_last_name'],
                        }
                    }
                    applicants.push(tempRow)
                });
            }
            msg.sendJSON(req,resp, applicants)
        }

    });
};

exports.generateApplication = function (req, resp, id){
    var uuid = uuidv4();
    let sql = `INSERT INTO ${settings.sqlConfig.database}.applications (uuid) VALUES (?)`
    db.executeSql(sql, [uuid],function (data, err){
        if(err){
            msg.show500(req, resp, err)
        }
        else{
            msg.sendJSON(req,resp, {
                id: data.insertId,
                uuid: uuid
            })
        }
    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    let sql = `SELECT * FROM ${settings.sqlConfig.database}.applications WHERE \`ID\` = ?`
    db.executeSql(sql, [id],function (data, err){
        if(err){
            msg.show500(req, resp, err)
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.applications set lock = 1 WHERE \`ID\` = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.applications set lock = 0 WHERE \`ID\` = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        console.log(data)
        // if (data){
        //             var newID  = 1
        //             var sql = "INSERT INTO ${settings.sqlConfig.database}.applications (\`Company ID\`, \`Submitted Date\`, \`Source ID\`, \`Submitted Amount\`, \`Equipment Cost\`, \`Collateral Description\`, \`Purpose of Loan\`, \`BDO\`, \`Requested Terms\`, \`Requested Frequency\`, \`Requested Advanced\`, Decision) "+"VALUES ";
        //             sql+= util.format("(%d,now(),%d,\"%s\",\"%s\",\"%s\",\"%s\", %d, \"%s\", \"%s\", \"%s\",\"%s\")",
        //                                  data.companyID, data.sourceID, data.submittedAmount, data.equipmentCost, data.collateralDescription, data.purposeOfLoan, data.bdo, data.terms, data.frequency, data.advance, data.decision);
        //             db.executeSql(sql,function (body, err){
        //                 if(err){
        //                     msg.show500(req, resp, err);
        //                 }
        //                 else{
        //                     msg.sendJSON(req,resp, body.insertId)
        //                 }
        //             });
        // }
        // else{
        //     throw new Error("Input not valid");
        // }
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.applications (`
            var sqlEnd = ") VALUES (";
            var params = []
            if(data.companyID){
                sql+="`Company ID`, "
                sqlEnd+=`?, `
                params.push(data.companyID)
            }
            if(data.sourceID){
                sql+="`Source ID`, "
                sqlEnd+=`?, `
                params.push(data.sourceID)
            }
            if(true){
                sql+="`Submitted Date`, "
                sqlEnd+=`now(), `
                
            }
            if(data.submittedAmount){
                sql+="`Submitted Amount`, "
                sqlEnd+=`?, `
                params.push(data.submittedAmount)
            }
            if(data.equipmentCost){
                sql+="`Equipment Cost`, "
                sqlEnd+=`?, `
                params.push(data.equipmentCost)
            }
            if(data.collateralDescription){
                sql+="`Collateral Description`, "
                sqlEnd+=`?, `
                params.push(data.collateralDescription)
            }
            if(data.purposeOfLoan){
                sql+="`Purpose of Loan`, "
                sqlEnd+=`?, `
                params.push(data.purposeOfLoan)
            }
            if(data.bdo){
                sql+="`BDO`, "
                sqlEnd+=`?, `
                params.push(data.bdo)
            }
            if(data.terms){
                sql+="`Requested Terms`, "
                sqlEnd+=`?, `
                params.push(data.terms)
            }
            if(data.frequency){
                sql+="`Requested Frequency`, "
                sqlEnd+=`?, `
                params.push(data.frequency)
            }
            if(data.advance){
                sql+="`Requested Advanced`, "
                sqlEnd+=`?, `
                params.push(data.advance)
            }
            if(data.decision){
                sql+="`Decision`, "
                sqlEnd+=`?, `
                params.push(data.decision)
            }
            if(data.status){
                sql+="`Status`, "
                sqlEnd+=`?, `
                params.push(data.status)
            }
            if(data.deadDate){
                sql+="`Dead Date`, "
                sqlEnd+=`?, `
                params.push(data.deadDate)
            }
            if(data.deathComment){
                sql+="`Death Comment`, "
                sqlEnd+=`?, `
                params.push(data.deathComment)
            }

            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql, params,function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.applications SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.companyID){
                sql+= (" `Company ID` = ?,"); //only strings need the single quote
                options.push(data.companyID)

            }if (data.submittedDate){
                sql+= (" `Submitted Date` = ?,"); //only strings need the single quote
                options.push(data.submittedDate)

            }if (data.sourceID){
                sql+= (" `Source ID` = ?,"); //only strings need the single quote
                options.push(data.sourceID)

            }
            if (data.submittedAmount){
                sql+= (" `Submitted Amount` = ?,"); //only strings need the single quote
                options.push(data.submittedAmount)

            }if (data.equipmentCost){
                sql+= (" `Equipment Cost` = ?,"); //only strings need the single quote
                options.push(data.equipmentCost)

            }if (data.collateralDescription){
                sql+= (" `Collateral Description` = ?,"); //only strings need the single quote
                options.push(data.collateralDescription)

            }if (data.decision){
                sql+= (" `Decision` = ?,"); //only strings need the single quote
                options.push(data.decision)

            }if (data.purposeOfLoan){
                sql+= (" `Purpose of Loan` = ?,"); //only strings need the single quote
                options.push(data.purposeOfLoan)

            }if (data.bdo){
                sql+= (" `BDO` = ?,"); //only strings need the single quote
                options.push(data.bdo)

            }if (data.terms){
                sql+= (" `Requested Terms` = ?,"); //only strings need the single quote
                options.push(data.terms)

            }if (data.frequency){
                sql+= (" `Requested Frequency` = ?,"); //only strings need the single quote
                options.push(data.frequency)

            }if (data.advance){
                sql+= (" `Requested Advanced` = ?,"); //only strings need the single quote
                options.push(data.advance)

            }
            if(data.status){
                sql+=(" `Status` = ?,")
                options.push(data.status)
            }
            if(data.deadDate){
                sql+=(" `Dead Date` =?,")
                options.push(data.deadDate)
            }
            if(data.deathComment){
                sql+=(" `Death Comment` = ?,")
                options.push(data.deathComment)
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql, options, function (body, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, body)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.applications`;

            sql = sql + ` WHERE ID = ?`;
            
            db.executeSql(sql,options, function (body, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, body)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
