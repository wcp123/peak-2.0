var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function (req, resp) {
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.staff`, [], function (data, err) {
        if (err) {
            msg.show500(req, resp, err);
        }
        else {
            msg.sendJSON(req, resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id) {
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.staff WHERE ID = ?`, [id], function (data, err) {
        if (err) {
            msg.show500(req, resp, err);
        }
        else {
            msg.sendJSON(req, resp, data)
        }
    });
};

//adds to the database, This method should only be used for the ${settings.sqlConfig.database}.staff table. But stuct is same for other methods
exports.add = function (req, resp, data) {
    try {
        if (data) {
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`staff\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if (data.firstName) {
                sql += "`First Name`, "
                options.push(data.firstName)
                sqlEnd += `?, `
            }
            if (data.lastName) {
                sql += "`Last Name`, "
                options.push(data.lastName)
                sqlEnd += `?, `
            }
            if (data.type) {
                sql += "`Type`, "
                options.push(data.type)
                sqlEnd += `?, `
            }

            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql, options, function (x, err) {
                if (err) {
                    msg.show500(req, resp, err);
                }
                else {
                    msg.sendJSON(req, resp, x.insertId)
                }
            });
        }


        else {
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);
    }
};
exports.lock = (req, resp, id) => {
    try {
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.staff set \`lock\` = 1 WHERE ID = ?`, [id], (x, err) => {
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, x)
        }

        )
    }
    catch (er) {
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) => {
    try {
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.staff set \`lock\` = 0 WHERE ID = ?`, [id], (data, err) => {
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er) {
        msg.show500(req, resp, er)
    }
}

exports.update = function (req, resp, data) {
    try {
        if (data) {

            var sql = `UPDATE ${settings.sqlConfig.database}.staff SET `;
            var options = []
            //repeat the following line for each column in the table;
            if (data.id) {
                sql += ("`ID` = ?,"); //only strings need the single quote
                options.push(data.id)

            }
            if (data.firstName) {
                sql += (" `First Name` = ?,"); //only strings need the single quote
                options.push(data.firstName)

            } if (data.lastName) {
                sql += (" `Last Name` = ?"); //only strings need the single quote
                options.push(data.lastName)

            }
            //for additional fields add a comma at the begining of next sql string
            sql = sql + `WHERE [ID] = ?`;
            //
            options.push(data.id)
            db.executeSql(sql, options, function (x, err) {
                if (err) {
                    msg.show500(req, resp, err);
                }
                else {
                    msg.sendJSON(req, resp, x)
                }
            });
        }
        else {
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function (req, resp, reqBody) {

};
exports.delete = function (req, resp, id) {
    try {
        if (id) {
            //this line is needed for other controllers that have id numbers
            //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.staff`;
            //repeat the following line for each column in the table;


            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //

            db.executeSql(sql, [data.id], function (x, err) {
                if (err) {
                    msg.show500(req, resp, err);
                }
                else {
                    msg.show200(req, resp, x)
                }
            });
        }
        else {
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getStaffNames = function (req, res, body) {
    try {
        console.log(body)
        var options = []
        var sqlCat = '('
        Object.entries(body).map(x=>{
            options.push(x[1])
            sqlCat+='?,'
        })
        // Object.keys(body).map(x=>{
        //     options.push(body[x]['ID'])
        //     sqlCat+='?,'
        // })
        sqlCat= sqlCat.slice(0,-1)
        sqlCat+=')'
        db.executeSql(`
                SELECT * FROM ${settings.sqlConfig.database}.staff WHERE ID in ${sqlCat}
                    `, options, function (x, err) {
            if (err) throw new Error(err)
            console.log(x)
            var obj ={}
            Object.entries(body).map(entry=>{
                var filtered = x.filter(row=>row['ID']===entry[1])
                if(filtered.length!==0){
                    obj[entry[0]] = filtered[0]

                }
            })
            console.log(obj)
            msg.sendJSON(req, res, obj)
        })
    }
    catch (err) {
        msg.show500(req, res, err)
    }
}

