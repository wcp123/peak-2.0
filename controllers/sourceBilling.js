var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source billing\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the sources.js file in the controllers module
exports.get = function (req, resp, sourceID){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source billing\` WHERE \`Source ID\` = ?`,[sourceID], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getBill = function (req, resp, sourceID){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source billing\` WHERE \`ID\` = ?`,[sourceID], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`source billing\` set \`lock\` = 1 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`source billing\` set lock = 0 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`source billing\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.sourceID){
                sql+="`Source ID`, "
                sqlEnd+=`?, `
                options.push(data.sourceID)
            }
            if(data.wireOrAch){
                sql+="`Wire Or Ach`, "
                sqlEnd+=`?, `
                options.push(data.wireOrAch)
            }
            if(data.accountNumber){
                sql+="`Account Number`, "
                sqlEnd+=`?, `
                options.push(data.accountHoldersName)
            }
            if(data.routingNumber){
                sql+="`Routing Number`, "
                sqlEnd+=`?, `
                options.push(data.routingNumber)
            }
            if(data.bankName){
                sql+="`Bank Name`, "
                sqlEnd+=`?, `
                options.push(data.bankName)
            }if(data.accountHoldersName){
                sql+="`Account Holders Name`, "
                sqlEnd+=`?, `
                options.push(data.accountHoldersName)
            }

            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`source billing\` SET`;
            //repeat the following line for each column in the table;
            var options = []
            if (data.accountNumber){
                sql+= (" `Account Number` = ?,"); //only strings need the single quote
                options.push(data.accountName)

            }
            if (data.routingNumber){
                sql+= (" `Routing Number` = ?,"); //only strings need the single quote
                options.push(data.routingNumber)

            }if(data.bankName){
                sql+=(" `Bank Name` = ?,")
                options.push(data.bankName)
            }if(data.wireOrAch){
                sql+=(" `Wire or ACH` = ? ,")
                options.push(data.wireOrAch)
            }
            if(data.accountHoldersName){
                sql+=(" `Account Holders Name` = ?,")
                options.push(data.accountHoldersName)
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`source billing\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
