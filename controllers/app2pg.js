var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
const setttings = require('../settings')

exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\'application to pg\'`, [], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
        
    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`Select * from ${setttings.sqlConfig.database}.\`application to pg\` Where \`App ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//this function needs put in the docs.js file in the controllers module
exports.getAllGuarantorsForApplication = function (req, resp, id){
    db.executeSql(`
        SELECT 
            pg.* 
        FROM
            ${setttings.sqlConfig.database}.\`personal gaurantors\` pg
        JOIN
            ${setttings.sqlConfig.database}.\`application to pg\` apg
                ON apg.\`PG ID\` = pg.\`ID\`
        WHERE
            apg.\`App ID\` = ?


        `,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var sql = `INSERT INTO ${setttings.sqlConfig.database}.\`application to pg\` `;
            sql += "(`App ID`, `PG ID`) "+"VALUES ";
            sql+= "(?,?)" 
            var options = [data.appID, data.pgID]
            db.executeSql(sql,function (body, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, body.insertId)
                }
            });          
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${setttings.sqlConfig.database}.\`application to pg\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.appID){
                sql+= (" `App ID` = ?, "); //only strings need the single quote
                options.push(data.appID)
            }
            if (data.PGID ){
                sql+= (" `PG ID` = ?, "); //only strings need the single quote
                options.push(data.PGID)
            }if (data.CGID){
                sql+= (" `CG ID` = ?, "); //only strings need the single quote
                options.push(data.CGID)
            }
            
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            options.push(data.id)
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            var isDataProvided = false;
            var sql = `DELETE FROM ${setttings.sqlConfig.database}.\`application to PG\``;

            sql = sql + ` WHERE ID = ?`;
            var options =[data.id]
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};