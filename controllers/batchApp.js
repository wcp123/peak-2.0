var db = require("../db");
var msg = require("../httpMsgs");
var util = require("util");

exports. update = function (req, resp, data){
    try{
        if (data){
            
            var sql = ''
            data.bankInfo.map(bank=>{
                // if(bank.new){
                //     sql += \"INSERT INTO jarvisdb.`bankinfo` ( `App ID`, `Withdrawals`, `Deposites`, Balance, `Date`, `Bank Name`) VALUES \"
                //     sql+= util.format("( %s,\"%s\",\"%s\",\"%s\",\"%s\",\"%s\");", 
                //             bank.appID, bank.withdraw, bank.deposite, bank.balance, bank.date, bank.bankName);
                // }
                // else{
                    
                    sql += " UPDATE jarvisdb.bankinfo SET ";
                    //repeat the following line for each column in the table;
                    var date = (bank.date+'').indexOf('T')===-1?bank.date:(bank.date+'').split('T')[0]
                    if (bank.appID){
                        sql+= (" `App ID` = " + bank.appID+ ","); //only strings need the single quote
                    
                    }if (bank.withdraw){
                        sql+= (" `Withdrawals` = \"" + bank.withdraw+ "\","); //only strings need the single quote
                    
                    }if (bank.deposite){
                        sql+= (" `Deposites` = \"" + bank.deposite+ "\","); //only strings need the single quote
                    
                    }
                    if (bank.balance){
                        sql+= (" Balance = \"" + bank.balance+ "\","); //only strings need the single quote
                    
                    }if (bank.date){
                        sql+= (" Date = \"" + date + "\","); //only strings need the single quote
                    
                    }if (bank.bankName){
                        sql+= (" `Bank Name` = \"" + bank.bankName+ "\","); //only strings need the single quote
                    
                    }
                    
                    sql = sql.slice(0, -1);
                    //for additional fields add a comma at the begining of next sql string
                    sql = sql + ` WHERE ID = ${bank.id};`;
                    //
                // }
                
            })
            // data.equipmentInfo.map(equipment =>{
            //     if(equipment.new){
            //         sql += "INSERT INTO jarvisdb.equipment ";
            //         sql += "(\`Equipment Description\`, \`Equipment Location\`, \`Equipment Cost\`, \`VIN Or Serial\`,\`Serial Number\`) VALUES ";
            //         sql+= util.format("(\"%s\", \"%s\", \"%s\", %s, \"%s\");", 
            //                     equipment.equipmentDescription, equipment.equipmentLocation, equipment.equipmentCost, equipment.vinOrSerial, equipment.serialNumber);
            //     }
            //     else{
            //         var sql = " UPDATE jarvisdb.equipment SET ";
            //         //repeat the following line for each column in the table;
                    
            //         if (equipment.equipmentDescription){
            //             sql+= (" `Equipment Description` = \"" + equipment.equipmentDescription + "\","); //only strings need the single quote
                    
            //         }
            //         if (equipment.equipmentLocation){
            //             sql+= (" `Equipment Location` = \"" + equipment.equipmentLocation + "\","); //only strings need the single quote
                    
            //         }if (equipment.equipmentCost){
            //             sql+= (" `Equipment Cost` = \"" + equipment.equipmentCost + "\","); //only strings need the single quote
                    
            //         }if (equipment.vinOrSerial){
            //             sql+= (" `VIN Or Serial` = " + equipment.vinOrSerial + ","); //only strings need the single quote
                    
            //         }
                    
            //         if (equipment.serialNumber){
            //             sql+= (" `Serial Number` = \"" + equipment.serialNumber + "\","); //only strings need the single quote
                    
            //         }
            //         if(equipment.city){
            //             sql+= (" `City` = \""+ equipment.city+"\",")
            //         }
            //         if(equipment.zip){
            //             sql+=(" `Zip` = \""+equipment.zip+"\",")
            //         }
            //         if(equipment.state){
            //             sql+=(" `State` = \""+equipment.state+"\",")
            //         }
                    
                    
            //         sql = sql.slice(0, -1);
            //         //for additional fields add a comma at the begining of next sql string
            //         sql = sql + ` WHERE ID = ${equipment.id};`;
            //     }
            // })
            // data.vendor.map(vendor=>{
            //     // if(vendor.new){
            //     //     sql += "INSERT INTO jarvisdb.vendors (\`Company Name\`, \`DBA Name\`, \`Address\`, City, County, State, Zip, \`Phone Number\`, Email, Website, \`Federal ID\`, \`Time in Business\`,\`Approved for Prefunding\`, \`Prefunding Terms\`, \`SOS Type\`, Approved, OFAC, \`Source Reference\`) VALUES";
            //     //     sql+= util.format("(\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",%s,\"%s\",\"%s\",\"%s\",\"%s\", \"%s\", %s, \"%s\", \"%s\", %s, %s, \"%s\");",
            //     //                 vendor.companyName, vendor.dbaName, vendor.address, vendor.city, vendor.county, vendor.state, vendor.zip, vendor.phoneNumber, vendor.email, vendor.website, vendor.federalID, vendor.timeInBusiness, vendor.approvedForPrefunding, vendor.prefundingTerms, vendor.sosType, vendor.approved, vendor.ofac, vendor.sourceReference);
            //     // }
            //     // else{
            //         sql += " UPDATE jarvisdb.vendors SET ";
            //         //repeat the following line for each column in the table;

            //         if (vendor.companyName){
            //             sql+= (" `Company Name` = \"" + vendor.companyName+ "\","); //only strings need the single quote

            //         }if (vendor.dbaName){
            //             sql+= (" `DBA Name` = \"" + vendor.dbaName+ "\","); //only strings need the single quote

            //         }if (vendor.address){
            //             sql+= (" `Address` = \"" + vendor.address+ "\","); //only strings need the single quote

            //         }
            //         if (vendor.city){
            //             sql+= (" City = \"" + vendor.city+ "\","); //only strings need the single quote

            //         }if (vendor.county){
            //             sql+= (" County = \"" + vendor.county+ "\","); //only strings need the single quote

            //         }if (vendor.state){
            //             sql+= (" State = \"" + vendor.state+ "\","); //only strings need the single quote

            //         }if (vendor.zip){
            //             sql+= (" Zip = \"" + vendor.zip+ "\","); //only strings need the single quote

            //         }if (vendor.phoneNumber){
            //             sql+= (" `Phone Number` = \"" + vendor.phoneNumber+ "\","); //only strings need the single quote

            //         }if (vendor.email){
            //             sql+= (" Email = \"" + vendor.email+ "\","); //only strings need the single quote

            //         }if (vendor.website){
            //             sql+= (" Website = \"" + vendor.website+ "\","); //only strings need the single quote

            //         }if (vendor.federalID){
            //             sql+= (" `Federal ID` = \"" + vendor.federalID+ "\","); //only strings need the single quote

            //         }if(vendor.timeInBusiness){
            //             sql+=(" `Time in Business` = \"" + vendor.timeInBusiness + "\",")
            //         }if(vendor.approvedForPrefunding){
            //             sql+=(" `Approved for Prefunding` = " + vendor.approvedForPrefunding + ",")
            //         }if(vendor.prefundingTerms){
            //             sql+=(" `Prefunding Terms` = \"" + vendor.prefundingTerms + "\",")
            //         }if(vendor.sosType){
            //             sql+=(" `SOS Type` = \"" + vendor.sosType + "\",")
            //         }if(vendor.approved){
            //             sql+=(" Approved = " + vendor.approved + ",")
            //         }if(vendor.ofac){
            //             sql+=(" OFAC = " + vendor.ofac + ",")
            //         }
            //         sql = sql.slice(0, -1);
            //         //for additional fields add a comma at the begining of next sql string
            //         sql = sql + ` WHERE ID = ${vendor.id};`;
            //     // }
            // })
            // data.pgInfo.map(pg=>{
            //     // if(pg.new){
            //     //     sql += "INSERT INTO jarvisdb.\`personal gaurantors\` (`First Name`, `Middle Name`, `Last Name`, `Phone Number`, `Address`, `City`, `County`, `State`, `Zip`, "+
            //     //     "`SSN`, `Ownership`, `Title`, `FICO`, `Trade Line`, `Years in Bureau`, `Revolving Available Credit`, `Date of Birth`) "+"VALUES";
            //     //     sql+= util.format("(\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\", \"%s\",\"%s\",\"%s\",%s, %s, %s, %s);", 
            //     //                 pg.firstName, pg.middleName, pg.lastName, pg.phoneNumber, pg.address, pg.city, pg.county, pg.state, pg.zip, pg.ssn, pg.ownership, pg.title, pg.fico, pg.tradeLine, pg.yearsInBureau, pg.revolvingAvailableCredit, pg.dob);
            //     // // }
            //     // else{
            //         sql += " UPDATE jarvisdb.\`personal gaurantors\` SET ";
            //         //repeat the following line for each column in the table;
                    
            //         if (pg.firstName){
            //             sql+= (" `First Name` = \"" + pg.firstName+ "\","); //only strings need the single quote
                    
            //         }if (pg.dbaName){
            //             sql+= (" `Middle Name` = \"" + pg.dbaName+ "\","); //only strings need the single quote
                    
            //         }if (pg.lastName){
            //             sql+= (" `Last Name` = \"" + pg.lastName+ "\","); //only strings need the single quote
                    
            //         }
            //         if(pg.dob){
            //             sql+=(" `Date of Birth` = \""+ pg.dob+"\",")
            //         }
            //         if (pg.phoneNumber){
            //             sql+= (" `Phone Number` = \"" + pg.phoneNumber+ "\","); //only strings need the single quote
                    
            //         }if (pg.address){
            //             sql+= (" Address = \"" + pg.address+ "\","); //only strings need the single quote
                    
            //         }if (pg.city){
            //             sql+= (" City = \"" + pg.city+ "\","); //only strings need the single quote
                    
            //         }if (pg.county){
            //             sql+= (" County = \"" + pg.county+ "\","); //only strings need the single quote
                    
            //         }if (pg.state){
            //             sql+= (" State = \"" + pg.state+ "\","); //only strings need the single quote
                    
            //         }if (pg.zip){
            //             sql+= (" Zip = \"" + pg.zip+ "\","); //only strings need the single quote
                    
            //         }if (pg.ssn){
            //             sql+= (" `SSN` = \"" + pg.ssn+ "\","); //only strings need the single quote
                    
            //         }if (pg.ownership){
            //             sql+= (" `Ownership` = \"" + pg.ownership+ "\","); //only strings need the single quote
                    
            //         }if (pg.title){
            //             sql+= (" `Title` = \"" + pg.title+ "\","); //only strings need the single quote
                    
            //         }if (pg.fico){
            //             sql+= (" `FICO` = " + pg.fico+ ","); //only strings need the single quote
                    
            //         }if (pg.tradeLine){
            //             sql+= (" `Trade Line` = " + pg.tradeLine+ ","); //only strings need the single quote
                    
            //         }if (pg.yearsInBureau){
            //             sql+= (" `Years in Bureau` = " + pg.yearsInBureau+ ","); //only strings need the single quote
                    
            //         }
            //         if (pg.revolvingAvailableCredit){
            //             sql+= (" `Revolving Available Credit` = \"" + pg.revolvingAvailableCredit+ "\","); //only strings need the single quote
                    
            //         }
                    
            //         sql = sql.slice(0, -1);
            //         //for additional fields add a comma at the begining of next sql string
            //         sql = sql + ` WHERE ID = ${pg.id};`;
            //     // }
            // })
            // data.CG.map(cg=>{
            //     // if(cg.new){
            //     //     sql += "INSERT INTO jarvisdb.`corporate gaurantor` ";
            //     //     sql += "(`Company Name`, `Signors Name`, `Signors Title`, Address, `SOS Type`, `Date Established`) "+"VALUES ";
            //     //     sql+= util.format("(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\");", 
            //     //             cg.companyName, cg.signorsName, cg.signorsTitle, cg.address, cg.sosType, cg.dateEstablished)
            //     // }
            //     // else{
            //         sql += " UPDATE jarvisdb.`corporate gaurantor` SET ";            
            //         if (cg.companyName){
            //             sql+= (" `Company Name` = \"" + cg.companyName + "\","); //only strings need the single quote
            //         }
            //         if (cg.signorsName ){
            //             sql+= (" `Signors Name` = \"" + cg.signorsName + "\","); //only strings need the single quote
            //         }if (cg.signorsName){
            //             sql+= (" `Signors Title` = \"" + cg.signorsTitle + "\","); //only strings need the single quote
            //         }if (cg.address){
            //             sql+=(" `Address` = \"" + cg.address + "\",")
            //         }if (cg.sosType){
            //             sql+=(" `SOS Type` = \"" + cg.sosType + "\",")
            //         }if (cg.dateEstablished){
            //             sql+=(" `Date Established` = \"" + cg.dateEstablished + "\",")
            //         }
            //         sql = sql.slice(0, -1);
            //         //for additional fields add a comma at the begining of next sql string
            //         sql = sql + ` WHERE ID = ${cg.id}`;
            //     // }
            // })
            // if(data.companyInfo.new){
            //     sql += "INSERT INTO jarvisdb.applicants (`Company Name`, `DBA Name`, `Contact Name`, `Phone Number`, `Email Address`, `Address`, `City`, `County`, `State`, `Zip`, `Is SSN`, "+
            //     "`Federal ID`, `SOS Type`, `Description of Business`, `Date Established`, NAICS, Website, `Incorporated State`) "+"VALUES";
            //     sql+= util.format("(\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\", %s,\"%s\",\"%s\",\"%s\", \"%s\", %s,\"%s\", \"%s\");", 
            //                         data.companyInfo.companyName, data.companyInfo.dbaName, data.companyInfo.contactName, data.companyInfo.phoneNumber, data.companyInfo.emailAddress, data.companyInfo.address, data.companyInfo.city, data.companyInfo.county, data.companyInfo.state, data.companyInfo.zip, data.companyInfo.isSSN, data.companyInfo.fedID, data.companyInfo.sosType, data.companyInfo.description, data.companyInfo.establishedDate, data.companyInfo.naics, data.companyInfo.website, data.companyInfo.incorporatedState);
            // }
            // else{
                sql += " UPDATE jarvisdb.applicants SET ";
                //repeat the following line for each column in the table;
                
                if (data.companyInfo.companyName){
                    sql+= (" `Company Name` = \"" + data.companyInfo.companyName+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.dbaName){
                    sql+= (" `DBA Name` = \"" + data.companyInfo.dbaName+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.contactName){
                    sql+= (" `Contact Name` = \"" + data.companyInfo.contactName+ "\","); //only strings need the single quote
                
                }
                if (data.companyInfo.phoneNumber){
                    sql+= (" `Phone Number` = \"" + data.companyInfo.phoneNumber+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.emailAddress){
                    sql+= (" `Email Address` = \"" + data.companyInfo.emailAddress+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.address){
                    sql+= (" Address = \"" + data.companyInfo.address+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.city){
                    sql+= (" City = \"" + data.companyInfo.city+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.county){
                    sql+= (" County = \"" + data.companyInfo.county+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.state){
                    sql+= (" State = \"" + data.companyInfo.state+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.zip){
                    sql+= (" Zip = \"" + data.companyInfo.zip+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.isSSN){
                    sql+= (" `Is SSN` = " + data.companyInfo.isSSN+ ","); //only strings need the single quote
                
                }if (data.companyInfo.fedID){
                    sql+= (" `Federal ID` = \"" + data.companyInfo.fedID+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.sosType){
                    sql+= (" `SOS Type` = \"" + data.companyInfo.sosType+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.description){
                    sql+= (" `Description of Business` = \"" + data.companyInfo.description+ "\","); //only strings need the single quote
                
                }if (data.companyInfo.establishDate){
                    sql+= (" `Establish Date` = " + data.companyInfo.establishDate+ ","); //only strings need the single quote
                
                }if (data.companyInfo.naics){
                    sql+= (" NAICS = " + data.companyInfo.naics+ ","); //only strings need the single quote
                
                }if (data.companyInfo.website){
                    sql+= (" Website = \"" + data.companyInfo.website+ "\","); //only strings need the single quote
                }if(data.companyInfo.incorporatedState){
                    sql+=(" `Incorporated State` = \""+ data.companyInfo.incorporatedState+"\",")
                }
                
                sql = sql.slice(0, -1);
                //for additional fields add a comma at the begining of next sql string
                sql = sql + ` WHERE ID = ${data.companyInfo.id};\n`;
            // }
            // if(data.application.new){
            //     sql += "INSERT INTO jarvisdb.applications (\`Company ID\`, \`Submitted Date\`, \`Source ID\`, \`Submitted Amount\`, \`Equipment Cost\`, \`Collateral Description\`, \`Purpose of Loan\`, \`BDO\`, \`Requested Terms\`, \`Requested Frequency\`, \`Requested Advanced\`, Decision) "+"VALUES ";
            //     sql+= util.format("(%s,now(),%s,\"%s\",\"%s\",\"%s\",\"%s\", %s, \"%s\", \"%s\", \"%s\",\"%s\");", 
            //                          data.application.companyID, data.application.sourceID, data.application.submittedAmount, data.application.equipmentCost, data.application.collateralDescription, data.application.purposeOfLoan, data.application.bdo, data.application.terms, data.application.frequency, data.application.advance, data.application.decision);
            // }
            // else{
                sql += " UPDATE jarvisdb.applications SET ";
                //repeat the following line for each column in the table;
                
                if (data.application.companyID){
                    sql+= (" `Company ID` = " + data.application.companyID+ ","); //only strings need the single quote
                
                }if (data.application.submittedDate){
                    sql+= (" `Submitted Date` = " + data.application.submittedDate+ ","); //only strings need the single quote
                
                }if (data.application.sourceID){
                    sql+= (" `Source ID` = " + data.application.sourceID+ ","); //only strings need the single quote
                
                }
                if (data.application.submittedAmount){
                    sql+= (" `Submitted Amount` = \"" + data.application.submittedAmount+ "\","); //only strings need the single quote
                
                }if (data.application.equipmentCost){
                    sql+= (" `Equipment Cost` = \"" + data.application.equipmentCost+ "\","); //only strings need the single quote
                
                }if (data.application.collateralDescription){
                    sql+= (" `Collateral Description` = \"" + data.application.collateralDescription+ "\","); //only strings need the single quote
                
                }if (data.application.decision){
                    sql+= (" `Decision` = \"" + data.application.decision+ "\","); //only strings need the single quote
                
                }if (data.application.purposeOfLoan){
                    sql+= (" `Purpose of Loan` = \"" + data.application.purposeOfLoan+ "\","); //only strings need the single quote
                
                }if (data.application.bdo){
                    sql+= (" `BDO` = " + data.application.bdo+ ","); //only strings need the single quote
                
                }if (data.application.terms){
                    sql+= (" `Requested Terms` = \"" + data.application.terms+ "\","); //only strings need the single quote
                
                }if (data.application.frequency){
                    sql+= (" `Requested Frequency` = \"" + data.application.frequency+ "\","); //only strings need the single quote
                
                }if (data.application.advance){
                    sql+= (" `Requested Advanced` = \"" + data.application.advance+ "\","); //only strings need the single quote
                
                }
                
                sql = sql.slice(0, -1);
                //for additional fields add a comma at the begining of next sql string
                sql = sql + ` WHERE ID = ${data.application.id};\n`;
            // }
            // if(data.source.new){
            //     sql += "INSERT INTO jarvisdb.sources (\`Company Name\`, \`Internal Name\`, \`DBA Name\`, \`Source Class\`, \`Status\`, \`Renewal Date\`, CFL, \`CFL Number\`, \`Federal ID\`, \`SOS Type\`, \`Application Received\`, \`Agreement Date\`, \`Peak Reference\`) "+"VALUES";
            //     sql+= util.format(" (\"%s\", \"%s\", \"%s\", %s, \"%s\", %s, %s, \"%s\", \"%s\", \"%s\", %s, %s, \"%s\");", 
            //                         data.source.companyName, data.source.internalName, data.source.dbaName, data.source.sourceClass, data.source.status, data.source.renewalDate, data.source.cfl, data.source.cflNumber, data.source.fedID, data.source.sosType, data.source.applicationRecieved, data.source.agreementDate, data.source.peakRef);//data.source.applicationRecieved, 
            // }
            // else{
                sql += " UPDATE jarvisdb.sources SET ";
                //repeat the following line for each column in the table;
    
                if (data.source.companyName){
                    sql+= (" \`Company Name\` = \"" + data.source.companyName+ "\","); //only strings need the single quote
                
                }if (data.source.internalName){
                    sql+= (" \`Internal Name\` = \"" + data.source.internalName+ "\","); //only strings need the single quote
                
                }if (data.source.dbaName){
                    sql+= (" \`DBA Name\` = \"" + data.source.dbaName+ "\","); //only strings need the single quote
                
                }if (data.source.sourceClass){
                    sql+= (" \`Source Class\` = " + data.source.sourceClass+ ","); //only strings need the single quote
                
                }
                if (data.source.status){
                    sql+= (" Status = \"" + data.source.status+ "\","); //only strings need the single quote
                
                }if (data.source.renewalDate){
                    var string = (data.source.renewalDate+"")
                    string = string.indexOf("T")===-1?string:string.split("T")[0]
                    sql+= (" \`Renewal Date\` = \"" + string+ "\","); //only strings need the single quote
                
                }if (data.source.cfl){
                    sql+= (" CFL = " + data.source.cfl+ ","); //only strings need the single quote
                
                }if (data.source.cflNumber){
                    sql+= (" \`CFL Number\` = \"" + data.source.cflNumber+ "\","); //only strings need the single quote
                
                }if (data.source.fedID){
                    sql+= (" \`Federal ID\` = \"" + data.source.fedID+ "\","); //only strings need the single quote
                
                }if (data.source.sosType){
                    sql+= (" \`SOS Type\` = \"" + data.source.sosType+ "\","); //only strings need the single quote
                
                }if (data.source.applicationReceived){
                    var string = (data.source.applicationReceived+"")
                    string = string.indexOf("T")===-1?string:string.split("T")[0]
                    sql+= (" \`Application Received\` = \"" + string+ "\","); //only strings need the single quote
                
                }if (data.source.agreementDate){
                    var string = (data.source.agreementDate+"")
                    string = string.indexOf("T")===-1?string:string.split("T")[0]
                    sql+= (" \`Agreement Date\` = \"" + string+ "\","); //only strings need the single quote
                
                }if(data.source.peakReferance){
                    sql+=(" \`Peak Reference\` = \"" + data.source.peakReferance + "\",")
                }
                sql = sql.slice(0, -1);
                //for additional fields add a comma at the begining of next sql string
                sql = sql + ` WHERE ID = ${data.source.id};`;
            // }



            db.executeSql(sql,function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports. updateAll = function(req, resp, data){
    
}
