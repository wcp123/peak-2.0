var db = require("../db");
var msg = require("../httpMsgs");
var util = require("util");
var setting = require('../settings');
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.payment`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.payment WHERE ID = ?`, [id],function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.payment set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.payment set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.payment (\`Source ID\`, \`ACH or Wire\`, \`Acount Number\`, \`Routing Number\`, \`Bank Name\`) VALUES`;
            sql+= "(?,?,?,?,?)"
    
                                            
            db.executeSql(sql,[data.sourceID, data.achOrwire, data.accountNum, data.routingNum, data.bankName], function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`payment\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.sourceID){
                sql+="`Source ID`, "
                
                sqlEnd+=`?, `
                options.push(data.sourceID)
            }
            if(data.achWaived){
                sql+="`ACH or Wire`, "
                sqlEnd+=`?, `
                options.push(data.achWaived)
            }
            if(data.accountNum){
                sql+="`Account Number`, "
                sqlEnd+=`?, `
                options.push(data.accountNum)
            }
            if(data.routingNum){
                sql+="`Routing Number`, "
                sqlEnd+=`?, `
                options.push(data.routingNum)
            }
            if(data.bankName){
                sql+="`Bank Name`, "
                sqlEnd+=`?, `
                options.push(data.bankName)
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.payment SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.sourceID){
                sql+= (" `Source ID` = ?,"); //only strings need the single quote
                options.push(data.sourceID)

            }
            if (data.achOrwire){
                sql+= (" `ACH or Wire` = ?,"); //only strings need the single quote
                options.push(data.achOrwire)

            }
            if (data.accountNum){
                sql+= (" `Account Number` = ?,"); //only strings need the single quote
                options.push(data.accountNum)

            }if (data.routingNum){
                sql+= (" `Routing Number` = ?,"); //only strings need the single quote
                options.push(data.routingNum)

            }if (data.bankName){
                sql+= (" `Bank Name` = ?,"); //only strings need the single quote
                options.push(data.bankName)

            }
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + `WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, id){
    try{
        db.executeSql(`DELETE * FROM ${settings.sqlConfig.database}.payment WHERE ID = ?`, [id],function (x, err){
            if(err){
                msg.show500(req, resp, err);
            }
            else{
                msg.sendJSON(req,resp, x)
            }
        })

    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
