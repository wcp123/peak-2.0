var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
const { application } = require("express");
const setttings = require('../settings')

exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\`applicant billing\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
        
    });
};

//this function needs put in the applicants.js file in the controllers module
exports.get = function (req, resp, applicantID){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\`applicant billing\` WHERE \`applicant ID\` = ?`,[applicantID], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getBill = function (req, resp, applicantID){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\`applicant billing\` WHERE \`ID\` = ?`,[applicantID], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${setttings.sqlConfig.database}.\`applicant billing\` set \`lock\` = 1 WHERE ID = ? `,[id],  (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }
        
        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${setttings.sqlConfig.database}.\`applicant billing\` set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }
        
        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        // if (data){
        //     var sql = "INSERT INTO ${setttings.sqlConfig.database}.\`applicant billing\` ";
        //     sql += "(\`Applicant ID\`, \`Wire or ACH\`, \`Account Number\`, \`Routing Number\`, \`Bank Name\`) "+"VALUES ";
        //         sql+= util.format("(%d, %d, '%s', '%s', '%s')", 
        //                         data.applicantID, data.wireOrACH, data.accountNumber, data.routingNumber, data.bankName);

        if (data){
            var options = []
            var sql = `INSERT INTO ${setttings.sqlConfig.database}.\`applicant billing\` (`
            var sqlEnd = ") VALUES (";
            if(data.applicantID){
                sql+="`Applicant ID`, "
                sqlEnd+=`?, `
                options.push(data.applicantID)
            }
            if(data.wireOrACH){
                sql+="\`Wire or ACH\`, "
                sqlEnd+=`?, `
                options.push(data.wireOrACH)
            }
            if(data.accountNumber){
                sql+="\`Account Number\`, "
                sqlEnd+=`?, `
                options.push(data.accountNumber)
            }
            if(data.routingNumber){
                sql+="\`Routing Number\`, "
                sqlEnd+=`?, `
                options.push(data.routingNumber)
            }
            if(data.bankName){
                sql+="`Bank Name`, "
                sqlEnd+=`?, `
                options.push(data.bankName)
            }
            if(data.accountHoldersName){
                sql+="`Account Holders Name`, "
                sqlEnd+=`?, `
                options.push(data.accountHoldersName)
            }
            
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
            
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var options = []
            var sql = `UPDATE ${setttings.sqlConfig.database}.\`applicant billing\` SET `;
            //repeat the following line for each column in the table;
            if(data.applicantID){
                sql+= (" `Applicant ID` = ?,")
                options.push(data.applicantID)
            }
            if (data.accountNumber){
                sql+= (" `Account Number` = ?,"); //only strings need the single quote
                options.push(data.accountHoldersName)
            
            }
            if (data.routingNumber){
                sql+= (" `Routing Number` = ?,"); //only strings need the single quote
                options.push(data.routingNumber)
            
            }if(data.bankName){
                sql+=(" `Bank Name` = ?,")
                options.push(data.bankName)
            }if(data.wireOrAch){
                sql+=(" `Wire or ACH` = ?,")
                options.push(data.wireOrACH)
            }
            if(data.accountHoldersName){
                sql+=(" `Account Holders Name` = ?,")
                options.push(data.accountHoldersName)
            }
            
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            options.push(data.id)
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${setttings.sqlConfig.database}.\`applicant billing\``;

            sql = sql + ` WHERE ID =?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};