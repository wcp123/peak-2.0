var db = require("../db");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require("../settings")
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`disbursements\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`disbursements\` WHERE \`Doc ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.disbursements set \`lock\` = 1 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.disbursements set lock = 0 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        console.log(data)
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`disbursements\` (`
            var sqlEnd = ") VALUES (";
            var options=[]
            if(data.docID){
                sql+="`Doc ID`, "
                options.push(data.docID)
                sqlEnd+=`?, `
            }
            if(data.docFeeAdd){
                sql+="`Doc Fee Add`, "
                options.push(data.docFeeAdd)
                sqlEnd+=`?, `
            }
            if(data.docFeeDeduct){
                sql+="`Doc Fee Deduct`, "
                options.push(data.docFeeDeduct)
                sqlEnd+=`?, `
            }
            if(data.commissionDue){
                sql+="`Commission Due`, "
                options.push(data.commissionDue)
                sqlEnd+=`?, `
            }if(data.commissionHeld){
                sql+="`Commission Held`, "
                options.push(data.commissionHeld)
                sqlEnd+=`?, `
            }
            if(data.releaseDate){
                sql+="`Release Date`, "
                options.push(data.releaseDate)
                sqlEnd+=`?, `
            }
            if(data.docFeeBalance){
                sql+="`Doc Fee Balance`, "
                options.push(data.docFeeBalance)
                sqlEnd+=`?, `
            }
            if(data.sourceReference){
                sql+="`Source Reference`, "
                options.push(data.sourceReference)
                sqlEnd+=`?, `
            }
            if(data.shortfall){
                sql+="`Shortfall`, "
                options.push(data.shortfall)
                sqlEnd+=`?, `
            }
            if(data.shortfallComment){
                sql+="`Shortfall Comment`, "
                options.push(data.shortfallComment)
                sqlEnd+=`?, `
            }
            if(data.discountedEquipmentCost){
                sql+="`Discounted Equipment Cost`, "
                options.push(data.discountedEquipmentCost)
                sqlEnd+=`?, `
            }
            if(data.docFee){
                sql+="`Doc Fee`, "
                options.push(data.docFee)
                sqlEnd+=`?, `
            }
            if(data.dedicatedFee){
                sql+="`Dedicated Fee`, "
                options.push(data.dedicatedFee)
                sqlEnd+=`?, `
            }
            if(data.docusignFee){
                sql+="`Docusign Fee`, "
                options.push(data.docusignFee)
                sqlEnd+=`?, `
            }
            if(data.wireFee){
                sql+="`Wire Fee`, "
                options.push(data.wireFee)
                sqlEnd+=`?, `
            }
            if(data.titleFee){
                sql+="`Title Fee`, "
                options.push(data.titleFee)
                sqlEnd+=`?, `
            }
            if(data.fedExFee){
                sql+="`FedEx Fee`, "
                options.push(data.fedExFee)
                sqlEnd+=`?, `
            }
            if(data.inspectionFee){
                sql+="`Inspection Fee`, "
                options.push(data.inspectionFee)
                sqlEnd+=`?, `
            }

            
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        console.log(ex)
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{

        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`disbursements\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.docID){
                options.push(data.docID)
                sql+= (" `Doc ID` = ?,"); //only strings need the double quote

            }
            if (data.docFeeAdd){
                options.push(data.docFeeAdd)
                sql+= (" `Doc Fee Add` = ?,"); //only strings need the double quote
            }if (data.docFeeDeduct){
                options.push(data.docFeeBalance)
                sql+= (" `Doc Fee Deduct` = ?,"); //only strings need the double quote

            }if (data.commissionDue){
                options.push(data.commissionDue)
                sql+= (" `Commission Due` = ?,"); //only strings need the double quote

            }if (data.commissionHeld){
                options.push(data.commissionHeld)
                sql+= (" `Commission Held` = ?,"); //only strings need the double quote

            }if (data.releaseDate){
                options.push(data.releaseDate)
                sql+= (" `Release Date` = ?,"); //only strings need the double quote

            }if (data.sourceReference){
                options.push(data.sourceReference)
                sql+= (" `Source Reference` = ?,"); //only strings need the double quote

            }
            if (data.billingState){
            options.push(data.billingState)//     
            sql+= (" `Billing State` = ?,"); //only strings need the double quote

            }
            if (data.shortfall){
                options.push(data.shortfall)
                sql+= (" `Shortfall` = ?,"); //only strings need the double quote

            }if(data.shortfallComment){
                options.push(data.shortfallComment)
                sql+=" \`Shortfall Comment\` = ?,"
            }if (data.discountedEquipmentCost){
                options.push(data.discountedEquipmentCost)
                sql+= (" \`Discounted Equipment Cost\` = ?,"); //only strings need the double quote

            }if (data.docFee){
                options.push(data.docFee)
                sql+= (" \`Doc Fee\` = ?,"); //only strings need the double quote

            }if (data.dedicatedFee){
                options.push(data.dedicatedFee)
                sql+= (" \`Dedicated Fee\` = ?,"); //only strings need the double quote

            }if (data.docusignFee){
                options.push(data.docusignFee)
                sql+= (" \`Docusign Fee\` = ?,"); //only strings need the double quote

            }
            if (data.wireFee){
                options.push(data.wireFee)
                sql+= (" \`Wire Fee\` = ?,"); //only strings need the double quote

            }
            if (data.titleFee){
                options.push(data.titleFee)
                sql+= (" \`Title Fee\` = ?,"); //only strings need the double quote

            }if (data.fedExFee){
                options.push(data.fedExFee)
                sql+= (" \`FedEx Fee\` = ?,"); //only strings need the double quote

            }if (data.inspectionFee){
                options.push(data.inspectionFee)
                sql+= (" \`Inspection Fee\` = ?,"); //only strings need the double quote

            }if (data.dedicatedFee){
                options.push(data.dedicatedFee)
                sql+= (" \`Dedicated Fee\` = ?,"); //only strings need the double quote

            }



            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`disbursements\``;
            sql = sql + ` WHERE ID = ?`;
            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
