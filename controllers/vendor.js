var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings');
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.vendors`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.vendors WHERE ID = ?`, [id],function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getAllVendorsForApplication = function (req, resp, id){
    db.executeSql(`
        SELECT
            v.* 
        FROM
            ${settings.sqlConfig.database}.\`vendors\` v
        JOIN
            ${settings.sqlConfig.database}.\`application to vendor\` av
                ON av.\`Vendor ID\` = v.\`ID\`
        WHERE
            av.\`Application ID\` = ?
        `,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};


exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.vendors set lock = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.vendors set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`vendors\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.companyName){
                sql+="`Company Name`, "
                options.push(data.companyName)
                sqlEnd+=`?, `
            }
            if(data.internalName){
                sql+="`Internal Name`, "
                options.push(data.internalName)
                sqlEnd+=`?, `
            }
            if(data.dbaName){
                sql+="`DBA Name`, "
                options.push(data.dbaName)
                sqlEnd+=`?, `
            }
            if(data.phoneNumber){
                sql+="`Phone Number`, "
                options.push(data.phoneNumber)
                sqlEnd+=`?, `
            }
            if(data.address){
                sql+="`Address`, "
                options.push(data.address)
                sqlEnd+=`?, `
            }
            if(data.city){
                sql+="`City`, "
                options.push(data.city)
                sqlEnd+=`?, `
            }if(data.county){
                sql+="`County`, "
                options.push(data.county)
                sqlEnd+=`?, `
            }
            if(data.state){
                sql+="`State`, "
                options.push(data.state)
                sqlEnd+=`?, `
            }
            if(data.zip){
                sql+="`Zip`, "
                options.push(data.zip)
                sqlEnd+=`?, `
            }
            if(data.federalID){
                sql+="`Federal ID`, "
                options.push(data.federalID)
                sqlEnd+=`?, `
            }
            if(data.approvedForPrefunding){
                sql+="`Approved for Prefunding`, "
                options.push(data.approvedForPrefunding)
                sqlEnd+=`?, `
            }
            else if(data.approvedForPrefunding===0){
                sql+="`Approved for Prefunding`, "
                options.push(data.approvedForPrefunding)
                sqlEnd+=`?, `
            }
            if(data.timeInBusiness){
                sql+="`Time in Business`, "
                options.push(data.timeInBusiness)
                sqlEnd+=`?, `
            }if(data.prefundingTerms){
                sql+="`Prefunding Terms`, "
                options.push(data.prefundingTerms)
                sqlEnd+=`?, `
            }
            if(data.sosType){
                sql+="`SOS Type`, "
                options.push(data.sosType)
                sqlEnd+=`?, `
            }
            if(data.approved){
                sql+="`Approved`, "
                options.push(data.approved)
                sqlEnd+=`?, `
            }
            else if(data.approved===0){
                sql+="`Approved`, "
                options.push(data.approved)
                sqlEnd+=`?, `
            }
            if(data.ofac){
                sql+="`OFAC`, "
                options.push(data.ofac)
                sqlEnd+=`?, `
            }
            if(data.sourceReference){
                sql+="`Source Reference`, "
                options.push(data.sourceReference)
                sqlEnd+=`?, `
            }
            if(data.email){
                sql+="`Email`, "
                options.push(data.email)
                sqlEnd+=`?, `
            }if(data.website){
                sql+="`Website`, "
                options.push(data.website)
                sqlEnd+=`?, `
            }if(data.comment){
                sql+="`Comment`, "
                options.push(data.comment)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (err) {
        msg.show500(req, resp, err);
    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var sql = `UPDATE ${settings.sqlConfig.database}.vendors SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.companyName){
                sql+= (" `Company Name` = ?,"); //only strings need the single quote
                options.push(data.companyName)

            }if (data.dbaName){
                sql+= (" `DBA Name` = ?,"); //only strings need the single quote
                options.push(data.dbaName)

            }if (data.address){
                sql+= (" `Address` = ?,"); //only strings need the single quote
                options.push(data.address)

            }
            if(data.internalName){
                sql+=(" `Internal Name` = ?,")
                options.push(data.internalName)
            }
            if (data.city){
                sql+= (" City = ?,"); //only strings need the single quote
                options.push(data.city)

            }if (data.county){
                sql+= (" County = ?,"); //only strings need the single quote
                options.push(data.county)

            }if (data.state){
                sql+= (" State = ?,"); //only strings need the single quote
                options.push(data.state)

            }if (data.zip){
                sql+= (" Zip = ?,"); //only strings need the single quote
                options.push(data.zip)

            }if (data.phoneNumber){
                sql+= (" `Phone Number` = ?,"); //only strings need the single quote
                options.push(data.phoneNumber)

            }if (data.email){
                sql+= (" Email = ?,"); //only strings need the single quote
                options.push(data.email)

            }if (data.website){
                sql+= (" Website = ?,"); //only strings need the single quote
                options.push(data.website)

            }if (data.federalID){
                sql+= (" `Federal ID` = ?,"); //only strings need the single quote
                options.push(data.federalID)

            }if(data.timeInBusiness){
                sql+=(" `Time in Business` = ?,")
                options.push(data.timeInBusiness)
            }if(data.approvedForPrefunding){
                sql+=(" `Approved for Prefunding` = ?,")
                options.push(data.approvedForPrefunding)
            }
            else if(data.approvedForPrefunding===0){
                sql+=(" `Approved for Prefunding` = ?,")
                options.push(data.approvedForPrefunding)
            }
            if(data.prefundingTerms){
                sql+=(" `Prefunding Terms` = ?,")
                options.push(data.prefundingTerms)
            }if(data.sosType){
                sql+=(" `SOS Type` = ?,")
                options.push(data.sosType)
            }if(data.approved){
                sql+=(" Approved = ?,")
                options.push(data.approved)
            }
            else if(data.approved===0){
                sql+=(" Approved = ?,")
                options.push(data.approved)
            }
            if(data.approvalDate){
                var date = (data.approvalDate+'').split("T")[0]
                sql+=(" `Approval Date` = ?,")
                options.push(date)
            }
            if(data.ofac){
                sql+=(" OFAC = ?,")
                options.push(data.ofac)
            }
            else if(data.ofac===0){
                sql+=(" OFAC = ?,")
                options.push(data.ofac)
            }
            if(data.comment){
                sql+=(" Comment = ?,")
                options.push(data.comment)
            }
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            var sql = `DELETE FROM ${settings.sqlConfig.database}.vendors`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
