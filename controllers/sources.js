var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.sources`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.sources WHERE ID = ?`, [id],function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.sources set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.sources set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`sources\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.companyName){
                sql+="`Company Name`, "
                options.push(data.companyName)
                sqlEnd+=`?, `
            }
            if(data.internalName){
                sql+="`Internal Name`, "
                options.push(data.internalName)
                sqlEnd+=`?, `
            }
            if(data.dbaName){
                sql+="`DBA Name`, "
                options.push(data.dbaName)
                sqlEnd+=`?, `
            }
            if(data.sourceClass){
                sql+="`Source Class`, "
                options.push(data.sourceClass)
                sqlEnd+=`?, `
            }
            if(data.status){
                sql+="`Status`, "
                options.push(data.status)
                sqlEnd+=`?, `
            }
            if(data.renewalDate){
                sql+="`Renewal Date`, "
                options.push(data.renewalDate)
                sqlEnd+=`?, `
            }if(data.cfl){
                sql+="`CFL`, "
                options.push(data.cfl)
                sqlEnd+=`?, `
            }
            if(data.cflNumber){
                sql+="`CFL Number`, "
                options.push(data.cflNumber)
                sqlEnd+=`?, `
            }
            if(data.fedID){
                sql+="`Federal ID`, "
                options.push(data.fedID)
                sqlEnd+=`?, `
            }
            if(data.sosType){
                sql+="`SOS Type`, "
                options.push(data.sosType)
                sqlEnd+=`?, `
            }
            if(data.applicationReceived){
                sql+="`Application Received`, "
                options.push(data.applicationReceived)
                sqlEnd+=`?, `
            }
            if(data.agreementDate){
                sql+="`Agreement Date`, "
                options.push(data.agreementDate)
                sqlEnd+=`?, `
            }if(data.peakRef){
                sql+="`Peak Reference`, "
                options.push(data.peakRef)
                sqlEnd+=`?, `
            }if(data.comment){
                sql+="`Comment`, "
                options.push(data.comment)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }

        else{
            throw new Error("Input not valid");
        }
    }
    catch (er) {
        msg.show500(req, resp, er);
    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            var sql = `UPDATE ${settings.sqlConfig.database}.sources SET `;
            //repeat the following line for each column in the table;

            if (data.companyName){
                sql+= (" \`Company Name\` = ?,"); //only strings need the single quote
                options.push(data.companyName)

            }if (data.internalName){
                sql+= (" \`Internal Name\` = ?,"); //only strings need the single quote
                options.push(data.internalName)

            }if (data.dbaName){
                sql+= (" \`DBA Name\` = ?,"); //only strings need the single quote
                options.push(data.dbaName)

            }if (data.sourceClass){
                sql+= (" \`Source Class\` = ?,"); //only strings need the single quote
                options.push(data.sourceClass)

            }
            if (data.status){
                sql+= (" Status = ?,"); //only strings need the single quote
                options.push(data.status)

            }if (data.renewalDate){
                var string = (data.renewalDate+'')
                string = string.indexOf('T')===-1?string:string.split('T')[0]
                sql+= (" \`Renewal Date\` = ?,"); //only strings need the single quote
                options.push(string)

            }if (data.cfl){
                sql+= (" CFL = ?,"); //only strings need the single quote
                options.push(data.cfl)

            }if (data.cflNumber){
                sql+= (" \`CFL Number\` = ?,"); //only strings need the single quote
                options.push(data.cflNumber)

            }if (data.fedID){
                sql+= (" \`Federal ID\` = ?,"); //only strings need the single quote
                options.push(data.fedID)

            }if (data.sosType){
                sql+= (" \`SOS Type\` = ?,"); //only strings need the single quote
                options.push(data.sosType)

            }if (data.applicationReceived){
                var string = (data.applicationReceived+'')
                string = string.indexOf('T')===-1?string:string.split('T')[0]
                sql+= (" \`Application Received\` = ?,"); //only strings need the single quote
                options.push(string)

            }if (data.agreementDate){
                var string = (data.agreementDate+'')
                string = string.indexOf('T')===-1?string:string.split('T')[0]
                sql+= (" \`Agreement Date\` = ?,"); //only strings need the single quote
                options.push(string)

            }
            if(data.approved){
                sql+=" \`Approved\` = ?,"
                options.push(data.approved)
            }
            if(data.approvalDate){
                var string = (data.approvalDate+'')
                string  = string.indexOf("T")===-1?string:string.split("T")[0]
                sql+=" \`Approval Date\` = ?,"
                options.push(data.approvalDate)
            }
            if(data.peakReferance){
                sql+=(" \`Peak Reference\` = ?,")
                options.push(data.peakReferance)
            }if(data.comment){
                sql+=(" \`Comment\` = ?,")
                options.push(data.comment)
            }
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            console.log(data.id)
            var id = data['id']
            sql = sql + " WHERE ID = ?";
            //
            options.push(id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql =   `DELETE FROM ${settings.sqlConfig.database}.sources `;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
