var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')

let handleDate = (date) =>{
    return String(date).includes('T')?String(date).split('T')[0]:String(date)
}
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.followup`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        } 
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.followup WHERE \`Doc ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.followup set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.followup set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        console.log(data)
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`followup\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.requiredItem){
                sql+="`Required Item`, "
                sqlEnd+=`?, `
                options.push(data.requiredItem)
            }
            if(data.docID){
                sql+="`Doc ID`, "
                sqlEnd+=`?, `
                options.push(data.docID)
            }
            if(data.completedDate){
                sql+="`Completed Date`, "
                sqlEnd+=`?, `
                options.push(handleDate(data.completedDate))
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.followup SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.docID){
                sql+= (" `Doc ID` = ?,"); //only strings need the single quote
                options.push(data.docID)
            }
            if (data.requiredItem){
                sql+= (" `Required Item` = ?,"); //only strings need the single quote
                options.push(data.requiredItem)
            }if (data.completedDate){
                sql+= (" `Completed Date` = ?,"); //only strings need the single quote
                options.push(handleDate(data.completedDate))
            }


            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.followup`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
