var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.status`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.status WHERE \`Doc ID\` = ?`, [id],function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.status set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.status set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`status\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.docID){
                sql+="`Doc ID`, "
                options.push(data.docID)
                sqlEnd+=`?, `
                
            }
            if(data.docsSentOut){
                var date = String(data.docsSentOut).indexOf('T')===-1?data.docsSentOut:String(data.docsSentOut).split('T')[0]
                sql+="`Docs Sent Out`, "
                options.push(date)
                sqlEnd+=`?, `
            }
            if(data.redoc){
                sql+="`Redoc`, "
                options.push(data.redoc)
                sqlEnd+=`?, `
            }
            if(data.docsReceived){
                var date = String(data.docsReceived).indexOf('T')===-1?data.docsReceived:String(data.docsReceived).split('T')[0]

                sql+="`Docs Received`, "
                options.push(date)
                sqlEnd+=`?, `
            }
            if(data.inspectionCompleted){
                var date = String(data.inspectionCompleted).indexOf('T')===-1?data.inspectionCompleted:String(data.inspectionCompleted).split('T')[0]

                sql+="`Inspection Completed`, "
                options.push(date)
                sqlEnd+=`?, `
            }
            if(data.uccRecorded){
                var date = String(data.uccRecorded).indexOf('T')===-1?data.uccRecorded:String(data.uccRecorded).split('T')[0]

                sql+="`UCC Recorded`, "
                options.push(date)
                sqlEnd+=`?, `
            }if(data.uccFixture){
                var date = String(data.uccFixture).indexOf('T')===-1?data.uccFixture:String(data.uccFixture).split('T')[0]

                sql+="`UCC Fixture`, "
                options.push(date)
                sqlEnd+=`?, `
            }
            if(data.status){
                sql+="`Status`, "
                options.push(data.status)
                sqlEnd+=`?, `
            }
            if(data.statusDate){
                var date = String(data.statusDate).indexOf('T')===-1?data.statusDate:String(data.statusDate).split('T')[0]

                sql+="`Status Date`, "
                options.push(date)
                sqlEnd+=`?, `
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.status SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.docID){
                sql+= (" `Doc ID` = ?,"); //only strings need the single quote
                options.push(data.docID)

            }
            if (data.docsSentOut){
                sql+= (" `Docs Sent Out` = ?,"); //only strings need the single quote
                options.push(data.docsSentOut)

            }if (data.docsReceived){
                sql+= (" `Docs Received` = ?,"); //only strings need the single quote
                options.push(data.docsReceived)

            }if (data.redoc){
                sql+= (" `Redoc` = ?,"); //only strings need the single quote
                options.push(data.redoc)

            }if (data.inspectionCompleted){
                sql+= (" `Inspection Completed` = ?,"); //only strings need the single quote
                options.push(data.inspectionCompleted)

            }if (data.uccRecorded){
                sql+= (" `UCC Recorded` = ?,"); //only strings need the single quote
                options.push(data.uccRecorded)

            }if (data.uccFixture){
                sql+= (" `UCC Fixture` = ?,"); //only strings need the single quote
                options.push(data.uccFixture)

            }if (data.status){
                sql+= (" `Status` = ?,"); //only strings need the single quote
                options.push(data.status)
            }if (data.statusDate){
                sql+= (" `Status Date` = ?,"); //only strings need the single quote
                options.push(data.statusDate)

            }


            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.status`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
