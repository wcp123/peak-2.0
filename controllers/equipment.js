var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
clean = (value)=>{
    return String(value).replace('"',"\\\"")
}
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.equipment`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.equipment WHERE ID = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.equipment set \`lock\` = 1 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.equipment set lock = 0 WHERE ID = ?`,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
          var sql = `INSERT INTO ${settings.sqlConfig.database}.\`equipment\` (`
          var sqlEnd = ") VALUES (";
          var options = []
          if(data.equipmentDescription){
              sql+="`Equipment Description`, "
              
              sqlEnd+=`?, `
              options.push(data.equipmentDescription)
          }
          if(data.equipmentLocation){
              sql+="`Equipment Location`, "
              sqlEnd+=`?, `
              options.push(data.equipmentLocation)
          }
          if(data.equipmentCost){
              sql+="`Equipment Cost`, "
              sqlEnd+=`?, `
              options.push(data.equipmentCost)
          }
          if(data.vinOrSerial){
              sql+="`VIN Or Serial`, "
              sqlEnd+=`?, `
              options.push(data.vinOrSerial)
          }
          if(data.serialNumber){
              sql+="`Serial Number`, "
              sqlEnd+=`?, `
              options.push(data.serialNumber)
          }
          if(data.titled){
              sql+="`Titled`, "
              sqlEnd+=`?, `
              options.push(data.titled)
          }
          if(data.vendorID){
            sql+="`Vendor ID`, "
            sqlEnd+=`?, `
            options.push(data.vendorID)
          }
          if(data.city){
            sql+="`City`, "
            sqlEnd+=`?, `
            options.push(data.city)
          }
          if(data.state){
            sql+="`State`, "
            sqlEnd+=`?, `
            options.push(data.state)
          }
          if(data.zip){
            sql+="`Zip`, "
            sqlEnd+=`?, `
            options.push(data.zip)
          }
          if(data.county){
            sql+="`County`, "
            sqlEnd+=`?, `
            options.push(data.county)
          }
          sql = sql.slice(0, -2);
          sqlEnd = sqlEnd.slice(0, -2)
          sqlEnd += ")"
          sql += sqlEnd
          db.executeSql(sql,options, function (x, err){
              if(err){
                  msg.show500(req, resp, err);
              }
              else{
                  msg.sendJSON(req,resp, x.insertId)
              }
          });
      }
      else{
          throw new Error("Input not valid");
      }

    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.equipment SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.equipmentDescription){
                sql+= (" `Equipment Description` = ?,"); //only strings need the single quote
                options.push(data.equipmentDescription)

            }
            if (data.equipmentLocation){
                sql+= (" `Equipment Location` = ?,"); //only strings need the single quote
                options.push(data.equipmentLocation)

            }if (data.equipmentCost){
                sql+= (" `Equipment Cost` = ?,"); //only strings need the single quote
                options.push(data.equipmentCost)

            }if (data.vinOrSerial){
                sql+= (" `VIN Or Serial` = ?,"); //only strings need the single quote
                options.push(data.vinOrSerial)

            }

            if (data.serialNumber){
                sql+= (" `Serial Number` = ?,"); //only strings need the single quote
                options.push(data.serialNumber)

            }
            if(data.county){
                sql+= (" `County` = ?,")
                options.push(data.county)
            }
            if(data.city){
                sql+= (" `City` = ?,")
                options.push(data.city)
            }
            if(data.zip){
                sql+=(" `Zip` = ?,")
                options.push(data.zip)
            }
            if(data.state){
                sql+=(" `State` = ?,")
                options.push(data.state)
            }
            if(data.titled){
                sql+=(" `Titled` = ?,")
                options.push(data.titled)
            }
            if(data.vendorID){
              sql+=(" `Vendor ID` = ?,")
              options.push(data.vendorID)
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.equipment`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
