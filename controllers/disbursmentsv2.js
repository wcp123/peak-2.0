var db = require("../dbOptions");
var msg = require("../httpMsgs");
var settings = require('../settings')
getApplication = (appID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.applications WHERE \`ID\` = ?`,[appID], function (data, err){
        
            if(err) return reject(err)

            return resolve(data[0])
        })
    })
}
getApplicant = (companyID, docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.applicants WHERE ID = ?`,[companyID], function(data, err){
            if(err) return reject(err)
            getApplicantBilling(docID).then(billing=>{
                data[0]['Billing']=billing?billing:{new:true}
                return resolve(data[0])
            })
        })
    })
}
getApplicantBilling = (docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.docapplicantbilling WHERE \`Doc ID\` = ?`,[docID], (data,err)=>{
            if(err) return reject(err)
            var joinData = data[0]
            console.log(joinData)
            if(data.length>0){
                db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`applicant billing\` WHERE ID = ?`,[joinData['Applicant Billing ID']], (data1, err1)=>{
                    if(err1) return reject(err)
                    console.log(data1[0])
                    if(data1[0]) return resolve(data1[0])
                    return resolve({new:true})
                })
            }
            return resolve({new:true})
            
        })
    })
}
getBoardingSheet = (docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`Select * from ${settings.sqlConfig.database}.\`boarding sheet\` Where \`Doc ID\` = ?`,[docID], (data, err)=>{
            if(err) return reject(err)

            return resolve(data[0])
        })
    })
}
getCredit = (appID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.credit WHERE \`App ID\` = ?`,[appID], (data, err)=>{
            if(err) return reject(err)

            return resolve(data[0])
        })
    })
}
getCgs = (appID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`application to cg\` WHERE \`Application ID\` = ?`,[appID], (data, err)=>{
            if(err) return reject(err)
            getPromiseAllHelper(data, getCgJoinHelper).then(list=>{
                return resolve(list)
            })
        })
    })
}
getCgJoinHelper = (item) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`corporate gaurantors\` WHERE ID = ?`,[item['CG ID']], (data,err)=>{
            if(err) return reject(err)
            return resolve(data[0])
        })
    })
}
getEquipment = async (item) =>{
    return new Promise((resolve, reject)=>{
        
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.equipment WHERE ID = ?`,[item['Equipment ID']], function(data, err){
            if(err) reject(err)

            return resolve(data[0])
        })
    })
}
getEquipments = async (appID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`application to equipment\` WHERE \`App ID\` = ?`,[appID], (data, err)=>{
            if(err) reject(err)
            getPromiseAllHelper(data, getEquipment).then(list=>{
                var obj = {a2e: data, equipmentInfo: list}
                return resolve(obj)
            })
        })
    })
}
getDisbursements = (docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`disbursements\` WHERE \`Doc ID\` = ?`,[docID], function (data, err){
            if(err) return reject(err)
            return resolve(data[0])
        })
    })
}

getDocumentation = (contractNumber) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`documentation\` WHERE \`Contract Number\` = ?`,[contractNumber], function (data, err){
            if(err){
                return reject(err)
            }
            else{
                if(data.length<1){
                    return reject(err)
                }
                return resolve(data[0])
            }
        })
    })
}
getFollowUpItems = (docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.followup WHERE \`Doc ID\` = ?`,[docID], (data,err)=>{
            if(err) return reject(err)
            if(data.length>0)return resolve(data)
            return resolve([{new:true}])
        })
    })
}
getPgs = (appID) =>{
    return new Promise((resolve, reject)=>{
        var pgs = []
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`application to pg\` WHERE \`App ID\` = ?`,[appID], (data, err)=>{
            if(err) return reject(err)
            getPromiseAllHelper(data, getPgJoinHelper).then(list=>{
                return resolve(list)
            })
        })
    })
}
getPgJoinHelper = (item) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`personal gaurantors\` WHERE ID = ?`,[item['PG ID']], (data,err)=>{
            if(err) return reject(err)
            return resolve(data[0])
        })
    })
}
getSource = (sourceID, docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.sources WHERE ID = ?`,[sourceID], (data,err)=>{
            if(err) return reject(err)
            if(data.length<1) reject(new Error('Nothing returned'))
            db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source contacts\` WHERE ID = ?`,[sourceID],(data1,err1)=>{
                if(err1) return reject(err1)
                data[0]['Contact'] = data1[0]
                getSourceBilling(docID).then(billing=>{
                    console.log(billing)
                    data[0]['Billing'] = billing?billing:{new:true}
                    return resolve(data[0])
    
                })
            })
            
        })
    })
}
getSourceBilling =(docID)=>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.docsourcebilling WHERE \`Doc ID\` = ${docID}`,[docID], (data, err)=>{
            if(err) return reject(err)
            console.log(data)
            if(data.length>0){
                db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source billing\` WHERE ID =?`,[data[0]['Source Billing ID']], (data1, err)=>{
                    if(err) return reject(err)
                    return resolve(data1[0])
                })
            }
            return resolve({new:true})
        })
    })
}
getStatus = (docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.status WHERE \`Doc ID\` = ?`,[docID], (data,err)=>{
            if(err) return reject(err)
            return resolve(data[0])
        })
    })
}

getVendor = async (item) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.vendors WHERE ID = ?`,[item['Vendor ID']], function(data, err){
            if(err) return reject(err)
            var obj = data[0]
            
            return resolve(obj)
        })
    })
}
getVendorBilling = async (item, docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.docvendorbilling WHERE \`Doc ID\` = ?`,[docID],(data, err)=>{
            if(err) reject(error)
            if(data.length>0){
                data.map(docToBilling=>{
                    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`vendor billing\` WHERE \`Vendor ID\` = ? AND \`ID\` = ?`,[item['ID'], docToBilling['ID']], (data1, err1)=>{
                        if(err1) return reject(err1)
                        if(data1.length>=1){
                            item['Billing'] = data1[0]
                            return resolve(item)
                        }
                        item['Billing'] = {}
                        return resolve(item)
                    })
                })
            }
            item['Billing'] ={}
            return resolve(item)
            
        })
    })
}
getVendorDisAdd = async (item, docID) =>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`disbursement additional\` WHERE \`Vendor ID\` = ? AND \`Doc ID\` = ?`,[item['ID'],docID], (data, err1)=>{
            if(err1) return reject(err1)
            if(data.length>0){
                item['Dis Add'] = data[0]
                return resolve(item)
            }
            item['Dis Add'] = {}
            return resolve(item)
        })
    })
    
}
getPromiseAllHelper = async (list, methodToUse, ...args) =>{
    return Promise.all(list.map((item)=>methodToUse(item, ...args)))
}
getVendorsOnDeal = async (appID, docID) =>{
    return new Promise((resolve, reject)=>{
        return getVendorJoinHelper(appID, docID).then(data=>resolve(data)).catch(err=>reject(err))
    })
}
getVendorJoinHelper = async(appID, docID)=>{
    return new Promise((resolve, reject)=>{
        db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`application to vendor\` WHERE \`Application ID\` = ?`,[appID], (data,err)=>{
            if(err) return reject(err)
            
            getPromiseAllHelper(data, getVendor).then(list=>{
                var obj = {}
                getPromiseAllHelper(list, getVendorBilling, docID).then(vendors=>{
                    getPromiseAllHelper(vendors, getVendorDisAdd, docID).then(finalVendor=>{
                        obj['vendors'] = finalVendor
                        obj['a2v'] = data
                        return resolve(obj)
                    }).catch(err=>reject(err))
                }).catch(err=>reject(err))
                
            }).catch(err=>reject(err))
            
        })
    })

}
combineAndCatch = (contractNumber) =>{
    return new Promise((resolve, reject)=>{
        getDocumentation(contractNumber).then(returnState=>{
            console.log(returnState)
            getApplication(returnState['App ID']).then(application=>{
                returnState['application']=application
                getApplicant(returnState['application']['Company ID'],returnState['ID']).then(company=>{
                    returnState['companyInfo']=company
                    getBoardingSheet(returnState['ID']).then(boardingSheet=>{
                        returnState['boardingsheet']=boardingSheet
                        getCgs(returnState['App ID']).then(cgs=>{
                            returnState['cgList'] = cgs
                            getCredit(returnState['App ID']).then(credit=>{
                                returnState['credit']=credit
                                getDisbursements(returnState['ID']).then(disbursement=>{
                                    if(disbursement){
                                        returnState['disbursements'] = disbursement
                                        
                                    }
                                    else{
                                        returnState['disbursements'] = {new:true}
                                    }
                                    getEquipments(returnState['App ID']).then(({a2e, equipmentInfo})=>{
                                        returnState['A2E'] = a2e
                                        returnState['equipmentInfo']=equipmentInfo
                                        getFollowUpItems(returnState['ID']).then(fitems=>{
                                            returnState['fitems'] = fitems
                                            getPgs(returnState['App ID']).then(pgs=>{
                                                returnState['pgList'] = pgs
                                                getSource(returnState['application']['Source ID'],returnState['ID']).then(source=>{
                                                    returnState['source'] = source
                                                    getStatus(returnState['ID']).then(status=>{
                                                        returnState['status'] = status
                                                        getVendorsOnDeal(returnState['App ID'], returnState['ID']).then(({a2v, vendors})=>{
                                                            returnState['A2V'] = a2v
                                                            returnState['vendors'] = vendors
                                                            // console.log(returnState)
                                                            return resolve(returnState)
                                                        }).catch(err=>reject(err))
                                                    }).catch(err=>reject(err))
                                                }).catch(err=>reject(err))
                                            }).catch(err=>reject(err))
                                        }).catch(err=>reject(err))
                                    }).catch(err=>reject(err))
                                }).catch(err=>reject(err))
                            }).catch(err=>reject(err))
                        }).catch(err=>reject(err))
                    }).catch(err=>reject(err))
                }).catch(err=>reject(err))
            }).catch(err=>reject(err))   
        })
        .catch(err=>reject(err))
    })
    
}

exports.getAll = function (req, resp, contractNumber){
    combineAndCatch(contractNumber).then(returnState=>msg.sendJSON(req, resp, returnState)).catch(err=>{
        console.error(err)
        msg.show500(req, resp, err)
    })
};
