var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`docsourcebilling\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`Select * from ${settings.sqlConfig.database}.\`docsourcebilling\` Where \`Doc ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`docsourcebilling\` `;
            sql += "(`Doc ID`, `Source Billing ID`) "+"VALUES ";
            sql+= "(?, ?)"
                    
            db.executeSql(sql,[data.docID, data.sourceBillingID],function (body, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, body.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);
    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`docsourcebilling\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.docID){
                sql+= (" `Doc ID` = ?,"); //only strings need the single quote
                options.push(data.docID)
            }
            if (data.sourceBillingID||data.sourceBillingID==null){
                sql+= (" `Source Billing ID` = ?,"); //only strings need the single quote
                options.push(data.sourceBillingID)
            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ${data.id}`;
            //
            db.executeSql(sql,function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`docsourcebilling\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.ID],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
