var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`documentation\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};
exports.getByApp = function(req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`documentation\` WHERE \`App ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`documentation\` WHERE ID = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getContract = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`documentation\` WHERE \`Contract Number\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.getFilteredContracts = function(req, resp){
    var date = new Date();
    var year = date.getFullYear();
    year = year%100
    var month = date.getMonth()+1;
    month = Math.floor(month/10)===1?String(month):('0'+String(month))
    var number = String(year)+ month + '00'
    console.log(number)
    db.executeSql("SELECT \`Contract Number\` FROM ${settings.sqlConfig.database}.documentation WHERE \`Contract Number\` > "+number,[], function(data, err){
        if(err){
            msg.show500(req, resp, err)

        }
        else{
            msg.sendJSON(req,resp,data)
        }
    })

}
exports.generateContractNumber = (req, res)=>{
    var date = new Date();
    var year = date.getFullYear();
    year = year%100
    var month = date.getMonth()+1;
    month = Math.floor(month/10)===1?String(month):('0'+String(month))
    var number = String(year)+ month + '00'
    console.log(number)
    db.executeSql("SELECT \`Contract Number\` FROM ${settings.sqlConfig.database}.documentation WHERE \`Contract Number\` > "+number,[], function(data, err){
        if(err){
            msg.show500(req, res, err)

        }
        else{
            data.sort((a,b)=>(a['Contract Number']>b['Contract Number'])?1:-1)
            if(data.length>=1){
                msg.sendJSON(req,res,(data[data.length-1]['Contract Number']+1))
            }
            else{
                msg.sendJSON(req,res,(parseInt(number)+1))
            }
        }
    })
}
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.documentation set \`lock\` = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.documentation set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        console.log(data)
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`documentation\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.appID){
                sql+="`App ID`, "
                sqlEnd+=`?, `
                options.push(data.appID)
            }
            if(data.accountingEmail){
                sql+="`Accounting Email`, "
                sqlEnd+=`?, `
                options.push(data.accountingEmail)
            }
            if(data.advancePayment){
                sql+="`Advance Payment`, "
                sqlEnd+=`?, `
                options.push(data.advancePayment)
            }
            if(data.assetCode){
                sql+="`Asset Code`, "
                sqlEnd+=`?, `
                options.push(data.assetCode)
            }
            if(data.billingAddress){
                sql+="`Billing Address`, "
                sqlEnd+=`?, `
                options.push(data.billingAddress)
            }
            if(data.billingCity){
                sql+="`Billing City`, "
                sqlEnd+=`?, `
                options.push(data.billingCity)
            }
            if(data.billingGroup){
                sql+="`Billing Group`, "
                sqlEnd+=`?, `
                options.push(data.billingGroup)
            }
            if(data.billingState){
                sql+="`Billing State`, "
                sqlEnd+=`?, `
                options.push(data.billingState)
            }
            if(data.billingZip){
                sql+="`Billing Zip`, "
                sqlEnd+=`?, `
                options.push(data.billingZip)
            }
            if(data.brokerCommission){
                sql+="`Broker Commission`, "
                sqlEnd+=`?, `
                options.push(data.brokerCommission)
            }
            if(data.coLessee){
                sql+="`CoLessee`, "
                sqlEnd+=`?, `
                options.push(data.coLessee)
            }
            if(data.contractType){
                sql+="`Contract Type`, "
                sqlEnd+=`?, `
                options.push(data.contractType)
            }
            if(data.contactName){
                sql+="`UCC Fixture Filing`, "
                sqlEnd+=`?, `
                options.push(data.contractNumber)
            }
            if(data.contractNumber){
                sql+="`Contract Number`, "
                sqlEnd+=`?, `
                options.push(data.contactName)
            }
            if(data.coSignerName){
                sql+="`Co Signer Name`, "
                sqlEnd+=`?, `
                options.push(data.coSignerName)
            }
            if(data.coSignerTitle){
                sql+="`Co Signer Title`, "
                sqlEnd+=`?, `
                options.push(data.coSignerTitle)
            }
            if(data.courtesyInvoiceEmail){
                sql+="`Courtesy Invoice Email`, "
                sqlEnd+=`?, `
                options.push(data.courtesyInvoiceEmail)
            }if(data.dedicatedRate){
                sql+="`Dedicated Rate`, "
                sqlEnd+=`?, `
                options.push(data.dedicatedRate)
            }
            if(data.docDate){
                var docDate = String(data.docDate).indexOf('T')===-1?data.docDate:data.docDate.split('T')[0]
                sql+="`Doc Date`, "
                sqlEnd+=`?, `
                options.push(docDate)
            }
            if(data.docFeeFinanced){
                sql+="`Doc Fee Financed`, "
                sqlEnd+=`?, `
                options.push(data.docFeeFinanced)
            }if(data.docFeeInvoice){
                sql+="`Doc Fee Invoice`, "
                sqlEnd+=`?, `
                options.push(data.docFeeInvoice)
            }
            if(data.docPerson){
                sql+="`Doc Person`, "
                sqlEnd+=`?, `
                options.push(data.docPerson)
            }
            if(data.documentEmail){
                sql+="`Document Email`, "
                sqlEnd+=`?, `
                options.push(data.documentEmail)
            }
            if(data.downPayment){
                sql+="`Down Payment`, "
                sqlEnd+=`?, `
                options.push(data.downPayment)
            }
            if(data.exempt){
                sql+="`Exempt`, "
                sqlEnd+=`?, `
                options.push(data.exempt)
            }
            if(data.exemptCertificate){
                sql+="`Exempt Certificate`, "
                sqlEnd+=`?, `
                options.push(data.exemptCertificate)
            }if(data.firstPayment){
                sql+="`First Payment`, "
                sqlEnd+=`?, `
                options.push(data.firstPayment)
            }
            if(data.lastPayment){
                sql+="`Last Payment`, "
                sqlEnd+=`?, `
                options.push(data.lastPayment)
            }
            if(data.loanAmount){
                sql+="`Loan Amount`, "
                sqlEnd+=`?, `
                options.push(data.loanAmount)
            }
            if(data.onStream){
                sql+="`On Stream`, "
                sqlEnd+=`?, `
                options.push(data.onStream)
            }if(data.paymentAmount){
                sql+="`Payment Amount`, "
                sqlEnd+=`?, `
                options.push(data.paymentAmount)
            }
            if(data.phoneNumber){
                sql+="`Phone Number`, "
                sqlEnd+=`?, `
                options.push(data.phoneNumber)
            }
            if(data.salesPrice){
                sql+="`Sales Price`, "
                sqlEnd+=`?, `
                options.push(data.salesPrice)
            }
            if(data.salesPrice){
                sql+="`Sales Price`, "
                sqlEnd+=`?, `
                options.push(data.salesPrice)
            }
            if(data.signorEmail){
                sql+="`Signer Email`, "
                sqlEnd+=`?, `
                options.push(data.signorEmail)
            }
            if(data.signorName){
                sql+="`Signer Name`, "
                sqlEnd+=`?, `
                options.push(data.signorName)
            }
            if(data.signorTitle){
                sql+="`Signer Title`, "
                sqlEnd+=`?, `
                options.push(data.signorTitle)
            }
            if(data.tax){
                sql+="`Tax`, "
                sqlEnd+=`?, `
                options.push(data.tax)
            }
            if(data.term){
                sql+="`Term`, "
                sqlEnd+=`?, `
                options.push(data.term)
            }
            if(data.titleWorker){
                sql+="`Title Worker`, "
                sqlEnd+=`?, `
                options.push(data.titleWorker)
            }
            if(data.totalInvoicedAmount){
                sql+="`Total Invoiced Amount`, "
                sqlEnd+=`?, `
                options.push(data.totalInvoicedAmount)
            }
            if(data.remainingPayments){
                sql+="`Remaining Payments`, "
                sqlEnd+=`?, `
                options.push(data.remainingPayments)
            }
            if(data.upfront){
                sql+="`Upfront`, "
                sqlEnd+=`?, `
                options.push(data.upfront)
            }
            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd

            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        console.log(ex)
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{

        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`documentation\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            // if (data.appID){
            //     sql+= (" `App ID` = " + data.appID+ ","); //only strings need the double quote

            // }
            if (data.accountingEmail){
                sql+= (" `Accounting Email` = ?,"); //only strings need the double quote
                options.push(data.accountingEmail)
            }if (data.advancePayment){
                sql+= (" `Advance Payment` = ?,"); //only strings need the double quote
                options.push(data.advancePayment)

            }if (data.assetCode){
                sql+= (" `Asset Code` = ?,"); //only strings need the double quote
                options.push(data.assetCode)

            }if (data.billingAddress){
                sql+= (" `Billing Address` = ?,"); //only strings need the double quote
                options.push(data.billingAddress)

            }if (data.billingGroup){
                sql+= (" `Billing Group` = ?,"); //only strings need the double quote
                options.push(data.billingGroup)

            }if (data.billingState){
                sql+= (" `Billing State` = ?,"); //only strings need the double quote
                options.push(data.billingState)

            }if (data.billingZip){
                sql+= (" `Billing Zip` = ?,"); //only strings need the double quote
                options.push(data.billingZip)

            }if(data.brokerCommission){
                sql+=" \`Broker Commission\` = ?,"
                options.push(data.brokerCommission)
            }if (data.coLessee){
                sql+= (" \`CoLessee\` = ?,"); //only strings need the double quote
                options.push(data.coLessee)

            }if (data.contractType){
                sql+= (" `Contract Type` = ?,"); //only strings need the double quote
                options.push(data.contractType)

            }if (data.contactName){
                sql+= (" `Contact Name` = ?,"); //only strings need the double quote
                options.push(data.contractNumber)

            }if (data.coSignerName){
                sql+= (" `Co Signer Name` = ?,"); //only strings need the double quote
                options.push(data.coSignerName)

            }if (data.coSignerTitle){
                sql+= (" `Co Signer Title` = ?,"); //only strings need the double quote
                options.push(data.coSignerTitle)

            }if (data.courtesyInvoiceEmail){
                sql+= (" `Courtesy Invoice Email` = ?,"); //only strings need the double quote
                options.push(data.courtesyInvoiceEmail)

            }if (data.dedicatedRate){
                sql+= (" `Dedicated Rate` = ?,"); //only strings need the double quote
                options.push(data.dedicatedRate)

            }if (data.docDate){
                var docDate = data.docDate.indexOf('T')===-1?data.docDate:data.docDate.split('T')[0]
                sql+= (" `Doc Date` = ?,"); //only strings need the double quote
                options.push(docDate)

            }if(data.docFeeFinanced){
                sql+=" `Doc Fee Financed` = \"?\","
                options.push(data.docFeeFinanced)
            }
            if (data.docFeeInvoice){
                sql+= (" `Doc Fee Invoice` = ?,"); //only strings need the double quote
                options.push(data.docFeeInvoice)

            }if (data.docPerson){
                sql+= (" `Doc Person` = ?,"); //only strings need the double quote
                options.push(data.docPerson)

            }if (data.documentEmail){
                sql+= (" `Document Email` = ?,"); //only strings need the double quote
                options.push(data.documentEmail)

            }if (data.downPayment){
                sql+= (" `Down Payment` = ?,"); //only strings need the double quote
                options.push(data.downPayment)

            }if (data.exempt){
                sql+= (" `Exempt` = ?,"); //only strings need the double quote
                options.push(data.exempt)

            }if (data.exemptCertificate){
                sql+= (" `Exempt Certificate` = ?,"); //only strings need the double quote
                options.push(data.exemptCertificate)

            }if (data.firstPayment){
                sql+= (" `First Payment` = ?,"); //only strings need the double quote
                options.push(data.firstPayment)

            }if(data.lastPayment){
                sql+="`Last Payment` = ?,"
                options.push(data.lastPayment)
            }if(data.loanAmount){
                sql+="`Loan Amount` = ?,"
                options.push(data.loanAmount)
            }if (data.onStream){
                sql+= (" `On Stream` = ?,"); //only strings need the double quote
                options.push(data.onStream)

            }if (data.paymentAmount){
                sql+= (" `Payment Amount` = ?,"); //only strings need the double quote
                options.push(data.paymentAmount)

            }if (data.phoneNumber){
                sql+= (" `Phone Number` = ?,"); //only strings need the double quote
                options.push(data.phoneNumber)

            }if (data.salesPrice){
                sql+= (" `Sales Price` = ?,"); //only strings need the double quote
                options.push(data.salesPrice)

            }if (data.signorEmail){
                sql+= (" `Signer Email` = ?,"); //only strings need the double quote
                options.push(data.signorEmail)

            }if (data.signorName){
                sql+= (" `Signer Name` = ?,"); //only strings need the double quote
                options.push(data.signorName)

            }if (data.signorTitle){
                sql+= (" `Signer Title` = ?,"); //only strings need the double quote
                options.push(data.signorTitle)

            }if (data.tax){
                sql+= (" `Tax` = ?,"); //only strings need the double quote
                options.push(data.tax)

            }if (data.term){
                sql+= (" `Term` = ?,"); //only strings need the double quote
                options.push(data.term)

            }if (data.titleWorker){
                sql+= (" `Title Worker` = ?,"); //only strings need the double quote
                options.push(data.titleWorker)

            }if (data.totalInvoicedAmount){
                sql+= (" `Total Invoiced Amount` = ?,"); //only strings need the double quote
                options.push(data.totalInvoicedAmount)

            }if (data.remainingPayments){
                sql+= (" `Remaining Payments` = ?,"); //only strings need the double quote
                options.push(data.remainingPayments)

            }if (data.upfront){
                sql+= (" `Upfront` = ?,"); //only strings need the double quote
                options.push(data.upfront)

            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`documentation\``;
            sql = sql + ` WHERE ID = ?`;
            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
