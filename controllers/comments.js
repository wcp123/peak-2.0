var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.comments`,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};
exports.getCredit = function(req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.comments where \`Credit ID\` = ?`,[id], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
}
exports.getApp = function(req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.comments where \`App ID\` = ?`,[id], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
}
exports.getCompany = function(req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.comments where \`Company ID\` = ?`,[id], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
}
//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.comments WHERE ID = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.comments set 'lock' = 1 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.comments set lock = 0 WHERE ID = ? `,[id], (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            if(data.creditID){
                // sql += "(\`Credit ID\`, \`Internal Comment\`, Type, Comment) "+"VALUES ";
                // sql+= util.format("(%d,%d,'%s','%s')",
                //                 data.creditID, data.internalComment, data.type, data.comment);


                var sql = `INSERT INTO ${settings.sqlConfig.database}.comments (`
                var sqlEnd = ") VALUES (";
                var options = []
                if(data.creditID){
                    sql+="`Credit ID`, "
                    options.push(data.creditID)
                    sqlEnd+=`?, `
                }
                if(data.internalComment){
                    sql+="`Internal Comment`, "
                    options.push(data.internalComment)
                    sqlEnd+=`?, `
                }
                if(data.type){
                    sql+="`Type`, "
                    options.push(data.type)
                    sqlEnd+=`?, `
                }
                if(data.comment){
                  sql+="`Comment`, "
                  options.push(data.comment)
                  sqlEnd+=`?, `
                }
                sql = sql.slice(0, -2);
                sqlEnd = sqlEnd.slice(0, -2)
                sqlEnd += ")"
                sql += sqlEnd
                db.executeSql(sql,options, function (x, err){
                    if(err){
                        msg.show500(req, resp, err);
                    }
                    else{
                        msg.sendJSON(req,resp, x.insertId)
                    }
                });


            }
            else if(data.appID){
                var sql = `INSERT INTO ${settings.sqlConfig.database}.comments (`
                var sqlEnd = ") VALUES (";
                if(data.appID){
                    sql+="`App ID`, "
                    options.push(data.appID)
                    sqlEnd+=`?, `
                }
                if(data.internalComment){
                    sql+="`Internal Comment`, "
                    options.push(data.internalComment)
                    sqlEnd+=`?, `
                }
                if(data.type){
                    sql+="`Type`, "
                    options.push(data.type)
                    sqlEnd+=`?, `
                }
                if(data.comment){
                    sql+='`Comment`, '
                    options.push(data.comment)
                    sqlEnd+=`?, `
                }

                sql = sql.slice(0, -2);
                sqlEnd = sqlEnd.slice(0, -2)
                sqlEnd += ")"
                sql += sqlEnd
                db.executeSql(sql,options, function (x, err){
                    if(err){
                        msg.show500(req, resp, err);
                    }
                    else{
                        msg.sendJSON(req,resp, x.insertId)
                    }
                });
            }else if(data.companyID){
                var sql = `INSERT INTO ${settings.sqlConfig.database}.comments (`
                var sqlEnd = ") VALUES (";
                if(data.companyID){
                    sql+="`Company ID`, "
                    options.push(data.companyID)
                    sqlEnd+=`?, `
                }
                if(data.internalComment){
                    sql+="`Internal Comment`, "
                    options.push(data.internalComment)
                    sqlEnd+=`?, `
                }
                if(data.type){
                    sql+="`Type`, "
                    options.push(data.type)
                    sqlEnd+=`?, `
                }
                if(data.comment){
                    sql+='`Comment`, '
                    options.push(data.comment)
                    sqlEnd+=`?, `
                }

                sql = sql.slice(0, -2);
                sqlEnd = sqlEnd.slice(0, -2)
                sqlEnd += ")"
                sql += sqlEnd
                db.executeSql(sql,options, function (x, err){
                    if(err){
                        msg.show500(req, resp, err);
                    }
                    else{
                        msg.sendJSON(req,resp, x.insertId)
                    }
                });
            }
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.comments SET `;
            var options =[]
            //repeat the following line for each column in the table;

            if (data.CreditID){
                options.push(data.CreditID)
                sql+= (" 'Credit ID' = ?,"); //only strings need the single quote

            }if (data.appID){
                options.push(data.appID)
                sql+= (" 'App ID' = ?,"); //only strings need the single quote

            }if (data.companyID){
                options.push(data.companyID)
                sql+= (" 'Company ID' = ?,"); //only strings need the single quote

            }
            if (data.internalComment){
                options.push(data.internalComment)
                sql+= (" 'Internal Comment' = ?,"); //only strings need the single quote

            }if (data.type){
                options.push(data.type)
                sql+= (" Type = \"?\","); //only strings need the single quote

            }if (data.comment){
                options.push(data.comment)
                sql+= (" Comment = \"?\","); //only strings need the single quote

            }

            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ${data.id}`;
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.comments`;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id], function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
