var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
const setttings = require('../settings')

exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${setttings.sqlConfig.database}.\`boarding sheet\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`Select * from ${setttings.sqlConfig.database}.\`boarding sheet\` Where \`Doc ID\` = ?`,[id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            var sql = `INSERT INTO ${setttings.sqlConfig.database}.\`boarding sheet\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.docID){
                sql+="`Doc ID`, "
                sqlEnd+=`?, `
                options.push(data.docID)
            }
            if(data.advanced){
                sql+="`Advanced`, "
                sqlEnd+=`?, `
                options.push(data.advanced)
            }
            if(data.basicRentalPayment){
                sql+="`Basic Rental Payment`, "
                sqlEnd+=`?, `
                options.push(data.basicRentalPayment)
            }
            if(data.customerRate){
                sql+="`Customer Rate`, "
                sqlEnd+=`?, `
                options.push(data.customerRate)
            }
            if(data.dedicatedRate){
                sql+="`Dedicated Rate`, "
                sqlEnd+=`?, `
                options.push(data.dedicatedRate)
            }
            if(data.effectDate){
                sql+="`Effect Date`, "
                sqlEnd+=`?, `
                options.push(data.effectDate)
            }
            if(data.equipmentDescription){
                var clean = (string)=>{
                    return String(string).replace('/','//')
                }
                sql+="`Equipment Description`, "
                sqlEnd+=`?, `
                options.push(clean(data.equipmentDescription))
            }
            if(data.expireDate){
                sql+="`Expire Date`, "
                sqlEnd+=`?, `
                options.push(data.expireDate)
            }
            if(data.firstPaymentDate){
                sql+="`First Payment Date`, "
                sqlEnd+=`?, `
                options.push(data.firstPaymentDate)
            }
            if(data.initialTerm){
                sql+="`Initial Term`, "
                sqlEnd+=`?, `
                options.push(data.initialTerm)
            }
            if(data.loanAmount){
                sql+="`Loan Amount`, "
                sqlEnd+=`?, `
                options.push(data.loanAmount)
            }
            if(data.loanStartDate){
                sql+="`Loan Start Date`, "
                sqlEnd+=`?, `
                options.push(data.loanStartDate)
            }
            if(data.payment){
                sql+="`Payment`, "
                sqlEnd+=`?, `
                options.push(data.payment)
            }
            if(data.paymentFrequency){
                sql+="`Payment Frequency`, "
                sqlEnd+=`?, `
                options.push(data.paymentFrequency)
            }
            if(data.remainingTerm){
                sql+="`Remaining Term`, "
                sqlEnd+=`?, `
                options.push(data.remainingTerm)
            }
            if(data.status){
                sql+="`Status`, "
                sqlEnd+=`?, `
                options.push(data.status)
            }
            if(data.statusDate){
                sql+="`Status Date`, "
                sqlEnd+=`?, `
                options.push(data.statusDate)
            }
            if(data.tbasRate){
                sql+="`TBAS Rate`, "
                sqlEnd+=`?, `
                options.push(data.tbasRate)
            }


            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            console.log(data)
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${setttings.sqlConfig.database}.\`boarding sheet\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.docID){
                sql+= (" `Doc ID` = ?,"); //only strings need the single quote
                options.push(data.docID)

            }
            if (data.advanced ){
                sql+= (" `Advanced` = ?,"); //only strings need the single quote
                options.push(data.advanced)

            }if (data.basicRentalPayment){
                sql+=(" `Basic Rental Payment` = ?,")
                options.push(data.basicRentalPayment)
            }if (data.customerRate){
                sql+=(" `Customer Rate` = ?,")
                options.push(data.customerRate)
            }if (data.dedicatedRate){
                sql+=(" `Dedicated Rate` = ?,")
                options.push(data.dedicatedRate)
            }if(data.effectDate){
                sql+=(" `Effect Date` = ?,")
                options.push(data.effectDate)
            }
            if(data.equipmentDescription){
                sql+=(" `Equipment Description` = ?,")
                options.push(data.equipmentDescription)
            }
            if(data.expireDate){
                sql+=(" `Expire Date` = ?,")
                options.push(data.expireDate)
            }
            if(data.firstPaymentDate){
                sql+=(" `First Payment Date` = ?,")
                options.push(data.firstPaymentDate)
            }
            if(data.initialTerm){
                sql+=(" `Initial Term` = ?,")
                options.push(data.initialTerm)
            }
            if(data.loanAmount){
                sql+=(" `Loan Amount` = ?,")
                options.push(data.loanAmount)
            }
            if(data.loanStartDate){
                sql+=(" `Loan Start Date` = ?,")
                options.push(data.loanStartDate)
            }
            if(data.payment){
                sql+=(" `Payment` = ?,")
                options.push(data.payment)
            }
            if(data.paymentFrequency){
                sql+=(" `Payment Frequency` = ?,")
                options.push(data.paymentFrequency)
            }
            if(data.remainingTerm){
                sql+=(" `Remaining Term` = ?,")
                options.push(data.remainingTerm)
            }
            if(data.status){
                sql+=(" `Status` = ?,")
                options.push(data.status)
            }
            if(data.statusDate){
                sql+=(" `Status Date` = ?,")
                options.push(data.statusDate)
            }
            if(data.tbasRate){
                sql+=(" `TBAS Rate` = ?,")
                options.push(data.tbasRate)
            }
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ${data.id}`;
            //
            db.executeSql(sql,options, function (x, err){
                if(err){
                    console.log(err)
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            var isDataProvided = false;
            var sql = `DELETE FROM ${setttings.sqlConfig.database}.\`boarding sheet\``;

            sql = sql + ` WHERE ID = ?`;

            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
