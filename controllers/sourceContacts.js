var db = require("../dbOptions");
var msg = require("../httpMsgs");
var util = require("util");
var settings = require('../settings')
exports.getList = function(req, resp){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source contacts\``,[], function (data, err) {
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }

    });
};

//this function needs put in the docs.js file in the controllers module
exports.get = function (req, resp, id){
    db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.\`source contacts\` WHERE \`Source ID\` = ?`, [id], function (data, err){
        if(err){
            msg.show500(req, resp, err);
        }
        else{
            msg.sendJSON(req,resp, data)
        }
    });
};
exports.lock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`source contacts\` set \`lock\` = 1 WHERE ID = ? `,[id],  (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}
exports.unlock = (req, resp, id) =>{
    try{
        db.executeSql(`UPDATE ${settings.sqlConfig.database}.\`source contacts\` set lock = 0 WHERE ID = ? `,[id],  (data, err) =>{
            if (err) msg.show500(req, resp, err);
            else msg.show200(req, resp, data)
        }

        )
    }
    catch (er){
        msg.show500(req, resp, er)
    }
}

//adds to the database, This method should only be used for the [Dedicated Staff] table. But stuct is same for other methods
exports.add = function (req, resp, data){
    try{
        if (data){
            var sql = `INSERT INTO ${settings.sqlConfig.database}.\`source contacts\` (`
            var sqlEnd = ") VALUES (";
            var options = []
            if(data.firstName){
                sql+="`First Name`, "
                sqlEnd+=`?, `
                options.push(data.firstName)
            }
            if(data.lastName){
                sql+="`Last Name`, "
                sqlEnd+=`?, `
                options.push(data.lastName)
            }
            if(data.sourceID){
                sql+="`Source ID`, "
                sqlEnd+=`?, `
                options.push(data.sourceID)
            }
            if(data.extention){
                sql+="`Extention`, "
                sqlEnd+=`?, `
                options.push(data.extention)
            }
            if(data.email){
                sql+="`Email`, "
                sqlEnd+=`?, `
                options.push(data.email)
            }if(data.address){
                sql+="`Address`, "
                sqlEnd+=`?, `
                options.push(data.address)
            }
            if(data.state){
                sql+="`State`, "
                sqlEnd+=`?, `
                options.push(data.state)
            }
            if(data.zip){
                sql+="`Zip`, "
                sqlEnd+=`?, `
                options.push(data.zip)
            }
            if(data.city){
                sql+="`City`, "
                sqlEnd+=`?, `
                options.push(data.city)
            }
            if(data.phoneNumber){
                sql+="`Phone Number`, "
                sqlEnd+=`?, `
                options.push(data.phoneNumber)
            }

            sql = sql.slice(0, -2);
            sqlEnd = sqlEnd.slice(0, -2)
            sqlEnd += ")"
            sql += sqlEnd
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x.insertId)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (er) {
        msg.show500(req, resp, er);
    }
};

exports.update = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `UPDATE ${settings.sqlConfig.database}.\`source contacts\` SET `;
            //repeat the following line for each column in the table;
            var options = []
            if (data.firstName){
                sql+= (" `First Name` = ?,"); //only strings need the single quote
                options.push(data.firstName)

            }if (data.lastName){
                sql+= (" `Last Name` = ?,"); //only strings need the single quote
                options.push(data.lastName)

            }if (data.sourceID){
                sql+= (" `Source ID` = ?,"); //only strings need the single quote
                options.push(data.sourceID)

            }
            if (data.extention){
                sql+= (" `Extention` = '/',"); //only strings need the single quote
                options.push(data.extention)

            }if (data.email){
                sql+= (" `Email` = ?,"); //only strings need the single quote
                options.push(data.email)

            }if (data.address){
                sql+= (" Address = ?,"); //only strings need the single quote
                options.push(data.address)

            }if (data.city){
                sql+= (" City = ?,"); //only strings need the single quote
                options.push(data.city)

            }if (data.state){
                sql+= (" State = ?,"); //only strings need the single quote
                options.push(data.state)

            }if (data.zip){
                sql+= (" Zip = ?,"); //only strings need the single quote
                options.push(data.zip)

            }
            if(data.phoneNumber){
                sql+=(" `Phone Number` = ?,")
                options.push(data.phoneNumber)
            }
            sql = sql.slice(0, -1);
            //for additional fields add a comma at the begining of next sql string
            sql = sql + ` WHERE ID = ?`;
            //\
            options.push(data.id)
            db.executeSql(sql,options, function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
    }
    catch (ex) {
        msg.show500(req, resp, ex);

    }
};
exports.getAdvanced = function(req, resp, reqBody){

};
exports.delete = function (req, resp, data){
    try{
        if (data){
            //this line is needed for other controllers that have id numbers
                //if(!data.Empno) throw new Error("Empno not provided");
            var isDataProvided = false;
            var sql = `DELETE FROM ${settings.sqlConfig.database}.\`source contacts\``;

            sql = sql + ` WHERE ID = ?`;
            
            db.executeSql(sql,[data.id],function (x, err){
                if(err){
                    msg.show500(req, resp, err);
                }
                else{
                    msg.sendJSON(req,resp, x)
                }
            });
        }
        else{
            throw new Error("Input not valid");
        }
}
catch (ex) {
    msg.show500(req, resp, ex);

}
};
