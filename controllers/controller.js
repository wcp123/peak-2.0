var applicants = require("./applicants");
var applications = require("./applications");
var bank = require("./bankInfo");
var brokerCom = require("./brokerCommision");
var comment = require("./comments");
var credit = require('./credit');
var eot = require("./eot");
var equipment = require("./equipment");
var equipmentFunding = require("./equipmentFunding");
var payment = require('./payment');
var pg = require("./pg");
var sources = require("./sources");
var staff = require('./staff');
var vendor = require('./vendor');
const db = require('../db');
var msg = require("../httpMsgs");
var app2equipment = require("./app2equipment");
var util = require("util");



advanced = function(req, resp){
    var reqBody = {};
            req.on("data", function (data){
                reqBody = JSON.parse(data);
                if(reqBody.length > 1e7){
                    msg.show413(req, resp);
                }
            });
            req.on("end", function() {
                db.executeSql(reqBody.sql, function(data, err){
                    if(err){
                        msg.show500(req, resp, err);
                    }
                    else{
                        msg.sendJSON(req,resp, data)
                    }
                });                
            });
}


exports.switch= function(req, resp){
    switch (req.method){
        case "GET":
            if (req.url === '/'){
                msg.showHome(req, resp);
            }
            else if(req.url === "/applicants"){
                applicants.getList(req, resp);
            }
            else if (req.url === "/applicants/name"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    applicants.getID(req, resp, reqBody);
                });
            }  
            else if(req.url === "/applications"){
                applications.getList(req, resp);
            }
            else if(req.url === "/bank"){
                bank.getList(req, resp);
            }
            else if(req.url === "/brokercommisions"){
                brokerCom.getList(req, resp);
            }
            else if (req.url === "/comments"){
                comment.getList(req, resp);
            }
            else if(req.url === "/credit"){
                credit.getList(req, resp);
            }
            else if(req.url === "/eot"){
                eot.getList(req, resp);
            }
            else if(req.url === "/equipment"){
                equipment.getList(req, resp);
            }
            else if(req.url === "/equipmentFunding"){
                equipmentFunding.getList(req, resp);
            }
            else if(req.url === "/payment"){
                payment.getList(req, resp);
            }
            else if(req.url === "/pg"){
                pg.getList(req, resp)
            }
            else if (req.url === "/staff"){
                staff.getList(req, resp);
            }
            else if(req.url === "/sources"){
                sources.getList(req, resp);
            }
            else if(req.url === "/vendors"){
                vendor.getList(req, resp);
            }
            else if(req.url === "/app2equipment"){
                app2equipment.getList(req, resp);
            }
            

            //this is the code block to get individual records
            else {
                var url = req.url
                var docPattern = "[0-9]+";
                var pattern = new RegExp("/payment/" + docPattern);
                if(pattern.test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    payment.get(req,resp,number);
                }
                else if (RegExp("/pg/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    pg.get(req,resp,number);
                }
                // pattern = new RegExp("/contacts/" + docPattern);
                else if (RegExp("/vendors/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    vendor.get(req,resp,number);
                }
                else if (RegExp("/sources/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    sources.get(req,resp,number);
                }
                
                else if (RegExp("/staff/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    staff.get(req,resp,number);
                }
                else if (RegExp("/staff/lock/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    staff.lock(req,resp,number);
                }else if (RegExp("/staff/unlock/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    staff.unlock(req,resp,number);
                }
                else if (RegExp("/equipmentfunding/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    equipmentFunding.get(req,resp,number);
                }
                else if (RegExp("/equipment/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    equipment.get(req,resp,number);
                }
                else if (RegExp("/eot/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    eot.get(req,resp,number);
                }
                else if (RegExp("/credit/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    credit.get(req,resp,number);
                }
                
                else if (RegExp("/comments/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    comment.get(req,resp,number);
                }else if (RegExp("/bank/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    bank.get(req,resp,number);
                }else if (RegExp("/brokercommision/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    brokerCom.get(req,resp,number);
                }
                else if (RegExp("/app2equipment/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    app2equipment.get(req,resp,number);
                }
                else if (RegExp("/applicants/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    applicants.get(req,resp,number);
                }else if (RegExp("/applications/" + docPattern).test(req.url)){
                    pattern = new RegExp(docPattern);
                    var number = pattern.exec(req.url);
                    applications.get(req,resp,number);
                }
                // if(urlSplit[3] === "advanced"){
                //     advanced(req, resp);
                // }
                else {
                    msg.show404(req,resp);
                }
            }
            break;
        case "POST":
            if (req.url === "/vendors/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else vendor.add(req, resp, reqBody);
                    
                });
                req.on("end", function() {
                });
            }
            if (req.url === "/pg/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else pg.add(req, resp, reqBody);
                });
                req.on("end", function() {
                });
            }
            else if(req.url === "/advanced"){
                advanced(req, resp);
            }
            if (req.url === "/staff/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else staff.add(req, resp, reqBody)
                });
                req.on("end", function() {
                    
                });
            }
            if (req.url === "/sources/add"){
                var reqBody = '';
                
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else sources.add(req, resp, reqBody);
                });
                req.on("end", function() {
                    
                });
            }
            if (req.url === "/credit/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    credit.add(req, resp, reqBody);
                });
            }
            if (req.url === "/credit2pg/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else credit2pg.add(req, resp, reqBody);
                });
                req.on("end", function() {
                    
                });
            }
            if (req.url === "/credit2vendor/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    credit2vendor.add(req, resp, reqBody);
                });
            }
            if (req.url === "/comment/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    comment.add(req, resp, reqBody);
                });
            }
            if (req.url === "/eot/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    eot.add(req, resp, reqBody);
                });
            }
            if (req.url === "/equipment/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else equipment.add(req, resp, reqBody);

                });
                req.on("end", function() {
                });
            }
            if (req.url === "/equipmentfunding/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    equipmentFunding.add(req, resp, reqBody);
                });
            }
            if(req.url === "/bankInfo/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else bank.add(req, resp, reqBody);

                });
                req.on("end", function() {
                });
            }
            if(req.url === "/applicants/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else applicants.add(req, resp, reqBody);

                });
                req.on("end", function() {
                });
            }
            if(req.url === "/application/add"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                    else applications.add(req, resp, reqBody);
                });
                req.on("end", function() {
                    
                });
            }
            else{
                msg.show404(req,resp);
            }
            break;
        case "PUT":
            if (req.url === "/staff/update"){
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    staff.update(req, resp, reqBody);
                });
                
            }
            else{
                msg.show404(req,resp);
            }

                break;
        case "DELETE":
            if (req.url === "/staff/delete"){
                var docPattern = "[0-9]+";
                var pattern = new RegExp("/staff/delete/" + docPattern);
                var reqBody = '';
                req.on("data", function (data){
                    reqBody += data;
                    if(reqBody.length > 1e7){
                        msg.show413(req, resp);
                    }
                });
                req.on("end", function() {
                    if(pattern.test(req.url)){
                        pattern = new RegExp(docPattern);
                        var number = pattern.exec(req.url);
                        staff.delete(req,resp,number);
                    }
                });
            }
            else{
                msg.show404(req,resp);
            }
                break;
        default:
            msg.show405(req, resp);
            break; 
}};