const application = require('./controllers/applications')
const app2cg = require('./controllers/app2cg')
const application2Equipment = require('./controllers/app2equipment')
const app2pg = require('./controllers/app2pg')
const app2vendor = require('./controllers/app2vendor')
const applicant = require('./controllers/applicants')
const applicantBilling = require('./controllers/applicantBilling')
const bankInfo = require('./controllers/bankInfo')
const batchApp = require('./controllers/batchApp')
const boardingsheet = require('./controllers/boardingSheet')
const brokerCommision = require('./controllers/brokerCommision')
const comment = require('./controllers/comments')
const commission = require('./controllers/Commissions')
const credit = require('./controllers/credit')
const creditAdditional = require('./controllers/creditAddtional')
const cg = require('./controllers/cg')
const deal = require('./controllers/getDeal')
const dealerKickback = require('./controllers/dealerKickback')
const discounter = require('./controllers/discounter')
const disbursement = require('./controllers/disbursment')
const disbursementAdditional = require('./controllers/disbursementAdditional')
const disbursementAll = require('./controllers/disbursmentsv2')
const templates = require('./Documents/Templates')
const pdf = require('html-pdf')
// const discountersTransactions = require('./controllers/discountersTransactions')
const documentation = require('./controllers/documentation')
const docApplicantBilling = require('./controllers/docApplicantBilling')
const docSourceBilling = require('./controllers/docSourceBilling')
const docVendorBilling = require('./controllers/docVendorBilling')
const eot = require('./controllers/eot')
const equipment = require('./controllers/equipment')
const equipmentFunding = require('./controllers/equipmentFunding')
const fee = require('./controllers/fee')
const followup = require('./controllers/followup')
const payment = require('./controllers/payment')
const prefunding = require('./controllers/prefunding')
const pc = require('./controllers/personalCredit')
const pg = require('./controllers/pg')
const sourceContacts = require('./controllers/sourceContacts')
const sources = require('./controllers/sources')
const sourceCommission = require('./controllers/sourceCommission')
const sourceBilling = require('./controllers/sourceBilling')
const sourceFunding = require('./controllers/sourceFunding')
const sourceTotal = require('./controllers/sourceTotal')
const staff = require('./controllers/staff')
const status = require('./controllers/status')
const vendor = require('./controllers/vendor')
const vendorBilling = require('./controllers/vendorBilling')
const vendorCommission = require('./controllers/vendorCommission')
const vendorTotals = require('./controllers/vendorTotals')
const vendorFunding = require("./controllers/vendorFunding")
const express = require('express');
const app = express.Router();
const fs = require('fs')
const db = require('./dbOptions')
const settings = require('./settings')

const hasRole = (role) => async (req, res, next) => {
    console.log('Authorization request on auth token: ', req.headers.peaktoken)
    console.log('Requested URL: ', req.url)
    const token = req.headers.peaktoken
    if (token) {
        try {
            await db.executeSql(`SELECT * from ${settings.sqlConfig.database}.user WHERE token = ? LIMIT 1`, [token], async function (tokenLookupResult, err) {
                if (tokenLookupResult.length === 0) {
                    return res.status(401).send({ ok: false, error: "You are not authorized" });
                } else {
                    return next();
                }
            })
        }
        catch (error) {
            return res.status(401).send({ ok: false, error: "You are not authorized" })
        }
    } else {
        return res.status(401).send({ ok: false, error: "You are not authorized" })
    }
}

app.get('/generate-application', hasRole('USER'), function (req, res, next) {
    application.generateApplication(req, res);
})
app.get('/application', hasRole('USER'), function (req, res, next) {
    application.getList(req, res);
})
app.get("/applicants", hasRole('USER'), function (req, res, next) {
    applicant.getList(req, res)
})
app.get("/applicantBilling", hasRole('USER'), function (req, res, next) {
    applicantBilling.getList(req, res)
})
app.get('/applicationToCg', hasRole('USER'), function (req, res, next) {
    applicationToCg.getList(req, res)
})
app.get("/applicationToEquipment", hasRole('USER'), function (req, res, next) {
    application2Equipment.getList(req, res)
})
app.get("/applicationToPg", hasRole('USER'), function (req, res, next) {
    app2pg.getList(req, res)
})
app.get('/applicationToVendor', hasRole('USER'), function (req, res, next) {
    app2vendor.getList(req, res)
})
app.get("/bankInfo", hasRole('USER'), function (req, res, next) {
    bankInfo.getList(req, res)
})
app.get('/boardingsheet', hasRole('USER'), function (req, res, next) {
    boardingsheet.getList(req, res)
})
app.get("/comments", hasRole('USER'), function (req, res, next) {
    comment.getList(req, res)
})
app.get("/commission", hasRole('USER'), function (req, res, next) {
    commission.getList(req, res)
})
app.get("/credit", hasRole('USER'), function (req, res, next) {
    credit.getList(req, res)
})
app.get("/creditAdditional", hasRole('USER'), function (req, res, next) {
    creditAdditional.getList(req, res)
})
app.get("/cg", hasRole('USER'), function (req, res, next) {
    cg.getList(req, res)
})
app.get("/brokerCommision", () => {
    brokerCommision.getList(req, res)
})
app.get("/discounter", hasRole('USER'), function (req, res, next) {
    discounter.getList(req, res)
})
app.get("/dealerKickback", hasRole('USER'), function (req, res, next) {
    dealerKickback.getList(req, res)
})
// app.get("/discountersTransactions", (req, res)=>{
//     discountersTransactions.getList(req, res)
// })
app.get("/documentation", hasRole('USER'), function (req, res, next) {
    documentation.getList(req, res)
})
app.get('/docApplicantBilling', hasRole('USER'), function (req, res, next) {
    docApplicantBilling.getList(req, res)
})
app.get('/docSourceBilling', hasRole('USER'), function (req, res, next) {
    docSourceBilling.getList(req, res)
})
app.get('/docVendorBilling', hasRole('USER'), function (req, res, next) {
    docVendorBilling.getList(req, res)
})
app.get("/documentationCon", hasRole('USER'), function (req, res, next) {
    documentation.getFilteredContracts(req, res)
})
app.get("/disbursement", hasRole('USER'), function (req, res, next) {
    disbursement.getList(req, res)
})
app.get('/disbursementAdditional', hasRole('USER'), function (req, res, next) {
    disbursementAdditional.getList(req, res)
})
app.get("/eot", hasRole('USER'), function (req, res, next) {
    eot.getList(req, res)
})
app.get("/equipment", hasRole('USER'), function (req, res, next) {
    equipment.getList(req, res)
})
app.get("/equipmentFunding", hasRole('USER'), function (req, res, next) {
    equipmentFunding.getList(req, res)
})
app.get("/fee", hasRole('USER'), function (req, res, next) {
    fee.getList(req, res)
})
app.get("/followup", hasRole('USER'), function (req, res, next) {
    followup.getList(req, res)
})
app.get("/payment", hasRole('USER'), function (req, res, next) {
    payment.getList(req, res)
})
app.get("/pg", hasRole('USER'), function (req, res, next) {
    pg.getList(req, res)
})
app.get('/prefunding', hasRole('USER'), function (req, res, next) {
    prefunding.getList(req, res)
})
app.get("/sourceContacts", hasRole('USER'), function (req, res, next) {
    sourceContacts.getList(req, res)
})
app.get("/sources", hasRole('USER'), function (req, res, next) {
    sources.getList(req, res)
})
app.get('/sourceBilling', hasRole('USER'), function (req, res, next) {
    sourceBilling.getList(req, res)
})
app.get("/staff", hasRole('USER'), function (req, res, next) {
    staff.getList(req, res)
})
app.get("/status", hasRole('USER'), function (req, res, next) {
    status.getList(req, res)
})
app.get("/vendors", hasRole('USER'), function (req, res, next) {
    vendor.getList(req, res)
})
app.get("/vendorsCommission", hasRole('USER'), function (req, res, next) {
    vendorCommission.getList(req, res)
})
app.get("/vendorsTotals", hasRole('USER'), function (req, res, next) {
    vendorTotals.getList(req, res)
})
app.get("/vendorFunding", hasRole('USER'), function (req, res, next) {
    vendorFunding.getList(req, res)
})



//New Getters
app.get('/dealByApp/:id', hasRole('USER'), hasRole('USER'), function (req, res, next) {
    deal.getDealByApp(req.params.id, req, res)
})
app.get('/dealByContract/:id', hasRole('USER'), hasRole('USER'), function (req, res, next) {
    deal.getDealByContract(req.params.id, req, res)
})


//getters by id
app.get('/application/:id', hasRole('SALES'), hasRole('USER'), function (req, res, next) {
    console.log('Application')
    application.get(req, res, req.params.id);
})

app.get("/applicants/:id", hasRole('USER'), function (req, res, next) {
    applicant.get(req, res, req.params.id)
})
app.get("/applicantBilling/:id", hasRole('USER'), function (req, res, next) {
    applicantBilling.get(req, res, req.params.id)
})
app.get("/applicantBill/:id", hasRole('USER'), function (req, res, next) {
    applicantBilling.getBill(req, res, req.params.id)
})
app.get('/applicationToCg/:id', hasRole('USER'), function (req, res, next) {
    app2cg.get(req, res, req.params.id)
})
app.get("/applicationToEquipment/:id", hasRole('USER'), function (req, res, next) {
    application2Equipment.get(req, res, req.params.id)
})
app.get("/applicationToPg/:id", hasRole('USER'), function (req, res, next) {
    app2pg.get(req, res, req.params.id)
})
app.get("/application/:id/personal-guarantors", hasRole('USER'), function (req, res, next) {
    app2pg.getAllGuarantorsForApplication(req, res, req.params.id)
})
app.get("/application/:id/corporate-guarantors", hasRole('USER'), function (req, res, next) {
    app2cg.getAllGuarantorsForApplication(req, res, req.params.id)
})
app.get("/application/:id/vendors", hasRole('USER'), function (req, res, next) {
    vendor.getAllVendorsForApplication(req, res, req.params.id)
})
app.get('/applicationToVendor/:id', hasRole('USER'), function (req, res, next) {
    app2vendor.get(req, res, req.params.id)
})
app.get("/bankInfo/:id", hasRole('USER'), function (req, res, next) {
    bankInfo.get(req, res, req.params.id)
})
app.get("/boardingsheet/:id", hasRole('USER'), function (req, res, next) {
    boardingsheet.get(req, res, req.params.id)
})
app.get("/comments/:id", hasRole('USER'), function (req, res, next) {
    comment.get(req, res, req.params.id)
})
app.get("/commentApp/:id", hasRole('USER'), function (req, res, next) {
    comment.getApp(req, res, req.params.id)
})
app.get("/commentCompany/:id", hasRole('USER'), function (req, res, next) {
    comment.getCompany(req, res, req.params.id)
})
app.get("/commentCredit/:id", hasRole('USER'), function (req, res, next) {
    comment.getCredit(req, res, req.params.id)
})

app.get("/commission/:id", hasRole('USER'), function (req, res, next) {
    commission.get(req, res, req.params.id)
})
app.get("/contractNumber/:id", hasRole('USER'), function (req, res, next) {
    documentation.getContract(req, res, req.params.id)
})
app.get("/credit/:id", hasRole('USER'), function (req, res, next) {
    credit.get(req, res, req.params.id)
})
app.get("/creditAdditional/:id", hasRole('USER'), function (req, res, next) {
    creditAdditional.get(req, res, req.params.id)
})
app.get("/cg/:id", hasRole('USER'), function (req, res, next) {
    cg.get(req, res, req.params.id)
})
app.get("/brokerCommision/:id", hasRole('USER'), function (req, res, next) {
    brokerCommision.get(req, res, req.params.id)
})
app.get("/disbursement/:id", hasRole('USER'), function (req, res, next) {
    disbursement.get(req, res, req.params.id)
})
app.get('/disbursement/all/:contractNumber', hasRole('USER'), function (req, res, next) {
    disbursementAll.getAll(req, res, req.params.contractNumber)
})
app.get('/disbursementAdditional/:id', hasRole('USER'), function (req, res, next) {
    disbursementAdditional.get(req, res, req.params.id)
})
app.get("/discounter/:id", hasRole('USER'), function (req, res, next) {
    discounter.get(req, res, req.params.id)
})
app.get("/dealerKickback/:id", hasRole('USER'), function (req, res, next) {
    dealerKickback.get(req, res, req.params.id)
})
// app.get("/discountersTransactions/:id", (req, res)=>{
//     discountersTransactions.get(req, res,req.params.id)
// })
app.get("/documentation/:id", hasRole('USER'), function (req, res, next) {
    documentation.get(req, res, req.params.id)
})
app.get('/docApplicantBilling/:id', hasRole('USER'), function (req, res, next) {
    docApplicantBilling.get(req, res, req.params.id)
})
app.get('/docSourceBilling/:id', hasRole('USER'), function (req, res, next) {
    docSourceBilling.get(req, res, req.params.id)
})
app.get('/docVendorBilling/:id', hasRole('USER'), function (req, res, next) {
    docVendorBilling.get(req, res, req.params.id)
})
app.get("/documentationByApp/:id", hasRole('USER'), function (req, res, next) {
    documentation.getByApp(req, res, req.params.id)
})
app.get("/documentationByContract/:id", hasRole('USER'), function (req, res, next) {
    documentation.getContract(req, res, req.params.id)
})
app.get("/eot/:id", hasRole('USER'), function (req, res, next) {
    eot.get(req, res, req.params.id)
})
app.get("/equipment/:id", hasRole('USER'), function (req, res, next) {
    equipment.get(req, res, req.params.id)
})
app.get("/equipmentFunding/:id", hasRole('USER'), function (req, res, next) {
    equipmentFunding.get(req, res, req.params.id)
})
app.get("/fee/:id", hasRole('USER'), function (req, res, next) {
    fee.get(req, res, req.params.id)
})
app.get("/followup/:id", hasRole('USER'), function (req, res, next) {
    followup.get(req, res, req.params.id)
})
app.get("/generateContractNumber", hasRole('USER'), function (req, res, next) {
    documentation.generateContractNumber(req, res)
})
app.get("/payment/:id", hasRole('USER'), function (req, res, next) {
    payment.get(req, res, req.params.id)
})
app.get('/personalCredit/:id', hasRole('USER'), function (req, res, next) {
    pc.getList(req, res, req.params.id)
})
app.get('pgCredit/:id', hasRole('USER'), function (req, res, next) {
    pc.get(req, res, req.params.id)
})
app.get("/pg/:id", hasRole('USER'), function (req, res, next) {
    pg.get(req, res, req.params.id)
})
app.get("/prefunding/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    prefunding.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/sourceContacts/:id", hasRole('USER'), function (req, res, next) {
    sourceContacts.get(req, res, req.params.id)
})
app.get("/sources/:id", hasRole('USER'), function (req, res, next) {
    sources.get(req, res, req.params.id)
})
app.get("/sourceBilling/:id", hasRole('USER'), function (req, res, next) {
    sourceBilling.get(req, res, req.params.id)
})
app.get("/sourceBill/:id", hasRole('USER'), function (req, res, next) {
    sourceBilling.getBill(req, res, req.params.id)
})
app.get("/sourceCommissions/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    sourceCommission.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/sourceFundings/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    sourceFunding.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/sourceTotal/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    sourceTotal.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/staff/:id", hasRole('USER'), function (req, res, next) {
    staff.get(req, res, req.params.id)
})
app.get("/status/:id", hasRole('USER'), function (req, res, next) {
    status.get(req, res, req.params.id)
})
app.get("/vendor/:id", hasRole('USER'), function (req, res, next) {
    vendor.get(req, res, req.params.id)
})
app.get("/vendorBilling/:vendorID", hasRole('USER'), function (req, res, next) {
    vendorBilling.get(req, res, req.params.vendorID)
})
app.get("/vendorBill/:vendorID", hasRole('USER'), function (req, res, next) {
    vendorBilling.getBill(req, res, req.params.vendorID)
})
app.get("/vendorCommission/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    vendorCommission.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/vendorTotals/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    vendorTotals.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/vendorFunding/:docID/:vendorID", hasRole('USER'), function (req, res, next) {
    vendorFunding.get(req, res, req.params.docID, req.params.vendorID)
})
app.get("/return-CreditApproval/:id", hasRole('CREDIT'), function (req, res, next) {
    console.log('return Approval')
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/Approval${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
    // var options = {headers: {'Context-Type':'application/pdf'}}
    // res.download(`D:/ReportOutput/Approval${req.params.id}.pdf`, `Approval${req.params.id}`)
    // res.end()
})
app.get("/return-CreditScoreCard/:id", hasRole('CREDIT'), function (req, res, next) {
    console.log('return Score Card')
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/ScoreCard${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
})
app.get("/return-CreditApprovalTransmittalBroker/:id", hasRole('CREDIT'), function (req, res, next) {
    console.log('return Approval Tansmittal Broker')
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/ApprovalTransmittalBroker${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
})
app.get("/return-CreditApprovalTransmittalDiscounter/:id", hasRole('CREDIT'), function (req, res, next) {
    console.log('return Approval Tansmittal Discounter')
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/ApprovalTransmittalDiscounter${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
})
app.get("/return-DocRequestForm/:id", hasRole('CREDIT'), function (req, res, next) {
    console.log('return Doc Request Form')
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/DocRequestForm${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
})
app.get("/return-FundingCoverSheet/:id", hasRole('CREDIT'), function (req, res, next) {
    console.log('return Funding Cover Sheet')
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/FundingCoverSheet${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
})
app.get("/return-BoardingSheet/:id", function (req, res, next) {
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/BoardingSheet${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
    // res.end()
})
app.get("/return-Disbursment/:id", function (req, res, next) {
    var chunks = []
    let buffer
    let stream = fs.createReadStream(`D:/ReportOutput/Disbursment${req.params.id}.pdf`)
    stream.on('data', function (chunk) {
        console.log('downloading')
        chunks.push(chunk)
    })
    stream.on('end', function () {
        console.log('Downloaded')
        var file = Buffer.concat(chunks).toString('base64')
        res.set("Access-Control-Allow-Origin", "*")
        res.append("Access-Control-Allow-Headers", "X-Requested-With")
        res.append('content-type', 'application/pdf; charset=base64')
        res.send(file)

    })
    // res.end()
})


//posters

app.post("/genCreditApproval/:id", hasRole('CREDIT'), function (req, res, next) {
    pdf.create(templates("Approval", req.body)).toFile('D:/ReportOutput/Approval' + req.params.id + '.pdf', (err) => {
        if (err) {
            res.send(Promise.reject(err));
        }
        res.send(Promise.resolve())
    })
})
app.post("/genBoardingSheet/:id", hasRole('DOCUMENTATION'), function (req, res, next) {
    pdf.create(templates("Boarding Sheet", req.body)).toFile(`D:/ReportOutput/BoardingSheet${req.params.id}.pdf`, (err) => {
        if (err) {
            res.send(Promise.reject())
        }
        res.send(Promise.resolve())
    })
    // res.end()
})
app.post("/genCreditScoreCard/:id", hasRole('CREDIT'), function (req, res, next) {
    pdf.create(templates("Score Card", req.body)).toFile('D:/ReportOutput/ScoreCard' + req.params.id + '.pdf', (err) => {
        if (err) {
            res.send(Promise.reject());
        }
        res.send(Promise.resolve());
    })
    // res.end()
})
app.post("/genCreditApprovalTransmittalBroker/:id", hasRole('CREDIT'), function (req, res, next) {
    pdf.create(templates("Approval Transmittal Broker", req.body)).toFile('D:/ReportOutput/ApprovalTransmittalBroker' + req.params.id + '.pdf', (err) => {
        if (err) {
            res.send(Promise.reject())
        }
        res.send(Promise.resolve())
    })
    // res.end()
})
app.post("/genCreditApprovalTransmittalDiscounter/:id", hasRole('CREDIT'), function (req, res, next) {
    pdf.create(templates("Approval Transmittal Discounter", req.body)).toFile('D:/ReportOutput/ApprovalTransmittalDiscounter' + req.params.id + '.pdf', (err) => {
        if (err) {
            res.send(Promise.reject());
        }
        res.send(Promise.resolve());
    })
    // res.end()
})
app.post("/genDocRequestForm/:id", hasRole('CREDIT'), function (req, res, next) {
    pdf.create(templates("Doc Request Form", req.body)).toFile('D:/ReportOutput/DocRequestForm' + req.params.id + '.pdf', (err) => {
        if (err) {
            res.send(Promise.reject())
        }
        res.send(Promise.resolve())
    })
    // res.end()
})
app.post("/genFundingCoverSheet/:id", hasRole('CREDIT'), function (req, res, next) {
    pdf.create(templates("Funding Cover Sheet", req.body)).toFile(`D:/ReportOutput/FundingCoverSheet${req.params.id}.pdf`, (err) => {
        if (err) {
            res.send(Promise.reject())
        }
        res.send(Promise.resolve())
    })
    // res.end()
})
app.post("/genDisbursment/:id", hasRole('DOCUMENTATION'), function (req, res, next) {
    console.log('Gen Dis')
    pdf.create(templates("Disbersment", req.body)).toFile(`D:/ReportOutput/Disbursment${req.params.id}.pdf`, (err) => {
        if (err) {
            res.send(Promise.reject())
        }
        res.send(Promise.resolve())
    })
    // pdf.create(templates("Disbersment",req.body)).toStream(function(err, stream){
    //     if(err){
    //         res.send(err)
    //     }
    //     else{
    //         res.writeHead(200, {
    //             'Conntent-disposition': `attachment; filename=${req.body.companyInfo['Company Name']}_Disbursment.pdf`
    //         })
    //         stream.pipe(res)
    // res.end()
    //     }
    // })
    // res.end()
})
app.post('/application/add', function (req, res, next) {
    application.add(req, res, req.body);
})
app.post("/applicants/add", function (req, res, next) {
    applicant.add(req, res, req.body)
})
app.post("/applicantBilling/add", function (req, res, next) {
    applicantBilling.add(req, res, req.body)
})
app.post('/applicationToCg/add', function (req, res, next) {
    app2cg.add(req, res, req.body)
})
app.post("/applicationToEquipment/add", function (req, res, next) {
    application2Equipment.add(req, res, req.body)
})
app.post("/applicationToPg/Add", function (req, res, next) {
    app2pg.add(req, res, req.body)
})
app.post('/applicationToVendor/add', function (req, res, next) {
    app2vendor.add(req, res, req.body)
})
app.post("/bankInfo/add", function (req, res, next) {
    bankInfo.add(req, res, req.body)
})
app.post('/boardingsheet/add', function (req, res, next) {
    boardingsheet.add(req, res, req.body)
})
app.post("/comments/add", function (req, res, next) {
    comment.add(req, res, req.body)
})
app.post("/commision/add", function (req, res, next) {
    commission.add(req, res, req.body)
})
app.post("/credit/add", function (req, res, next) {
    credit.add(req, res, req.body)
})
app.post("/creditAdditional/add", function (req, res, next) {
    creditAdditional.add(req, res, req.body)
})
app.post('/credit/staff/all', hasRole('CREDIT'), function (req, res, next) {
    var data = req.body
    staff.getStaffNames(req, res, req.body)
    // try {
    //     db.executeSql('SELECT * FROM staff WHERE ID = ?', [data.creditOfficer], function (creditOfficer, err) {
    //         if (err) throw new Error(err)
    //             db.executeSql('SELECT * FROM staff WHERE ID = ?', [data.secondApproval], function (secondApproval, err1) {
    //                 if(err1) throw new Error(err1)
    //                 db.executeSql('SELECT * FROM staff WHERE = ?', [data.bdo], function (bdo, err2) {
    //                     if(err2) throw new Error(err2)
    //                     res.send({creditOfficer, secondApproval, bdo})
    //                 })
    //             })
    //     })
    // }
    // catch (err) {
    //     res.writeHead(500, err.message);
    //     res.end(JSON.stringify({data: "Error occurred: " + err.message}));
    // }
})
app.post("/cg/add", function (req, res, next) {
    cg.add(req, res, req.body)
})
app.post("/brokerCommision/add", () => {
    brokerCommision.add(req, res, req.body)
})
app.post("/discounter/add", function (req, res, next) {
    discounter.add(req, res, req.body)
})
app.post("/dealerKickback/add", function (req, res, next) {
    dealerKickback.add(req, res, req.body)
})
// app.post("/discountersTransactions/add", (req, res)=>{
//     discountersTransactions.add(req, res, req.body)
// })
app.post("/documentation/add", function (req, res, next) {
    documentation.add(req, res, req.body)
})
app.post('/docApplicantBilling/add', function (req, res, next) {
    docApplicantBilling.add(req, res, req.body)
})
app.post('/docSourceBilling/add', function (req, res, next) {
    docSourceBilling.add(req, res, req.body)
})
app.post('/docVendorBilling/add', function (req, res, next) {
    docVendorBilling.add(req, res, req.body)
})
app.post("/disbursement/add", function (req, res, next) {
    disbursement.add(req, res, req.body)
})
app.post("/disbursementAdditional/add", function (req, res, next) {
    disbursementAdditional.add(req, res, req.body)
})
app.post("/eot/add", function (req, res, next) {
    eot.add(req, res, req.body)
})
app.post("/equipment/add", function (req, res, next) {
    equipment.add(req, res, req.body)
})
app.post("/equipmentFunding/add", function (req, res, next) {
    equipmentFunding.add(req, res, req.body)
})
app.post("/fee/add", function (req, res, next) {
    fee.add(req, res, req.body)
})
app.post("/followup/add", function (req, res, next) {
    followup.add(req, res, req.body)
})
app.post("/payment/add", function (req, res, next) {
    payment.add(req, res, req.body)
})
app.post('/personalCredit/add', function (req, res, next) {
    pc.add(req, res, req.body)
})
app.post("/pg/add", function (req, res, next) {
    pg.add(req, res, req.body)
})
app.post("/prefunding/add", function (req, res, next) {
    prefunding.add(req, res, req.body)
})
app.post("/sourceContacts/add", function (req, res, next) {
    sourceContacts.add(req, res, req.body)
})
app.post("/sources/add", function (req, res, next) {
    sources.add(req, res, req.body)
})
app.post("/sourceBilling/add", function (req, res, next) {
    sourceBilling.add(req, res, req.body)
})
app.post("/sourcesCommission/add", function (req, res, next) {
    sourceCommission.add(req, res, req.body)
})
app.post("/sourcesFunding/add", function (req, res, next) {
    sourceFunding.add(req, res, req.body)
})
app.post("/sourcesTotal/add", function (req, res, next) {
    sourceTotal.add(req, res, req.body)
})
app.post("/staff/add", function (req, res, next) {
    staff.add(req, res, req.body)
})
app.post("/status/add", function (req, res, next) {
    status.add(req, res, req.body)
})
app.post("/vendor/add", function (req, res, next) {
    vendor.add(req, res, req.body)
})
app.post("/vendorBilling/add", function (req, res, next) {
    vendorBilling.add(req, res, req.body)
})
app.post("/vendorCommission/add", function (req, res, next) {
    vendorCommission.add(req, res, req.body)
})
app.post("/vendorFunding/add", function (req, res, next) {
    vendorFunding.add(req, res, req.body)
})
app.post("/vendorTotals/add", function (req, res, next) {
    vendorTotals.add(req, res, req.body)
})

//putters
app.put('/application/update', function (req, res, next) {
    application.update(req, res, req.body);
})
app.put("/applicants/update", function (req, res, next) {
    applicant.update(req, res, req.body)
})
app.put("/applicantBilling/update", function (req, res, next) {
    applicantBilling.update(req, res, req.body)
})
app.put('/applicationToCg/update', function (req, res, next) {
    applicationToCg.update(req, res, req.body)
})
app.put("/applicationToEquipment/update", function (req, res, next) {
    application2Equipment.update(req, res, req.body)
})
app.put("/applicationToPg/update", function (req, res, next) {
    app2pg.update(req, res, req.body)
})
app.put('/applicationToVendor/update', function (req, res, next) {
    app2vendor.update(req, res, req.body)
})
app.put("/bankInfo/update", function (req, res, next) {
    bankInfo.update(req, res, req.body)
})
app.put("/batchApp/update", function (req, res, next) {
    batchApp.update(req, res, req.body)
})
app.put('/boardingsheet/update', function (req, res, next) {
    boardingsheet.update(req, res, req.body)
})
app.put("/comments/update", function (req, res, next) {
    comment.update(req, res, req.body)
})
app.put("/commission/update", function (req, res, next) {
    commission.update(req, res, req.body)
})
app.put("/credit/update", function (req, res, next) {
    credit.update(req, res, req.body)
})
app.put("/creditAdditional/update", function (req, res, next) {
    creditAdditional.update(req, res, req.body)
})
app.put("/cg/update", function (req, res, next) {
    cg.update(req, res, req.body)
})
app.put("/brokerCommision/update", () => {
    brokerCommision.update(req, res, req.body)
})
app.put("/discounter/update", function (req, res, next) {
    discounter.update(req, res, req.body)
})
app.put("/dealerKickback/update", function (req, res, next) {
    dealerKickback.update(req, res, req.body)
})
// app.put("/discountersTransactions/update", (req, res)=>{
//     discountersTransactions.update(req, res, req.body)
// })
app.put("/documentation/update", function (req, res, next) {
    documentation.update(req, res, req.body)
})
app.put('/docApplicantBilling/update', function (req, res, next) {
    docApplicantBilling.update(req, res, req.body)
})
app.put('/docSourceBilling/update', function (req, res, next) {
    docSourceBilling.update(req, res, req.body)
})
app.put('/docVendorBilling/update', function (req, res, next) {
    docVendorBilling.update(req, res, req.body)
})
app.put("/disbursement/update", function (req, res, next) {
    disbursement.update(req, res, req.body)
})
app.put("/disbursementAdditional/update", function (req, res, next) {
    disbursementAdditional.update(req, res, req.body)
})
app.put("/eot/update", function (req, res, next) {
    eot.update(req, res, req.body)
})
app.put("/equipment/update", function (req, res, next) {
    equipment.update(req, res, req.body)
})
app.put("/equipmentFunding/update", function (req, res, next) {
    equipmentFunding.update(req, res, req.body)
})
app.put("/fee/update", function (req, res, next) {
    fee.update(req, res, req.body)
})
app.put("/followup/update", function (req, res, next) {
    followup.update(req, res, req.body)
})
app.put("/payment/update", function (req, res, next) {
    payment.update(req, res, req.body)
})
app.put("/personalCredit/update", function (req, res, next) {
    pc.update(req, res, req.body)
})
app.put("/pg/update", function (req, res, next) {
    pg.update(req, res, req.body)
})
app.put("/prefunding/update", function (req, res, next) {
    prefunding.update(req, res, req.body)
})
app.put("/sourceContacts/update", function (req, res, next) {
    sourceContacts.update(req, res, req.body)
})
app.put("/sources/update", function (req, res, next) {
    sources.update(req, res, req.body)
})
app.put("/sourceBilling/update", function (req, res, next) {
    sourceBilling.update(req, res, req.body)
})
app.put("/sourceCommission/update", function (req, res, next) {
    sourceCommission.update(req, res, req.body)
})
app.put("/sourceFunding/update", function (req, res, next) {
    sourceFunding.update(req, res, req.body)
})
app.put("/sourceTotal/update", function (req, res, next) {
    sourceTotal.update(req, res, req.body)
})
app.put("/staff/update", function (req, res, next) {
    staff.update(req, res, req.body)
})
app.put("/status/update", function (req, res, next) {
    status.update(req, res, req.body)
})
app.put("/vendor/update", function (req, res, next) {
    vendor.update(req, res, req.body)
})
app.put("/vendorBilling/update", function (req, res, next) {
    vendorBilling.update(req, res, req.body)
})
app.put("/vendorCommission/update", function (req, res, next) {
    vendorCommission.update(req, res, req.body)
})
app.put("/vendorTotals/update", function (req, res, next) {
    vendorTotals.update(req, res, req.body)
})
app.put("/vendorFunding/update", function (req, res, next) {
    vendorFunding.update(req, res, req.body)
})
//deleters
app.delete("/applicationToEquipment/delete", function (req, res, next) {
    console.log(req)
    application2Equipment.delete(req, res, req.query)
})
app.delete('/applicationToCg/delete', function (req, res, next) {
    app2cg.delete(req, res, req.query)
})
app.delete('/applicationToPg/delete', function (req, res, next) {
    app2pg.delete(req, res, req.query)
})
app.delete('/applicationToVendor/delete', function (req, res, next) {
    app2vendor.delete(req, res, req.query)
})

app.delete("/bank-info/:id", hasRole('USER'), function (req, res, next) {
    bankInfo.delete(req, res, req.params.id)
})

//lockers
app.lock("/applicant/lock/:id", function (req, res, next) {
    applicant.lock(req, res, req.params.id)
})
//unlockers

// const PORT = process.env.PORT || 9000

// app.listen(PORT, () => console.log(`Listening on port ${PORT}...`))
module.exports = app;
