Required files:

    settings.js:
        PORT: 
            The listening port
        httpMsgsFormat:
            'JSON' or 'HTML';
        sqlConfig:
            host:
                Database host name or address
            user:
                Database user name
            password:
                Database password for the user
            database:
                Name of the database
                    NEEDED
            multipleStatements: 
                true
            connectionLimit: 
                10
            queueLimit:
                0