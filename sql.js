const mysql = require('mysql2');
const setting = require('./settings');
function createPool(){
    try {
        const mysql = require('mysql2');

        const pool = mysql.createPool(setting.sqlConfig);
        const promisePool = pool.promise();
        return promisePool
    }catch (error) {
        return (console.log(`Could not connect - ${error}`))
    }
}  
const pool = mysql.createPool(setting.sqlConfig); 

exports.executeSql = (sql, callback) =>{
    console.log(sql)
    pool.getConnection(function(err, con){
        if(err) throw new Error(err)
        con.query(sql, function(err, row, fields){
            if (err) throw new Error(err)
            return callback(row)
        })
        pool.releaseConnection(con)
        
    })
    
    
};
