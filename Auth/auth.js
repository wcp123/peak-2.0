const express = require('express');
const router = express.Router();
const debug = require('debug')('api:auth');
const { v4: uuidv4 } = require('uuid');
const db = require('../dbOptions')
const bcrypt = require('bcryptjs')
const dotenv = require('dotenv')
const settings = require('../settings')
dotenv.config()

router.post('/login', async function (req, res, next) {
  const { user, password } = req.body;
  debug(`authenticating: ${user} / ${password}`);

  try {
    await db.executeSql(`SELECT * from ${settings.sqlConfig.database}.user WHERE lower(user) = ? OR lower(email) = ? LIMIT 1`, [user.toLowerCase(), user.toLowerCase()], async function (result, err) {

      if (result.length === 0) {
        return res.status(404).send({ ok: false, error: 'Account could not be found'})
      }

      result = result[0]
      await bcrypt.compare(password, result.password, async (error, success) => {
        try {
          if (!success) {
            return res.status(404).send({ ok: false, error: 'The password used is incorrect'})
          }

          await db.executeSql(`SELECT * FROM ${settings.sqlConfig.database}.user_role WHERE user_id = ?`, [result.user_id], function (results, errs) {
            var token = uuidv4();
            db.executeSql(`UPDATE ${settings.sqlConfig.database}.user set \`token\` = ? WHERE user_id = ? `,[token, result.user_id]);

            return res.send({
              ok: true,
              user: {
                name: result.name,
                roles: results.map(x => x.role_id),
                email: result.email,
                token: token,
              },
            });
          })

        }
        catch (error) {
          return res.status(error.status || 500).send({ ok: false, error: error.message })
        }
      })

    })

  }
  catch (error) {
    res.status(error.status || 500).send({ ok: false, error: error.message || error })
  }
});

router.post('/logout', async function (req, res, next) {
  if(req.headers.peaktoken){
    console.log("logging user out with token: ", req.headers.peaktoken)
    await db.executeSql(`UPDATE  ${settings.sqlConfig.database}.user SET token = null where token = ?`, [req.headers.peaktoken], function (results, errs) {
      try {
        res.send({ ok: true, message: 'You have logged out' });
      }
      catch (error) {
        return res.status(error.status || 500).send({ ok: false, error: error.message })
      }
    })
  }else{
    res.send({ ok: true, message: 'You have logged out' });
  }
});

router.get('/profile', async function (req, res, next) {
  console.log('profile')
  var refreshToken = req.cookies.rt
  if (!refreshToken) return res.status(403).send({ ok: false, error: "No Session" });
  debug(`rt: ${refreshToken}`);
  let userRecord
  try {
    userRecord = await jwt.verify(refreshToken, privateKey, { algorithms: 'RS256' });
  }
  catch (err) {
    return res.status(500).send({ ok: false, error: "Malformed JWT", reason: err.message })
  }
  debug(userRecord)
  await db.executeSql(`SELECT * from ${settings.sqlConfig.database}.user WHERE name = ?`, [userRecord.user], async function (result, err) {
    try {

      if (err) {
        console.log("executeSQL err")
        var error = new Error(err.message)
        error.message = "Login Failed"
        error.status = 403
        throw error
      }
      if (result.length === 0) {
        var error = new Error("Login not Found")
        error.status = 404
        error.message = "Login Not Found"
        throw error
      }
      //x is placeholder for logic check on decrypted password

      let foundUser = result[0]
      await db.executeSql(`SELECT * from ${settings.sqlConfig.database}.user_role WHERE user_id = ?`, [foundUser.user_id], async function (result, err) {
        try {
          if (err) {
            console.log('executeSQL err')
            var error = new Error("Login Failed")
            error.reason = err
            error.status(404)
            error.message = "Roles not found"
            throw errow
          }
          if (result.length === 0) {
            var error = new Error("Login not Found")
            error.status = 404
            error.message = "Login Not Found"
            throw error
          }
          console.log('Profile')
          refreshToken = jwt.sign(
            {
              user: foundUser.name,
              logins: foundUser.logins,
            },
            privateKeyObj,
            {
              algorithm: 'RS256',
              expiresIn: '2h',
            }
          );

          res.cookie('rt', refreshToken, {
            sameSite: true,
            httpOnly: true,
          });
          return res.send({
            ok: true,
            user: {
              name: foundUser.name,
              roles: foundUser.roles,
              email: foundUser.email,
              accessToken: jwt.sign(
                {
                  user: foundUser.user,
                  name: foundUser.name,
                  roles: result.map(x => x.role_id),
                  email: foundUser.email,
                },
                privateKey,
                {
                  algorithm: 'RS256',
                  expiresIn: '1m',
                }
              ),
            },
          });
        }
        catch (err) {
          console.log(err)
          return res.status(err.status || 500).send({ ok: false, error: err.message })

        }

      })

    }
    catch (err) {
      console.log(err)
      return res.status(err.status || 500).send({ ok: false, error: err.message })
    }
  })
})

router.post('/register', async function (req, res, next) {
  const header = req.headers.authorization
  if (header) {
    var token = header.split(' ')[1]
    try {
      var user = await jwt.verify(token, privateKey, {
        algorithms: ['RS256']
      })
      if (user.roles.includes('ADMIN')) {
        var { email, name, user, password } = req.body
        var hash = await bcrypt.hash(password, 10)
        var sql = `INSERT into ${settings.sqlConfig.database}.user (email, name, user, password) values (?,?,?,?)`
        var options = [email, name, user, hash]
        db.executeSql(sql, options, function (result, err) {
          req.body.roles.map(x => {
            db.executeSql(`INSERT into ${settings.sqlConfig.database}.user_role (user_id,role_id) values (?,?)`, [result.insertId, x], function (result, err) {
              if (err) throw err
            })
          })
          if (err) {
            throw err
          }
          if (result.length === 0) {
            let error = new Error("Insert Fault")
            error.status = 500
            throw error
          }
        })
        return res.status(200).send({ok:true, message:"Registered"})

      }
      return res.status(403).send({ ok: false, error: "You are not Authorized" })


    }
    catch (error) {
      console.log(error)
      res.status(error.status || 500).send({ ok: false, error: error.message || error })
    }

  }

})

module.exports = router;

