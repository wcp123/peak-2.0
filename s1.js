const bodyParser = require('body-parser')
const express = require('express');
const cors = require('cors')
const app = express();
const fs = require('fs')
const path = require('path')
const cookieParser = require('cookie-parser');
const apiRouter = require('./ExpressDemo')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
// app.use(cors())
app.use(cookieParser())

const authRouter = require('./Auth/auth')
app.use('/auth', authRouter)
app.use('/api', apiRouter)

const PORT = process.env.PORT || 9000

app.listen(PORT,'0.0.0.0', ()=> console.log(`Listening on port ${PORT}...`))