const settings = require("./settings");
const http = require("http");
exports.show500 = function(req, resp, err) {
    if(settings.httpMsgsFormat === "HTML"){
        resp.writeHead(500, "Internal Error occured", {"Content-Type": "application/json"});
        resp.end(JSON.stringify({data: "ERROR occured: " + err}));
    }
    else {
        resp.writeHead(500, "Internal Error Occured", {"Content-type" : "application/json"});
        resp.end(JSON.stringify({data: "Error occurred: " + err}));
    }
}

exports.show200 = function(req, resp){
    resp.writeHead(200, {"Content-type" : "application/json"});
    resp.end();
}

exports.show405 = function(req, resp) {
    if(settings.httpMsgsFormat === "HTML"){
        resp.writeHead(405, "Method not supported", {"Content-Type": "application/json"});
        resp.end(JSON.stringify({data: "ERROR occured: " + "Method not supported"}));
    }
    else {
        resp.writeHead(405, "Method not supported", {"Content-type" : "application/json"});
        resp.end(JSON.stringify({data: "Error occurred: " + "Method not supported"}));
    }
}
exports.show404 = function(req,resp) {
    if(settings.httpMsgsFormat === "HTML"){
        resp.writeHead(404, "Resource not found", {"Content-Type": "application/json"});
        resp.end(JSON.stringify({data: "ERROR occured: " + "Resource not found"}));
    }
    else {
        resp.writeHead(404, "Resource not found", {"Content-type" : "application/json"});
        resp.end(JSON.stringify({data: "Error occurred: " + "Resource not found"}));
    }
}
exports.show413 = function(req,resp) {
    if(settings.httpMsgsFormat === "HTML"){
        resp.writeHead(413, "Request Entity Too Large", {"Content-Type": "application/json"});
        resp.end(JSON.stringify({data: "ERROR occured: " + "equest Entity Too Large"}), function(err){ resp.end();});
    }
    else {
        resp.writeHead(413, "equest Entity Too Large", {"Content-type" : "application/json"});
        resp.end(JSON.stringify({data: "Error occurred: " + "equest Entity Too Large"}), function(err){ resp.end()});
    }
}

exports.sendJSON = function(req,resp,data){
    if(data){
        resp.writeHead(200, {"Content-type" : "application/json"});
        resp.end(JSON.stringify(data));
    }
}
exports.showHome = function (req, resp){
    if(settings.httpMsgsFormat === "HTML"){
        resp.writeHead(200, {"Content-Type": "text/html"});
        resp.end("<html><head><title>Home</title></head><body>Valid endpoints: <br>/employees - GET - To list all employees</br><br>/employees/{emplyNumber} - GET - To Search any Employee by Number</br></body></html>");
    }
    ///else is currently broken
    else {
        resp.writeHead(200, {"Content-type" : "application/json"});
        resp.end(JSON.stringify([{"url": "/employees", "operation": "GET", "description": "To list all employees"},
                                                {"url": "/employees/<emplyNumber>", "operation": "GET", "description": "To Search any Employee by Number"}]));
    }
}