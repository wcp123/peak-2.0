const mysql = require('mysql2');
const setting = require('./settings');
function createPool(){
    try {
        const mysql = require('mysql2');

        const pool = mysql.createPool(setting.sqlConfig);
        const promisePool = pool.promise();
        return promisePool
    }catch (error) {
        return (console.log(`Could not connect - ${error}`))
    }
}  
const pool = createPool(); 
exports.executeSql = (sql, callback) =>{
    const connection = mysql.createConnection(setting.sqlConfig);
    console.log(sql)
    connection.query(sql, function(err, results, fields){
        if(err){
            console.log(err)
            connection.end()
        };
        if(results){
            callback(results)
            connection.end()
            return
        }
        
    })
    
};
